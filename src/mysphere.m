function mysphere(vC,k,clo,numpts)
%vC=[XV,YV,ZV], k=radius clo= color string
if ~exist('numpts','var');
    numpts=10;
end
[iR iC] = size(vC);
for iCount = 1: 1 : iR
    [a b c] = sphere(numpts);
    hold on
    surf(k*a+vC(iCount,1),k*b+vC(iCount,2),k*c+vC(iCount,3),eps*ones(size(c)),'EdgeColor','none','FaceColor',clo(iCount,:))
end
 
