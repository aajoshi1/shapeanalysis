function h=plot_curve(curve,color1,h)

if ~exist('h','var')
    h=figure;
end
hold on;
if ~exist('color1','var')
    color1=repmat([0,.2,.5],length(curve.vertices),1);
end
    for jj=1:size(curve.faces)
        if size(curve.vertices,2) ==2
            
        if length(curve.faces)==length(color1)
            line(curve.vertices(curve.faces(jj,:),1),curve.vertices(curve.faces(jj,:),2),'Color',mean(color1(curve.faces(jj,:),:),1),'LineWidth',4);
        else
            line(curve.vertices(curve.faces(jj,:),1),curve.vertices(curve.faces(jj,:),2),'Color',color1(jj,:),'LineWidth',4);
        end
        else
        if length(curve.faces)==length(color1)
            line(curve.vertices(curve.faces(jj,:),1),curve.vertices(curve.faces(jj,:),2),curve.vertices(curve.faces(jj,:),3),'Color',mean(color1(curve.faces(jj,:),:),1),'LineWidth',4);
        else
            line(curve.vertices(curve.faces(jj,:),1),curve.vertices(curve.faces(jj,:),2),curve.vertices(curve.faces(jj,:),3),'Color',color1(jj,:),'LineWidth',4);
        end
            
            
        end
        
    jj
    end


axis equal;