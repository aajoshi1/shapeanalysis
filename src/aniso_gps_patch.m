
function [GPS_surf,gps_spectr,curvt] = aniso_gps_patch(so1,NumEig) 

  %  GPS_surf=zeros(length(so1.vertices),NumEig);
    %GPS_tar=zeros(length(to1.vertices),NumEig);
if ~exist('NumEig','var')
    NumEig    
end
%     for roiid1=1:length(aa)
%         roiid=aa(roiid1);
        s1=so1;
%         s1.faces(flabels~=roiid,:)=[];
      %  s1=myclean_patch3(s1);
        s1=myclean_patch_cc(s1);
        [s1sm,~,C]=smooth_cortex_fast(s1,.1,5);
        curvt=curvature_cortex_fast(s1sm,50,0,C);
        %view_patch(s1);
        
        B=get_mass_matrix_tri(s1);
        bdr=boundary_vertices(s1);
        inds=[1:length(s1.vertices)]';
        inds(bdr)=[];
        
        %[A,Dx,Dy]=get_stiffness_matrix_tri_wt(s1,ones(length(s1.vertices),1));
        [A,Dx,Dy]=get_stiffness_matrix_tri_wt(s1,(0.51+curvt).^.3);
        A=.5*(A+A');B=.5*(B+B');
        [EVec1,Eval]=eigs(A,B,NumEig+1,'sm');
        
        EVec1=real(EVec1);Eval=abs(Eval);
        [Eval,ind]=sort(diag(Eval));Eval=diag(Eval);
        
        EVec1=EVec1(:,ind);
        %[EVec,Eval]=eigs(A(inds,inds),B(inds,inds),6,'sm');
        %EVec1(inds,:)=EVec;EVec1(bdr,:)=0;

     %   colr=prism(length(aa)*NCu);
        if corr(EVec1(:,2),s1.vertices(:,2))>0
            EVec1(:,2)=-EVec1(:,2);
        end
        
        GPS=EVec1(:,2:end)*(sqrt(Eval(2:end,2:end)^-1));
        gps_spectr.coeff=diag(sqrt(Eval(2:end,2:end)^-1));
        gps_spectr.vec=EVec1(:,2:end);
     %   surfpatch=s1;
        %[~,ind1]=intersect(so1.vertices,s1.vertices,'rows');
        GPS_surf=GPS;  
        
%