
function crv=calc_GPS_curves(crv,NEIG,VERBOSE)
if ~exist('VERBOSE','var')
    VERBOSE=1;
end
if length(crv)<15
    return;
end

curve.faces=[[1:length(crv)-1]',[2:length(crv)]'];
curve.vertices=double(crv);
CC1=double(crv);

CC1=double(crv);

      [b,m,n]=unique(round(1000*CC1),'rows');CC1=CC1(sort(m),:);
% % % 
    x=CC1(:,1)';y=CC1(:,2)';z=CC1(:,3)';
    curve.vertices=[x',y',z'];
   curve.faces=[[1:length(x)-1]',[2:length(x)]'];%curve.faces=[curve.faces;[length(x),1]];
windowSize = 5;
p=polyfit([1:length(curve.vertices)]',curve.vertices(:,1),18);
curve.vertices(:,1)=polyval(p,[1:length(curve.vertices)]');

p=polyfit([1:length(curve.vertices)]',curve.vertices(:,2),18);
curve.vertices(:,2)=polyval(p,[1:length(curve.vertices)]');

p=polyfit([1:length(curve.vertices)]',curve.vertices(:,3),18);
curve.vertices(:,3)=polyval(p,[1:length(curve.vertices)]');


[cuv,tau,T,N,B,s,ds] = frenetframe(curve.vertices,-1e300);
 cuv(isnan(cuv))=eps;cuv(isinf(cuv))=eps;cuv=abs(cuv);
 d=[cuv(1);cuv;cuv(end)];
   M=get_mass_matrix_curve_aniso(curve,1);
   S=get_stiffness_matrix_curve_aniso(curve,d);
   opts.tol=1e-200;A=full(M^-1*S);
   A(1,:)=[];A(end,:)=[];A(:,1)=[];A(:,end)=[];
   [E,lambda]=eigs(sparse(A),NEIG,'sm',opts);
   
   E=[zeros(1,size(E,2));E;zeros(1,size(E,2))];
  
   E=real(E);lambda=abs(lambda);
   [lambda,ind]=sort(diag(lambda));lambda=diag(lambda);
   E=E(:,ind);

   E=real(E);E2=E;
   for kk=1:size(E,2)
       E(:,kk)=(E(:,kk))./norm(E(:,kk));
       E2(:,kk)= E(:,kk)./sqrt(lambda(kk,kk));
   end
   colr=jet(1000);
    E1=abs(E)-min(abs(E(:)));E1=1000*E1./max(E1(:));E1=round(E1); E1=max(min(E1,1000),1);
    
    if (VERBOSE)
    hh= figure;
    for kk1=1:min(NEIG,4)
        E1(:,kk1)=round(950*E1(:,kk1)/max(E1(:,kk1)))+1;
        subplot(1,min(NEIG,4),kk1);
        plot_curve(curve,colr(E1(:,kk1),:),hh);axis off;
    end
    end
    EigVal=diag(lambda);%close all;
    EigVec=E*size(E,1);
    GPS=EigVec*diag(sqrt(1./EigVal));%Left.Sig{kkk}(1:2);Right.Sig{kkk}=Right.Sig{kkk}(1:2);
    crv=curve;
    crv.GPS=GPS;
    crv.EigVec=EigVec;
    crv.EigVal=EigVal;
    
    
    