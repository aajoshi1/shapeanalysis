%||AUM||
%||Shree Ganeshaya Namaha||
% clc;clear all;%close all;
% opengl software;
% 
% dfc2eig('/home/ajoshi/for_hanna/automatic/2125c/2125c.left.mapped.dfc',14);
% dfc2eig('/home/ajoshi/for_hanna/automatic/2125c/2125c.right.mapped.dfc',14);
% %dfc2eig('/home/ajoshi/for_hanna/with_sulci/2125c/2125c.left.dfc',10);
%fdfc2eig('/home/ajoshi/for_hanna/with_sulci/2125c/2125c.right.dfc',10);

function left_vs_right_atlas_dist_refined(subbasename)

atlasbasename='/ifshome/ajoshi/AnandJoshi/cvs_sandbox/svreg/HD_Atlas/mri';

% l=readdfc_sipi(sprintf('/home/ajoshi/for_hanna/automatic/2125c/2125c.left.mapped.dfc')
% r=readdfc_sipi('/home/ajoshi/for_hanna/automatic/2125c/2125c.right.mapped.dfc')
% %close all;
% 
% l{1}
 Left=load(sprintf('%s.left.mapped.refined.mat',subbasename));
 Right=load(sprintf('%s.right.refined.mat',atlasbasename));
%  
%  Right=load('/home/ajoshi/for_hanna/automatic/2125c/2125c.right.mapped.mat');

%Left=load('/home/ajoshi/for_hanna/automatic/2125c/2125c.left.mat');
%Right=load('/home/ajoshi/for_hanna/automatic/2125c/2125c.right.mat');
% l_param=param_curve(l{1});
% r_param=param_curve(r{1});

close all; drawnow;
%for jjj=1:length(Left.EigVal)
%for kkk=1:length(Left.EigVal)
for jjj=length(Left.curves):-1:1
kkk=jjj;
if isempty(Left.curves{jjj}) | isempty(Right.curves{jjj} )
    continue;
end
l{kkk}=Left.curves{jjj}.vertices;
r{kkk}=Right.curves{jjj}.vertices;

%Left.curve;
lgnth=2^size(Right.GPS{jjj},2);
aqq=1e100;
r_param=param_curve(r{jjj});l_param=param_curve(l{kkk});

l_param=[1:length(l_param)]'/length(l_param);
r_param=[1:length(r_param)]'/length(r_param);

for kk=0:lgnth-1
    aaa=dec2bin(kk,size(Right.GPS{jjj},2));
    
    for k=1:length(aaa)
        bbb(k)=str2num(aaa(k));
    end
    
    aaa=diag(2*(bbb-.5));
    Mx=3*max(abs(Left.GPS{jjj}(:,1)));
    [aq,pm,indmov,indfxed]=l2dist([Left.GPS{jjj},Mx*l_param],[Right.GPS{kkk}*aaa,Mx*r_param]);
    if aq<aqq
        aqq=aq;
        ind=indmov;
        ind2=indfxed;
        sgn1=aaa;
    %aqq=min(aq,aqq);
    end
end
%figure;plot(l_param(ind(2:end-1)));hold on;
%h=figure;plot(l_param,Left.GPS{kkk},'LineWidth',2);saveas(h,sprintf('%d_curves_left.png',kkk));
%h=figure;plot(r_param,Right.GPS{kkk}*sgn1,'LineWidth',2);saveas(h,sprintf('%d_curves_right.png',kkk));
l_param(ind);
lll=medfilt1(l_param(ind(2:end-1))',3);
 p=polyfit(1:length(ind)-2,medfilt1(l_param(ind(2:end-1))'),10);
 l_param(ind(2:end-1))=polyval(p,1:length(ind)-2);
% 
 p=polyfit(1:length(ind2)-2,medfilt1(r_param(ind2(2:end-1))'),10);
 r_param(ind2(2:end-1))=polyval(p,1:length(ind2)-2);
 left_coordinates=[Left.GPS{jjj},Mx*l_param];
 right_coordinates=[Right.GPS{kkk}*sgn1,Mx*r_param];
 
%save(sprintf('%s_curvesdist.mat',subbasename));
save(sprintf('%s_curvesdist_right_atlas_%d.refined.mat',subbasename,jjj),'ind','ind2','sgn1','Mx','left_coordinates','right_coordinates');


 % 
 %plot(l_param(ind(2:end-1)));hold on;
% 
% 
 %ind3=dsearchn(l_param(2:end-1),r_param(ind2(2:end-1)));ind2=[1;ind3+1;length(ind3)+2];
 %ind3=dsearchn(r_param(2:end-1),l_param(ind(2:end-1)));ind=[1;ind3+1;length(ind3)+2];


% h=figure;line(l{kkk}(:,1),l{kkk}(:,2),l{kkk}(:,3),'LineWidth',2);
% %figure;line(l{1}(ind,1),l{1}(ind,2),l{1}(ind,3));
% hold on;line(r{jjj}(1:5:end,1),r{jjj}(1:5:end,2),r{jjj}(1:5:end,3),'LineWidth',2);
% hold on;line([l{kkk}(ind(1:5:end),1)';r{kkk}(1:5:end,1)'],[l{kkk}(ind(1:5:end),2)';r{kkk}(1:5:end,2)'],[l{kkk}(ind(1:5:end),3)';r{kkk}(1:5:end,3)'],'LineWidth',2);
% saveas(h,sprintf('%d_curves_corr.png',kkk));
% view(-90,0);
% saveas(h,sprintf('%d_curves_corr2.png',kkk));

% 
% figure;line([r{1}(ind2,1),l{1}(:,1)],[r{1}(ind2,2),l{1}(:,2)],[r{1}(ind2,3),l{1}(:,3)]);
% hold on;
% plot([r{1}(ind2,1),l{1}(:,1)],[r{1}(ind2,2),l{1}(:,2)],[r{1}(ind2,3),l{1}(:,3)],'.');
% dst(jjj,kkk)=aqq;

%     if length(Left.Sig{jjj}).*length(Right.Sig{jjj})>0
%         dst(jjj,kkk)=sum((1./sqrt(Left.Sig{kkk})-1./sqrt(Right.Sig{jjj})).^2)
%     end
kkk
jjj
close all;drawnow;
end

% figure;
%  plot(l_param,Left.GPS{jjj})
% end
% end
% 
% for jjj=1:length(Left.Sig)
% for kkk=1:length(Left.Sig)
%     
%     
% Left.Sig{kkk}=Left.Sig{kkk}(1:2);Right.Sig{kkk}=Right.Sig{kkk}(1:2);
% l2dist()
%     if length(Left.Sig{jjj}).*length(Right.Sig{jjj})>0
%         dst(jjj,kkk)=sum((1./sqrt(Left.Sig{kkk})-1./sqrt(Right.Sig{jjj})).^2)
%     end
% end
% end
% Left.Sig
% Right.Sig
% close all;figure;
% imagesc(dst)


