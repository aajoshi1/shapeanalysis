% Copyright 2010 Anand A. Joshi, David W. Shattuck and Richard M. Leahy 
% This file is part SVREG.
% 
% SVREG is free software: you can redistribute it and/or modify
% it under the terms of the GNU Lesser General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
% 
% SVREG is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU Lesser General Public License for more details.
% 
% You should have received a copy of the GNU Lesser General Public License
% along with SVREG.  If not, see <http://www.gnu.org/licenses/>.

function [dst,ptmov_dist,indmov indfxed] = l2dist(ptmov,ptfxed)
% calculates distances from s1 to s2, s2 to s1 and calculates average along
% s1

[indmov]=dsearchn(ptmov,ptfxed);
dist_vec_indmov = ptfxed - ptmov(indmov,:);

[indfxed]=dsearchn(ptfxed,ptmov);
dist_vec = ptfxed(indfxed,:) - ptmov;

dval = [dist_vec_indmov;dist_vec];
ind = [indmov;[1:length(ptmov)]'];

dval1=zeros(length(ptmov),3);
for jj=1:size(dval,2)
dval1(:,jj)=accumarray(ind,dval(:,jj));
end


divf=accumarray(ind,ones(length(ind),1));

for jj=1:size(dval,2)
dval1(:,jj)=dval1(:,jj)./divf;
end


ptmov_dist=dval1;

dst=mean(sqrt(sum(ptmov_dist.^2,2)));

