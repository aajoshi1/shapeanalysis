function M=get_mass_matrix_curve_aniso(curve,d)


V1=curve.faces(:,1);V2=curve.faces(:,2);
XY=curve.vertices(V1,:)-curve.vertices(V2,:);
L=sqrt(sum(XY.^2,2));

rows=[V1;V1;V2;V2];

cols=[V1;V2;V1;V2];
L=L.*d;
vals=[L./3;L./6;L./6;L./3];


M=sparse(rows,cols,vals);

