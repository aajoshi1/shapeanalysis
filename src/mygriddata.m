% Copyright 2010 Anand A. Joshi, David W. Shattuck and Richard M. Leahy 
% This file is part SVREG.
% 
% SVREG is free software: you can redistribute it and/or modify
% it under the terms of the GNU Lesser General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
% 
% SVREG is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU Lesser General Public License for more details.
% 
% You should have received a copy of the GNU Lesser General Public License
% along with BSE.  If not, see <http://www.gnu.org/licenses/>.

function znew=mygriddata(x,y,z,xin,yin,interpm)
    warning off
    
    if ~exist('interpm','var')
        F = TriScatteredInterp(x,y,z);
    else
        F = TriScatteredInterp(x,y,z,interpm);
    end
    
    znew=xin;
    znew(:)=F(xin(:),yin(:));
    indx=find(isnan(znew));
    %znew(indx)=-100;
    %disp('mygriddata changed!!');
    F = TriScatteredInterp(x,y,z,'nearest');

    znew(indx)=F(xin(indx),yin(indx));
    
    warning on
