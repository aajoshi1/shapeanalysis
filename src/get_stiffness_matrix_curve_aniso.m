%||AUM||
%||Shree Ganeshaya Namaha||

function [S]=get_stiffness_matrix_curve_aniso(curve,d)
%This finds stiffness matrix for triangular elements using techniques from
%book by Sadiku: Numerical Techniques Formulas 6.17,6.18



V1=curve.faces(:,1);V2=curve.faces(:,2);
XY=curve.vertices(V1,:)-curve.vertices(V2,:);
L=sqrt(sum(XY.^2,2));
L=L.*d;

rows=[V1;V1;V2;V2];

cols=[V1;V2;V1;V2];

vals=[1./L;-1./L;-1./L;1./L];


S=sparse(rows,cols,vals);

