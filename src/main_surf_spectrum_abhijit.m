%||AUM|| ||Shree Ganeshaya Namaha||
clc;clear all;close all;opengl software;

%spectrum_surf('C:\Users\ajoshi\Documents\git_sandbox\svreg-matlab\sample\data1\data3\brainsuite_subj1_m6')

        s1=readdfs('C:\Users\ajoshi\Downloads\bones.dfs');
       B=get_mass_matrix_tri(s1);
        bdr=boundary_vertices(s1);
        inds=[1:length(s1.vertices)]';
        inds(bdr)=[];
        
        %[A,Dx,Dy]=get_stiffness_matrix_tri_wt(s1,ones(length(s1.vertices),1));
        [A,Dx,Dy]=get_stiffness_matrix_tri_wt(s1,ones(length(s1.vertices),1));
        A=.5*(A+A');B=.5*(B+B');
        [EVec1,Eval]=eigs(A,B,16+1,'sm');
        EVec1=real(EVec1);Eval=abs(Eval);
        [Eval,ind]=sort(diag(Eval));Eval=diag(Eval);
        EVec1=EVec1(:,ind);

        
for jj=1:16
    h=figure;
    patch('faces',s1.faces,'vertices',s1.vertices,'facevertexcdata',EVec1(:,jj),'facecolor','interp','edgecolor','none');axis equal;axis off; material dull;camlight;lighting phong;
    saveas(h,sprintf('bone_eig_%d.png',jj));
end