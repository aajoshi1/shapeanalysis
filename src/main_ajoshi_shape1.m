%||AUM||
%||Shree Ganeshaya Namaha||
opengl software
clc;clear all;close all;
fname='/home/ajoshi/cvs_sandbox/shape_representation/data/shapes216/face11.pgm';
file2eig(fname);
I=imread('/home/ajoshi/cvs_sandbox/shape_representation/data/shapes216/face11.pgm');
%Ed=edge(I);
[X,Y]=find(I'<10);X=X+0.00001*rand(size(X));Y=Y+0.00001*rand(size(Y));
Y=max(Y(:))-Y;
A=ashape(X,Y,10);

line(A.x(A.seg(:,:)).',A.y(A.seg(:,:)).','Marker','.');
edges1=A.seg;
vertices1=[A.x,A.y]'
A.x(A.seg)
curve1.faces=double(A.seg);
curve1.vertices=double([A.x,A.y]);
vc=vertices_connectivity_curve(curve1);
bdr=trace_boundary(1,vc,curve1);

curve.faces=[bdr,[bdr(2:end);bdr(1)]]
curve.vertices=curve1.vertices;
plot_curve(curve);
close all;
bdr=bdr(1:round(length(bdr)/50):end)
%CC1=CC3(:,[2,3]);
CC1=curve.vertices(bdr,:);
% theta=linspace(0,2*pi,500);theta=theta(1:end-1);
%    y=cos(theta);%+.01*rand(size(theta));
%    x=2*sin(theta);CC1=[x',y'];[b,m,n]=unique(round(1000*CC1),'rows');CC1=CC1(sort(m),:);
%       CC1=CC1(1:5:end,:); 
      [b,m,n]=unique(round(1000*CC1),'rows');CC1=CC1(sort(m),:);
% % % 
      [CC11,t]=fnplt(cscvn([CC1;CC1(1,:)]'));CC1=CC11';
      [b,m,n]=unique(round(1000*CC1),'rows');CC1=CC1(sort(m),:);
    x=CC1(:,1)';y=CC1(:,2)';
    curve.vertices=[x',y'];
   curve.faces=[[1:length(x)-1]',[2:length(x)]'];curve.faces=[curve.faces;[length(x),1]];
   

 cuv=  curvature_curve(curve);cuv(1)=cuv(2);cuv(end)=cuv(end-1);%cuv=abs(cuv);
 cuv(isnan(cuv))=eps;cuv(isinf(cuv))=eps;cuv=abs(cuv);
   d=sum(cuv(curve.faces),2);%d=ones(size((d)));
   M=get_mass_matrix_curve_aniso(curve,1);
   S=get_stiffness_matrix_curve_aniso(curve,d);
  % S=get_stiffness_matrix_curve(curve);
   
 %cuv=10*x  ;
 %cuv=cuv;
 %C=diag(cuv);S=C*S;
   opts.tol=1e-200;
   [E,lambda]=eigs(M^-1*S,15,'sm',opts);
   E=real(E);
   for kk=1:size(E,2)
       E(:,kk)=(E(:,kk))./norm(E(:,kk));
        E2(:,kk)= E(:,kk)./sqrt(lambda(kk,kk));
   end
   colr=jet(1000);
    E1=E-min(E(:));E1=1000*E1./max(E1(:));E1=round(E1); E1=max(min(E1,1000),1);
  for kk=1:5
     h=plot_curve(curve,colr(E1(:,kk),:));axis off;colorbar;
     saveas(h,sprintf('CC_Eig_%d.png',kk));colorbar;
  end
  
 
 aa=kmeans(E2(:,2:end),4);
 
 figure;plot_curve(curve);
 hold on;plot(x(aa==1),y(aa==1),'.b','MarkerSize',30);plot(x(aa==2),y(aa==2),'.r','MarkerSize',30);
hold on;plot(x(aa==3),y(aa==3),'.g','MarkerSize',30);plot(x(aa==4),y(aa==4),'.y','MarkerSize',30);axis equal;
Sig=diag(lambda)
save Ellipse Sig
%    
%    row=[1:length(x)-1,2:length(x),1,length(x)];col=[2:length(x),1:length(x)-1,length(x),1]; dat=1*ones(size(row));
%    
%    L=sparse(row,col,dat);
%    
%    L=L-2*speye(size(L));
%    opt.tol=1e-100;
%    [E,l]=eigs(-L,10,'sm',opt);
%    diag(l)
%    
%       E=E-min(E(:));E=1000*E./max(E(:));E=round(E); E=max(min(E,1000),1);
%    plot_curve(curve,colr(E(:,10),:));axis off;
%  