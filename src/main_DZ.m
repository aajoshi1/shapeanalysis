%||AUM||
%||Shree Ganeshaya Namaha||
clc;clear all;close all;


%close all;
dz=fopen('/ifshome/ajoshi/DZ.txt');

kk=1;
while ~feof(dz)
  dz1{kk}=fscanf(dz,'%s\n',1);kk=kk+1;
end
    
for kk=1:2:length(dz1)
    if exist(['/ifs/enigma/ychou/julio/Reg700/',dz1{kk},'/',dz1{kk},'.bse.left.mapped.mat'],'file')
        Left1{(kk-1)/2+1}=load(['/ifs/enigma/ychou/julio/Reg700/',dz1{kk},'/',dz1{kk},'.bse.left.mapped.mat']);
    else
        Left1{(kk-1)/2+1}.Sig=[];
    end
    
    if exist(['/ifs/enigma/ychou/julio/Reg700/',dz1{kk},'/',dz1{kk},'.bse.right.mapped.mat'],'file')
        Right1{(kk-1)/2+1}=load(['/ifs/enigma/ychou/julio/Reg700/',dz1{kk},'/',dz1{kk},'.bse.right.mapped.mat']);
    else
        Right1{(kk-1)/2+1}.Sig=[];
    end
    
    if exist(['/ifs/enigma/ychou/julio/Reg700/',dz1{kk+1},'/',dz1{kk+1},'.bse.left.mapped.mat'],'file')
        Left2{(kk-1)/2+1}=load(['/ifs/enigma/ychou/julio/Reg700/',dz1{kk+1},'/',dz1{kk+1},'.bse.left.mapped.mat']);
    else
        Left2{(kk-1)/2+1}.Sig=[];
    end
    if exist(['/ifs/enigma/ychou/julio/Reg700/',dz1{kk+1},'/',dz1{kk+1},'.bse.right.mapped.mat'],'file')
        Right2{(kk-1)/2+1}=load(['/ifs/enigma/ychou/julio/Reg700/',dz1{kk+1},'/',dz1{kk+1},'.bse.right.mapped.mat']);    
    else
        Right2{(kk-1)/2+1}.Sig=[];
    end

end



for jjj=1:length(Left1)
if ~isempty(Left1{jjj}.Sig) & ~isempty(Left2{jjj}.Sig)
    for kkk=1:min(length(Left1{jjj}.Sig),length(Left2{jjj}.Sig))
    if length(Left1{jjj}.Sig{kkk}).*length(Left2{jjj}.Sig{kkk})>0
        dz_left_dst(jjj,kkk)=sum((1./sqrt(Left1{jjj}.Sig{kkk})-1./sqrt(Left2{jjj}.Sig{kkk})).^2);
    end
    end
else
    dz_left_dst(jjj,:)=0;
end
end


for jjj=1:length(Right1)
if ~isempty(Right1{jjj}.Sig) & ~isempty(Right2{jjj}.Sig)
    for kkk=1:min(length(Right1{jjj}.Sig),length(Right2{jjj}.Sig))
    if length(Right1{jjj}.Sig{kkk}).*length(Right2{jjj}.Sig{kkk})>0
        dz_right_dst(jjj,kkk)=sum((1./sqrt(Right1{jjj}.Sig{kkk})-1./sqrt(Right2{jjj}.Sig{kkk})).^2);
    end
    end
else
    dz_right_dst(jjj,:)=0;
end
end



fclose(dz);
save dz_left_dst dz_left_dst
save dz_right_dst dz_right_dst
