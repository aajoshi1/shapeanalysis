

function GPS_dist=curves_distance(c1,c2)


  %  c1=load(sub1);
  %  sub2=strrep(sub2,'_lh_','_rh_');
  % c2=load(sub2);
%    [~,sub2fn]=fileparts(sub2);
 %   [pth,sub1fn]=fileparts(sub1);
    if ~isfield(c1','GPS')
        c1=calc_GPS_curves(c1,8,1);
    end
    if ~isfield(c2','GPS')
        c2=calc_GPS_curves(c2,8,1);
    end
    
   % close all; drawnow;

%kkk=jjj;
if isempty(c1.vertices) | isempty(c2.vertices)
    return;
end
l=c1.vertices;
r=c2.vertices;

lgnth=2^size(c2.GPS,2);
aqq=1e100;
r_param=param_curve(r);l_param=param_curve(l);

l_param=[1:length(l_param)]'/length(l_param);
r_param=[1:length(r_param)]'/length(r_param);

for kk=0:lgnth-1
    aaa=dec2bin(kk,size(c2.GPS,2));
    
    for k=1:length(aaa)
        bbb(k)=str2num(aaa(k));
    end
    
    aaa=diag(2*(bbb-.5));
    Mx=4*max(abs(c1.GPS(:,1)));
    [aq,pm,indmov,indfxed]=l2dist([c1.GPS,Mx*l_param],[c2.GPS*aaa,Mx*r_param]);
    if aq<aqq
        aqq=aq;
        pmm=pm;
        ind=indmov;
        ind2=indfxed;
        sgn1=aaa;
    end
end
l_param(ind);
lll=medfilt1(l_param(ind(2:end-1))',3);
 p=polyfit(1:length(ind)-2,medfilt1(l_param(ind(2:end-1))'),4);
 l_param(ind(2:end-1))=polyval(p,1:length(ind)-2);
% 
 p=polyfit(1:length(ind2)-2,medfilt1(r_param(ind2(2:end-1))'),4);
 r_param(ind2(2:end-1))=polyval(p,1:length(ind2)-2);
 
 c1_coordinates=[c1.GPS,Mx*l_param];
 c2_coordinates=[c2.GPS*sgn1,Mx*r_param];
 
%save(fullfile(pth,sprintf('%s_c1_%s_c2_curvesdist_%d.mat',sub1fn,sub2fn,jjj)),'ind','ind2','sgn1','Mx','c1_coordinates','c2_coordinates');

GPS_dist.c1_coordinates=c1_coordinates;
GPS_dist.c2_coordinates=c2_coordinates;
GPS_dist.ind1to2=ind;
GPS_dist.ind2to1=ind2;
GPS_dist.dist=aqq;
GPS_dist.dist_ptwise=sqrt(sum(pmm.^2,2));
GPS_dist.c1=c1;GPS_dist.c2=c2;

