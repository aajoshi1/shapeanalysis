% Copyright 2012 Anand A. Joshi, David W. Shattuck and Richard M. Leahy 
% This file is part SVREG.
% 

clc;close all;clear% all;
addpath(genpath('../src'));
addpath(genpath('../src/MEX_Files'));
%%% Correct the path of your data
h=tic;
%svreg 
%spectrum_surf 
generate_stats_xls_v2 'C:\Users\ajoshi\Downloads\FewvoxEdit\FewvoxEdit\Edit Extraction\MPRAGE_high_res_lowered_res_fewvox'
toc(h)

% Note that extensions of files is omitted in the command.
% The Execution of the SVREG sequence takes 60-90 minutes.
% Incase you have problems, 
% please contact Anand A Joshi
% ajoshi@sipi.usc.edu



