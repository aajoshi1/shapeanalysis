%||AUM|| ||Shree Ganeshaya Namaha||
clc;clear all;close all;opengl software;
subbasename='/ifs/enigma/ychou/julio/Reg700/8003001_GW/8003001_GW.bse';
addpath(genpath('/ifs/ccb/CCB_SW_Tools/VolumeTools/AnandJoshi/cvs_sandbox/svreg/src'));
%load(sprintf('%s_curvesdist.mat',subbasename));


for jjj=1:26%length(Left.GPS)
    fp=fopen('/ifshome/ajoshi/twins.txt','r');
    ns=0;
    while ~feof(fp)
        
        subbasename=fscanf(fp,'%s',1);
        if ~exist(sprintf('%s_curvesdist_%d_refined.mat',subbasename,jjj),'file')
            continue;
        end
        cl=readdfc_sipi([subbasename,'.left.mapped.refined.dfc']);
        cr=readdfc_sipi([subbasename,'.left.mapped.refined.dfc']);
        
        if length(cr{jjj})<5 | length(cl{jjj})<5
            continue;
        end
        
        ns=ns+1;
        
        load(sprintf('%s_curvesdist_%d_refined.mat',subbasename,jjj));
        aa=interp1(1:length(ind2),sqrt(sum((right_coordinates(ind2,:)-left_coordinates).^2,2)),linspace(1,length(ind2),100));
        dist_left_right{jjj}(:,ns)=aa;%sqrt(sum((right_coordinates(ind2,:)-left_coordinates).^2,2));
        aa=interp1(1:length(ind),sqrt(sum((right_coordinates-left_coordinates(ind,:)).^2,2)),linspace(1,length(ind),100));
        dist_right_left{jjj}(:,ns)=aa;%sqrt(sum((right_coordinates-left_coordinates(ind,:)).^2,2));

        
        cr{jjj}(:,1)=220-cr{jjj}(:,1);
        cl1=interp1(1:length(cl{jjj}(ind,:)),cl{jjj}(ind,:),linspace(1,length(cl{jjj}(ind,:)),100));
        cr1=interp1(1:length(cr{jjj}),cr{jjj},linspace(1,length(cr{jjj}),100));

        cl1=affine_reg_ptset(cl1,cr1);
        
        dist_euclidean{jjj}(:,ns)=sqrt(sum((cl1-cr1).^2,2));%sqrt(sum((right_coordinates(ind2,:)-left_coordinates).^2,2));

        cl1=interp1(1:length(cl{jjj}),cl{jjj},linspace(1,length(cl{jjj}),100));
        cr1=interp1(1:length(cr{jjj}),cr{jjj},linspace(1,length(cr{jjj}),100));
        
        cl1=affine_reg_ptset(cl1,cr1);
        dist_euclidean_match_euclidean{jjj}(:,ns)=sqrt(sum((cl1-cr1).^2,2));%sqrt(sum((right_coordinates(ind2,:)-left_coordinates).^2,2));
        
        right_coordinates=interp1(1:length(right_coordinates),right_coordinates,linspace(1,length(right_coordinates),100));
        left_coordinates=interp1(1:length(left_coordinates),left_coordinates,linspace(1,length(left_coordinates),100));

        dist_GPS_match_euclidean{jjj}(:,ns)=sqrt(sum((right_coordinates-left_coordinates).^2,2));

    end
    jjj
    fclose(fp);
end
%save tmp

for jj=1:length(dist_left_right)
    indd=find(sum(dist_left_right{jj})==0);dist_left_right{jj}(:,indd)=[];
end

for jj=1:length(dist_right_left)
    indd=find(sum(dist_right_left{jj})==0);dist_right_left{jj}(:,indd)=[];
end

for jj=1:26
    if isempty(dist_left_right{jj})
        continue;
    end
    dist_lr(:,jj)=trimmean(dist_left_right{jj},80,2);
    dist_rl(:,jj)=trimmean(dist_right_left{jj},80,2);
    dist_lr1(:,jj)=dist_left_right{jj}(:,1);
    dist_rl1(:,jj)=dist_right_left{jj}(:,1);
end
figure;imagesc(dist_lr);
figure;imagesc(dist_rl);
% a=mean(dist_right_left,3);
% figure;imagesc(a);

atl=readdfs('/ifshome/ajoshi/AnandJoshi/cvs_sandbox/svreg/HD_Atlas/mri.left.mid.cortex.dfs');
cl=readdfc_sipi('/ifshome/ajoshi/AnandJoshi/cvs_sandbox/svreg/HD_Atlas/mri.left.dfc');

view_patch(atl); hold on;
clr=jet(1000);
for kk=1:26
    nc=interp1(1:length(cl{kk}),cl{kk},linspace(1,length(cl{kk}),100));
    ind_c{kk}=dsearchn(atl.vertices,nc);
end

atls=smooth_cortex_fast(atl,.1,3000);view_patch(atls);hold on;
for kk=1:26
    mysphere(atls.vertices(ind_c{kk},:),2,clr(round(1+(999/max(dist_lr(:)))*dist_lr(:,kk)),:),10)
end

dist_lr_main=dist_lr+dist_rl;
dist_left_right_main=dist_left_right;
dist_right_left_main=dist_right_left;
dist_euclidean_main=dist_euclidean;
dist_euclidean_match_euclidean_main=dist_euclidean_match_euclidean;
dist_GPS_match_euclidean_main=dist_GPS_match_euclidean;
%clear dist*
%load l_atlasr_dist_refined
load ll_1sub_dist_refined

dist_lr_atlas=dist_lr+dist_rl;
dist_left_right_atlas=dist_left_right;
dist_right_left_atlas=dist_right_left;
dist_euclidean_atlas=dist_euclidean;
dist_euclidean_match_euclidean_atlas=dist_euclidean_match_euclidean;
dist_GPS_match_euclidean_atlas=dist_GPS_match_euclidean;

view_patch(atls); hold on;
clr=jet(1000);clr(1,:)=0;
r=dist_lr_main./dist_lr_atlas;r=max(min(r,1),0);%r(r>.9)=0;
for kk=1:26
    mysphere(atls.vertices(ind_c{kk},:),2,clr(round(1+(999)*r(:,kk)),:),10)
end

for jj=1:26
if    isempty(dist_left_right_main{jj}) | isempty(dist_left_right_atlas{jj})
    continue;
end
    [h{jj},p{jj}]=ttest2(dist_left_right_main{jj}',dist_left_right_atlas{jj}',0.0001);
    [he{jj},pe{jj}]=ttest2(dist_euclidean_main{jj}',dist_euclidean_atlas{jj}',0.0001);
    [hee{jj},pee{jj}]=ttest2(dist_euclidean_match_euclidean_main{jj}',dist_euclidean_match_euclidean_atlas{jj}',0.0001);
    [heg{jj},peg{jj}]=ttest2(dist_GPS_match_euclidean_main{jj}',dist_GPS_match_euclidean_atlas{jj}',0.0001);

end

view_patch(atls); hold on;
for kk=1:26
    if isempty(h{kk})
        continue;
    end
    r=mean(dist_lr_main(:,kk),2)./mean(dist_lr_atlas(:,kk),2);r=max(min(r,1),0);%r(r>.9)=0;
    mysphere(atls.vertices(ind_c{kk},:),2,clr(round(999-(700)*r.*h{kk}').*h{kk}'.*(r>.1)+1,:),10)
end


r=dist_euclidean_main./dist_euclidean_atlas;r=max(min(r,1),0);%r(r>.9)=0;


view_patch(atls); hold on;
for kk=1:26
    if isempty(he{kk})
        continue;
    end
    re=mean(dist_euclidean_main{kk},2)./mean(dist_euclidean_atlas{kk},2);re=max(min(re,1),0);%r(r>.9)=0;

    mysphere(atls.vertices(ind_c{kk},:),2,clr(round(999-(700)*re.*he{kk}').*he{kk}'.*(re>.1)+1,:),10)
end
%ttest2(dist_lr_atlas


view_patch(atls); hold on;
for kk=1:26
    if isempty(hee{kk})
        continue;
    end
    re=mean(dist_euclidean_match_euclidean_main{kk},2)./mean(dist_euclidean_match_euclidean_atlas{kk},2);re=max(min(re,1),0);%r(r>.9)=0;    
    mysphere(atls.vertices(ind_c{kk},:),2,clr(round(999-(700)*re.*her{kk}').*her{kk}'.*(re>.1)+1,:),10);
end


view_patch(atls); hold on;
for kk=1:26
    if isempty(heg{kk})
        continue;
    end
    re=mean(dist_GPS_match_euclidean_main{kk},2)./mean(dist_GPS_match_euclidean_atlas{kk},2);re=max(min(re,1),0);%r(r>.9)=0;    
    mysphere(atls.vertices(ind_c{kk},:),2,clr(round(999-(700)*re.*heg{kk}').*heg{kk}'.*(re>.1)+1,:),10);
end



