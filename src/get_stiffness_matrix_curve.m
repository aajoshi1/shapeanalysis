%||AUM||
%||Shree Ganeshaya Namaha||

function [S]=get_stiffness_matrix_curve(curve)
%This finds stiffness matrix for triangular elements using techniques from
%book by Sadiku: Numerical Techniques Formulas 6.17,6.18

% A=tri_area(surf1.faces,surf1.vertices);
% 
% 
% f=surf1.faces;v=surf1.vertices;
% 
% C11e=(1./(4.*A)).*(v(f(:,2),2)- v(f(:,3),2)).^2
% 




V1=curve.faces(:,1);V2=curve.faces(:,2);
XY=curve.vertices(V1,:)-curve.vertices(V2,:);
L=sqrt(sum(XY.^2,2));

rows=[V1;V1;V2;V2];

cols=[V1;V2;V1;V2];

vals=[1./L;-1./L;-1./L;1./L];


S=sparse(rows,cols,vals);

