%||AUM|| ||Shree Ganeshaya Namaha||
clc;clear all;close all;opengl software;
%subbasename='/ifs/enigma/ychou/julio/Reg700/8003001_GW/8003001_GW.bse';
addpath(genpath('/ifs/ccb/CCB_SW_Tools/VolumeTools/AnandJoshi/cvs_sandbox/svreg/src'));
%load(sprintf('%s_curvesdist.mat',subbasename));



    fp=fopen('/ifshome/ajoshi/twins.txt','r');
    
    

    ns=0;
    while ~feof(fp)
        
        subbasename=fscanf(fp,'%s',1);
        if ~exist([subbasename,'.left.mapped.refined.dfc'],'file')| ~exist([subbasename,'.right.mapped.refined.dfc'],'file')
            continue;
        end
            cl=readdfc_sipi([subbasename,'.left.mapped.refined.dfc']);
            cr=readdfc_sipi([subbasename,'.right.mapped.refined.dfc']);

        ns=ns+1;
    for jjj=1:26%length(Left.GPS)        
                %load(sprintf('%s_curvesdist_right_atlas_%d.mat',subbasename,jjj));
                if length(cr{jjj})<5 | length(cl{jjj})<5
                    continue;
                end
        cr{jjj}(:,1)=220-cr{jjj}(:,1);
        cl1=interp1(1:length(cl{jjj}),cl{jjj},linspace(1,length(cl{jjj}),100));
        cr1=interp1(1:length(cr{jjj}),cr{jjj},linspace(1,length(cr{jjj}),100));

        dist_left_right{jjj}(:,ns)=sqrt(sum((cl1-cr1).^2,2));%sqrt(sum((right_coordinates(ind2,:)-left_coordinates).^2,2));

        %aa=interp1(1:length(ind),sqrt(sum((right_coordinates-left_coordinates(ind,:)).^2,2)),linspace(1,length(ind),100));
        
        dist_right_left{jjj}(:,ns)=sqrt(sum((cl1-cr1).^2,2));%sqrt(sum((right_coordinates-left_coordinates(ind,:)).^2,2));
        
    end
   % jjj
    end
    fclose(fp);

%save tmp_euclidean
for jj=1:26
    if isempty(dist_left_right{jj})
        continue;
    end
    dist_lr(:,jj)=trimmean(dist_left_right{jj},80,2);
    dist_rl(:,jj)=trimmean(dist_right_left{jj},80,2);

end
figure;imagesc(dist_lr);
figure;imagesc(dist_rl);
% a=mean(dist_right_left,3);
% figure;imagesc(a);

 atl=readdfs('/ifshome/ajoshi/AnandJoshi/cvs_sandbox/svreg/HD_Atlas/mri.left.mid.cortex.dfs');
 cl=readdfc_sipi('/ifshome/ajoshi/AnandJoshi/cvs_sandbox/svreg/HD_Atlas/mri.left.dfc');
% 
% view_patch(atl); hold on;
 clr=jet(1000);
 for kk=1:26
     nc=interp1(1:length(cl{kk}),cl{kk},linspace(1,length(cl{kk}),100));
     ind_c{kk}=dsearchn(atl.vertices,nc);
 end
% 
 atls=smooth_cortex_fast(atl,.1,3000);view_patch(atls);hold on;
% for kk=1:26
%     mysphere(atls.vertices(ind_c{kk},:),2,clr(round(1+(999/max(dist_lr(:)))*dist_lr(:,kk)),:),10)
% end


% dist_lr_main=dist_lr;
% 
% save lr_dist_euclidean_refined dist_lr dist_left_right dist_right_left


dist_lr_main=dist_lr+dist_rl;
dist_left_right_main=dist_left_right;
dist_right_left_main=dist_right_left;

%load l_atlasr_dist_refined
load lr_sub1_dist_euclidean_refined

dist_lr_atlas=dist_lr+dist_rl;
dist_left_right_atlas=dist_left_right;
dist_right_left_atlas=dist_right_left;

view_patch(atls); hold on;
clr=jet(1000);
r=dist_lr_main./dist_lr_atlas;r=max(min(r,1),0);
for kk=1:26
    mysphere(atls.vertices(ind_c{kk},:),2,clr(round(1+(999)*r(:,kk)),:),10)
end

for jj=1:26
if    isempty(dist_left_right_main{jj}) | isempty(dist_left_right_atlas{jj})
    continue;
end
    [h{jj},p{jj}]=ttest2(dist_left_right_main{jj}',dist_left_right_atlas{jj}');
end

view_patch(atls); hold on;
for kk=1:26
    if isempty(h{kk})
        continue;
    end
    mysphere(atls.vertices(ind_c{kk},:),2,clr(round(1+(999)*h{kk}),:),10)
end

%ttest2(dist_lr_atlas


