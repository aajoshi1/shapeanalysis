
clc;clear all;%close all;
opengl software;

%fn=dir('../data/24brains/*_lh_*8.mat');
load fnlist
for kk1=1:length(fn)
    kk2=kk1;
    left_vs_left_differentsub(['../data/24brains/',fn(kk1).name],['../data/24brains/',fn(kk2).name]);close all;drawnow;  
    right_vs_right_differentsub(['../data/24brains/',fn(kk1).name],['../data/24brains/',fn(kk2).name]);close all;drawnow;  
end

for kk1=1:length(fn)
    for kk2=1:length(fn)
        if kk1~=kk2
            left_vs_left_differentsub(['../data/24brains/',fn(kk1).name],['../data/24brains/',fn(kk2).name]);close all;drawnow;  
            right_vs_right_differentsub(['../data/24brains/',fn(kk1).name],['../data/24brains/',fn(kk2).name]);close all;drawnow;  
        end
    end
end

