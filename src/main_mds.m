%||AUM||
clc;clear all;close all;
fnames={'CC1.mat','CC2.mat','CC3.mat','Ellipse.mat'};
for jj=1:4
    load(fnames{jj});Sig=real(Sig(2:end));Sigjj=1./sqrt(Sig);
    for kk=1:4
        load(fnames{kk});Sig=real(Sig(2:end));
        Sigkk=1./sqrt(Sig);
        D(jj,kk)=(norm(Sigjj-Sigkk));
    end
end

[X]=mdscale(D,2);

figure;hold on; axis equal;
plot(X(1,1),X(1,2),'*r','MarkerSize',30);

plot(X(2,1),X(2,2),'*g','MarkerSize',30);

plot(X(3,1),X(3,2),'*b','MarkerSize',30);

plot(X(4,1),X(4,2),'*m','MarkerSize',30);