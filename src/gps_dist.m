function gps_dist=gps_dist(surf_atlas,surf_sub)

L=loreta(surf_atlas);
T=DelaunayTri(surf_sub.vertices);

for kk=1:250%150
% tic
%     k=dsearchn(surf_atlas.vertices,surf_sub.vertices);
% toc
% %k is an index into atlas surface
% 
%     [vec_atlas_pts,ind]=unique(k);
% %put edge length penalty
%     vec_atlas2sub=surf_sub.vertices(ind,:)-surf_atlas.vertices(vec_atlas_pts,:);
    vec_atlas2sub = l2dist_asy(surf_atlas.vertices,surf_sub.vertices,T.Triangulation);
