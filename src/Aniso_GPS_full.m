%||AUM||
%||Shree Ganeshaya Namaha||
function Aniso_GPS_full(subbasename)
% clc;close all;clear all;
% addpath(genpath('../src'));
% addpath(genpath('../vol_src'));
%subbasename1{1}='/home/ajoshi/cvs_sandbox/svreg/HD_Atlas/mri.left.mid.cortex.dfs';
subbasename1{1}=[subbasename,'.left.mid.cortex.reg.dfs'];
subbasename1{2}=[subbasename,'.right.mid.cortex.reg.dfs'];
tarbasename1{1}=[subbasename,'.tar.left.mid.cortex.reg.dfs'];
tarbasename1{2}=[subbasename,'.tar.right.mid.cortex.reg.dfs'];

NCu=7;NumEig=16;
for jj1=1:2
    subbasename=subbasename1{jj1};
    tarbasename=tarbasename1{jj1};
    so1=readdfs(subbasename);clear GPS;
    to1=readdfs(tarbasename);clear GPS;

%   h= view_patch(so1);
    flabels=median(so1.labels(so1.faces),2);
    aa=unique(flabels);%h=figure;
    aa=aa(aa<600 & aa>100);GPS_surf=zeros(length(so1.vertices),NumEig);
    GPS_tar=zeros(length(to1.vertices),NumEig);
    
%     for roiid1=1:length(aa)
%         roiid=aa(roiid1);
        s1=so1;
%         s1.faces(flabels~=roiid,:)=[];
      %  s1=myclean_patch3(s1);
        s1=myclean_patch_cc(s1);
        [s1sm,~,C]=smooth_cortex_fast(s1,.1,200);
        curvt=curvature_cortex_fast(s1sm,50,0,C);
        %view_patch(s1);
        
        B=get_mass_matrix_tri(s1);
        bdr=boundary_vertices(s1);
        inds=[1:length(s1.vertices)]';
        inds(bdr)=[];
        
        %[A,Dx,Dy]=get_stiffness_matrix_tri_wt(s1,ones(length(s1.vertices),1));
        [A,Dx,Dy]=get_stiffness_matrix_tri_wt(s1,(0.51+curvt).^.3);
        A=.5*(A+A');B=.5*(B+B');
        [EVec1,Eval]=eigs(A,B,NumEig+1,'sm');
        EVec1=real(EVec1);Eval=abs(Eval);
        %[EVec,Eval]=eigs(A(inds,inds),B(inds,inds),6,'sm');
        %EVec1(inds,:)=EVec;EVec1(bdr,:)=0;

        colr=prism(length(aa)*NCu);
        if corr(EVec1(:,2),s1.vertices(:,2))>0
            EVec1(:,2)=-EVec1(:,2);
        end
        
        GPS=EVec1(:,2:end)*(sqrt(Eval(2:end,2:end)^-1));
        surfpatch=s1;
        %[~,ind1]=intersect(so1.vertices,s1.vertices,'rows');
        GPS_surf=GPS;  
        
%     end
%    view(-90,0);camlight;zoom;
    for jjj1=1:size(GPS_surf,2)
        GPS_tar(:,jjj1)=mygriddata(so1.u',so1.v',GPS_surf(:,jjj1),to1.u',to1.v');
    end
    
    save([subbasename(1:end-4),'.Aniso_GPS.mat'],'GPS_surf','surfpatch','GPS_tar');
    %save([tarbasename(1:end-4),'.GPS.mat'],'GPS','GPS_tar','surfpatch');
    
end

