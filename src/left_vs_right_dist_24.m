function left_vs_right_dist_24(subbasename)

    Left=load(subbasename);
    subbasenamer=strrep(subbasename,'_lh_','_rh_');
    Right=load(subbasenamer);

close all; drawnow;
for jjj=length(Left.curves):-1:1
kkk=jjj;
if length(Right.curves) <26 | length(Right.curves)<26
    continue;
end
if isempty(Left.curves{jjj}) | isempty(Right.curves{jjj} )
    continue;
end
l{kkk}=Left.curves{jjj}.vertices;
r{kkk}=Right.curves{jjj}.vertices;

lgnth=2^size(Right.GPS{jjj},2);
aqq=1e100;
r_param=param_curve(r{jjj});l_param=param_curve(l{kkk});

l_param=[1:length(l_param)]'/length(l_param);
r_param=[1:length(r_param)]'/length(r_param);

for kk=0:lgnth-1
    aaa=dec2bin(kk,size(Right.GPS{jjj},2));
    
    for k=1:length(aaa)
        bbb(k)=str2num(aaa(k));
    end
    
    aaa=diag(2*(bbb-.5));
    Mx=3*max(abs(Left.GPS{jjj}(:,1)));
    [aq,pm,indmov,indfxed]=l2dist([Left.GPS{jjj},Mx*l_param],[Right.GPS{kkk}*aaa,Mx*r_param]);
    if aq<aqq
        aqq=aq;
        ind=indmov;
        ind2=indfxed;
        sgn1=aaa;
    end
end
l_param(ind);
lll=medfilt1(l_param(ind(2:end-1))',3);
 p=polyfit(1:length(ind)-2,medfilt1(l_param(ind(2:end-1))'),10);
 l_param(ind(2:end-1))=polyval(p,1:length(ind)-2);
% 
 p=polyfit(1:length(ind2)-2,medfilt1(r_param(ind2(2:end-1))'),10);
 r_param(ind2(2:end-1))=polyval(p,1:length(ind2)-2);
 
 left_coordinates=[Left.GPS{jjj},Mx*l_param];
 right_coordinates=[Right.GPS{kkk}*sgn1,Mx*r_param];
 
save(sprintf('%s_curvesdist_%d_refined.mat',subbasename,jjj),'ind','ind2','sgn1','Mx','left_coordinates','right_coordinates');


kkk
jjj
close all;drawnow;
end


