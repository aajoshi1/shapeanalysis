
clc;clear all;close all;
addpath(genpath('../../src'));

[m1.vertices,nrm,m1.faces]=ReadObjShape('data/michael_1.obj'); m1=myclean_patch_cc(m1);
view_patch(m1);view(0,0);camlight;lighting gouraud; material dull;

[m2.vertices,nrm,m2.faces]=ReadObjShape('data/michael_2.obj');m2=myclean_patch_cc(m2);
view_patch(m2);view(0,0);camlight; lighting gouraud;material dull;
%m2=m1;
[GPS_surf_m1,gps_spectr1,curv1] = aniso_gps_patch(m1,71);


figure;
patch('faces',m1.faces,'vertices',m1.vertices,'facevertexcdata',curv1,'edgecolor','none','facecolor','interp'); axis equal;axis off;
view(0,0);camlight; material dull;lighting gouraud;zoom(3);

figure;
patch('faces',m1.faces,'vertices',m1.vertices,'facevertexcdata', GPS_surf_m1(:,1) ,'edgecolor','none','facecolor','interp'); axis equal;axis off;
view(0,0);camlight; material dull;lighting gouraud;zoom(3);


[GPS_surf_m2,gps_spectr2,curv2] = aniso_gps_patch(m2,71);
h=figure;
patch('faces',m2.faces,'vertices',m2.vertices,'facevertexcdata',curv2,'edgecolor','none','facecolor','interp'); axis equal;axis off;
view(0,0);camlight; material dull;lighting gouraud;zoom(3);

h=figure;
patch('faces',m2.faces,'vertices',m2.vertices,'facevertexcdata',-GPS_surf_m2(:,1) ,'edgecolor','none','facecolor','interp'); axis equal;axis off;
view(0,0);camlight; material dull;lighting gouraud;zoom(3);



diffsp=gps_spectr1.vec*(gps_spectr1.coeff-gps_spectr2.coeff);


ind=dsearchn([abs(GPS_surf_m1),m1.vertices],[abs(GPS_surf_m2),m2.vertices]);
dist_gps=sqrt(sum((abs(GPS_surf_m1(ind,:))-abs(GPS_surf_m2)).^2,2));

figure;
patch('faces',m2.faces,'vertices',m2.vertices,'facevertexcdata', abs(dist_gps) ,'edgecolor','none','facecolor','interp'); axis equal;axis off;%caxis([0,1]);
view(0,0);camlight; material dull;lighting gouraud;zoom(3);colorbar;%caxis([0,.25]);
saveas(h,'aniso_GPS_diff2.png');

