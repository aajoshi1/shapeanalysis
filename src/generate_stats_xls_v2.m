%||AUM||
%||Shree Ganeshaya Namaha||

function generate_stats_xls_v2(subbasename)


%l=readdfs([subbasename,'.tar.left.mid.cortex.reg.dfs']);
%r=readdfs([subbasename,'.tar.right.mid.cortex.reg.dfs']);
l=readdfs([subbasename,'.left.mid.cortex.reg.dfs']);
r=readdfs([subbasename,'.right.mid.cortex.reg.dfs']);

gunzip([subbasename,'.svreg.label.nii.gz']);
vl=load_nii([subbasename,'.svreg.label.nii']);
delete([subbasename,'.svreg.label.nii']);

labs=unique(l.labels);labs=[labs;unique(r.labels)];labs=unique(labs);labs=sort(labs);labs(labs==0)=[];
labs=reshape(labs,[length(labs),1]);
labs=labs(labs>=100);
labs=labs(labs<600);
labs_sub=sort(unique(vl.img(:)));
labs_sub=labs_sub(labs_sub>=600);
%labs_sub=labs_sub(labs_sub<100);

th_roi=zeros(length(labs),1);

for jj=1:length(labs)
   th_roi(jj)=mean([l.attributes(l.labels==labs(jj));r.attributes(r.labels==labs(jj))]); 
  % roi_id(jj)=labs(jj);
end




gunzip([subbasename,'.pvc.frac.nii.gz']);
vgm=load_nii([subbasename,'.pvc.frac.nii']);
delete([subbasename,'.pvc.frac.nii']);

vgm.img=(vgm.img~=0).*(1-abs(vgm.img-2));
gmv=zeros(length(labs),1);
for jj=1:length(labs)
   ind=(vl.img==labs(jj)); %=mean([l.attributes(l.labels==labs(jj));r.attributes(r.labels==labs(jj))]); 
   gmv(jj)=sum(vgm.img(ind))*vl.hdr.dime.pixdim(2)*vl.hdr.dime.pixdim(3)*vl.hdr.dime.pixdim(4);
  % roi_id(jj)=labs(jj);
end

for jj=1:length(labs_sub)
   ind=find(vl.img==labs_sub(jj)); %=mean([l.attributes(l.labels==labs(jj));r.attributes(r.labels==labs(jj))]); 
   labs_vol(jj)=length(ind)*vl.hdr.dime.pixdim(2)*vl.hdr.dime.pixdim(3)*vl.hdr.dime.pixdim(4);
  % roi_id(jj)=labs(jj);
end



fp=fopen([subbasename,'.roiwise.stats.txt'],'w');

fprintf(fp,'ROI_ID\tMean_Thickness(mm)\tGM_Volume(mm^3)\n');
for jj=1:length(labs)
fprintf(fp,'%d\t%.4f\t%.4f\n',labs(jj),th_roi(jj),gmv(jj));
end


fprintf(fp,'ROI_ID\t ROI_Volume(mm^3)\n');
for jj=1:length(labs_sub)
fprintf(fp,'%d\t%.4f\n',labs_sub(jj),labs_vol(jj));
end



fclose(fp);


