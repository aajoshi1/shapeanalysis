%||AUM||
%||Shree Ganeshaya Namaha||
clc;clear all;close all;


%close all;
mz=fopen('/ifshome/ajoshi/MZ.txt');
dz=fopen('/ifshome/ajoshi/DZ.txt');

kk=1;
while ~feof(mz)
  mz1{kk}=fscanf(mz,'%s\n',1);kk=kk+1;
end
    
for kk=1:2:length(mz1)
    if exist(['/ifs/enigma/ychou/julio/Reg700/',mz1{kk},'/',mz1{kk},'.bse.left.mapped.mat'],'file')
        Left1{(kk-1)/2+1}=load(['/ifs/enigma/ychou/julio/Reg700/',mz1{kk},'/',mz1{kk},'.bse.left.mapped.mat']);
    else
        Left1{(kk-1)/2+1}.Sig=[];
    end
    
    if exist(['/ifs/enigma/ychou/julio/Reg700/',mz1{kk},'/',mz1{kk},'.bse.right.mapped.mat'],'file')
        Right1{(kk-1)/2+1}=load(['/ifs/enigma/ychou/julio/Reg700/',mz1{kk},'/',mz1{kk},'.bse.right.mapped.mat']);
    else
        Right1{(kk-1)/2+1}.Sig=[];
    end
    
    if exist(['/ifs/enigma/ychou/julio/Reg700/',mz1{kk+1},'/',mz1{kk+1},'.bse.left.mapped.mat'],'file')
        Left2{(kk-1)/2+1}=load(['/ifs/enigma/ychou/julio/Reg700/',mz1{kk+1},'/',mz1{kk+1},'.bse.left.mapped.mat']);
    else
        Left2{(kk-1)/2+1}.Sig=[];
    end
    if exist(['/ifs/enigma/ychou/julio/Reg700/',mz1{kk+1},'/',mz1{kk+1},'.bse.right.mapped.mat'],'file')
        Right2{(kk-1)/2+1}=load(['/ifs/enigma/ychou/julio/Reg700/',mz1{kk+1},'/',mz1{kk+1},'.bse.right.mapped.mat']);    
    else
        Right2{(kk-1)/2+1}.Sig=[];
    end

end



for jjj=1:length(Left1)
if ~isempty(Left1{jjj}.Sig) & ~isempty(Left2{jjj}.Sig)
    for kkk=1:min(length(Left1{jjj}.Sig),length(Left2{jjj}.Sig))
    if length(Left1{jjj}.Sig{kkk}).*length(Left2{jjj}.Sig{kkk})>0
        mz_left_dst(jjj,kkk)=sum((1./sqrt(Left1{jjj}.Sig{kkk})-1./sqrt(Left2{jjj}.Sig{kkk})).^2);
    end
    end
else
    mz_left_dst(jjj,:)=0;
end
end
fclose(mz);




for jjj=1:length(Right1)
if ~isempty(Right1{jjj}.Sig) & ~isempty(Right2{jjj}.Sig)
    for kkk=1:min(length(Right1{jjj}.Sig),length(Right2{jjj}.Sig))
    if length(Right1{jjj}.Sig{kkk}).*length(Right2{jjj}.Sig{kkk})>0
        mz_right_dst(jjj,kkk)=sum((1./sqrt(Right1{jjj}.Sig{kkk})-1./sqrt(Right2{jjj}.Sig{kkk})).^2);
    end
    end
else
    mz_right_dst(jjj,:)=0;
end
end






save mz_left_dst mz_left_dst
save mz_right_dst mz_right_dst

