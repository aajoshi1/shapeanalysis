% Copyright 2010 Anand A. Joshi, David W. Shattuck and Richard M. Leahy 
% This file is part SVREG.
% 
% SVREG is free software: you can redistribute it and/or modify
% it under the terms of the GNU Lesser General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
% 
% SVREG is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU Lesser General Public License for more details.
% 
% You should have received a copy of the GNU Lesser General Public License
% along with SVREG.  If not, see <http://www.gnu.org/licenses/>.

function [nrm,Anrm] = colnorm(A);

[m,n] = size(A);

if(m>1),			% multiple rows
  nrm = sqrt(sum([A.*conj(A)]));
else				% A is row vector
  nrm = abs(A);			% just return mag of each column
end

if(nargout > 1),
  ndx = find(nrm>0);		% any zero norm?
  Anrm = zeros(size(A));
  % normalize any non-zero columns
  Anrm(:,ndx) = A(:,ndx) ./ nrm(ones(m,1),ndx);
end

return
