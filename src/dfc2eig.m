%||AUM||
%||Shree Ganeshaya Namaha||
% opengl software
% clc;clear all;close all;
function dfc2eig(fname,NEIG)
fname
crv=readdfc_sipi(fname);
for kkk=1:length(crv)
if length(crv{kkk})<15
    continue;
end
curve.faces=[[1:length(crv{kkk})-1]',[2:length(crv{kkk})]'];
curve.vertices=double(crv{kkk});
CC1=double(crv{kkk});

% vc=vertices_connectivity_curve(curve1);
% bdr=trace_boundary(1,vc,curve1);
% 
% curve.faces=[bdr,[bdr(2:end);bdr(1)]]
% curve.vertices=curve1.vertices;
%plot_curve(curve);
%close all;
% bdr=bdr(1:round(length(bdr)/50):end)
% %CC1=CC3(:,[2,3]);
% CC1=curve.vertices(bdr,:);
% theta=linspace(0,2*pi,500);theta=theta(1:end-1);
%    y=cos(theta);%+.01*rand(size(theta));
%    x=2*sin(theta);CC1=[x',y'];[b,m,n]=unique(round(1000*CC1),'rows');CC1=CC1(sort(m),:);
CC1=double(crv{kkk});
     %  CC1=CC1(1:5:end,:); 

      [b,m,n]=unique(round(1000*CC1),'rows');CC1=CC1(sort(m),:);
% % % 
    x=CC1(:,1)';y=CC1(:,2)';z=CC1(:,3)';
    curve.vertices=[x',y',z'];
   curve.faces=[[1:length(x)-1]',[2:length(x)]'];%curve.faces=[curve.faces;[length(x),1]];
windowSize = 5;
p=polyfit([1:length(curve.vertices)]',curve.vertices(:,1),20);
curve.vertices(:,1)=polyval(p,[1:length(curve.vertices)]')

p=polyfit([1:length(curve.vertices)]',curve.vertices(:,2),20);
curve.vertices(:,2)=polyval(p,[1:length(curve.vertices)]')

p=polyfit([1:length(curve.vertices)]',curve.vertices(:,3),20);
curve.vertices(:,3)=polyval(p,[1:length(curve.vertices)]')


[cuv,tau,T,N,B,s,ds] = frenetframe(curve.vertices,-1e300);
 %cuv=  curvature_curve(curve);cuv(1)=cuv(2);cuv(end)=cuv(end-1);%cuv=abs(cuv);
 cuv(isnan(cuv))=eps;cuv(isinf(cuv))=eps;cuv=abs(cuv);
 d=[cuv(1);cuv;cuv(end)];
  % d=sum(cuv(curve.faces),2);%d=ones(size((d)));
   M=get_mass_matrix_curve_aniso(curve,1);
   S=get_stiffness_matrix_curve_aniso(curve,d);
  % S=get_stiffness_matrix_curve(curve);
   
 %cuv=10*x  ;
 %cuv=cuv;
 %C=diag(cuv);S=C*S;
   opts.tol=1e-200;A=full(M^-1*S);
   A(1,:)=[];A(end,:)=[];A(:,1)=[];A(:,end)=[];
   [E,lambda]=eigs(sparse(A),NEIG,'sm',opts);
  E=[zeros(1,size(E,2));E;zeros(1,size(E,2))];
   %[E,lambda]=eigs(M^-1*S,15,'sm',opts);
   E=real(E);E2=E;
   for kk=1:size(E,2)
       E(:,kk)=(E(:,kk))./norm(E(:,kk));
       E2(:,kk)= E(:,kk)./sqrt(lambda(kk,kk));
   end
   colr=jet(1000);
    E1=abs(E)-min(abs(E(:)));E1=1000*E1./max(E1(:));E1=round(E1); E1=max(min(E1,1000),1);
    
   hh= figure;
    for kk1=1:min(NEIG,4)
        E1(:,kk1)=round(950*E1(:,kk1)/max(E1(:,kk1)))+1;
        subplot(1,min(NEIG,4),kk1);
        plot_curve(curve,colr(E1(:,kk1),:),hh);axis off;
    end
    EigVal{kkk}=diag(lambda);%close all;
    EigVec{kkk}=E*size(E,1);
    GPS{kkk}=EigVec{kkk}*diag(sqrt(1./EigVal{kkk}));%Left.Sig{kkk}(1:2);Right.Sig{kkk}=Right.Sig{kkk}(1:2);
    curves{kkk}=curve;
    save([fname(1:end-3),'mat'],'EigVal','EigVec','GPS','curves');
end
%    
%    row=[1:length(x)-1,2:length(x),1,length(x)];col=[2:length(x),1:length(x)-1,length(x),1]; dat=1*ones(size(row));
%    
%    L=sparse(row,col,dat);
%    
%    L=L-2*speye(size(L));
%    opt.tol=1e-100;
%    [E,l]=eigs(-L,10,'sm',opt);
%    diag(l)
%    
%       E=E-min(E(:));E=1000*E./max(E(:));E=round(E); E=max(min(E,1000),1);
%    plot_curve(curve,colr(E(:,10),:));axis off;
%  