
function spectrum_surf(subbasename)

dfc2eig_GPS([subbasename,'.left.mapped.dfc'],7);
dfc2eig_GPS([subbasename,'.right.mapped.dfc'],7);
ROIwise_GPS(subbasename,7);
Iso_GPS(subbasename,7);
Aniso_GPS(subbasename,7);
ROIwise_GPS_Iso(subbasename,7);
