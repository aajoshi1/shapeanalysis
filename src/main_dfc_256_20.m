%||AUM||
%||Shree Ganeshaya Namaha||
clc;clear all;close all;
opengl software;

% dfc2eig('/home/ajoshi/for_hanna/automatic/2125c/2125c.left.mapped.dfc');
% dfc2eig('/home/ajoshi/for_hanna/automatic/2125c/2125c.right.mapped.dfc');
dfc2eig('/home/ajoshi/for_hanna/with_sulci/2125c/2125c.left.dfc');
dfc2eig('/home/ajoshi/for_hanna/with_sulci/2125c/2125c.right.dfc');
l=readdfc_sipi('/home/ajoshi/for_hanna/with_sulci/2125c/2125c.left.dfc')
r=readdfc_sipi('/home/ajoshi/for_hanna/with_sulci/2125c/2125c.right.dfc')
%close all;


% Left=load('/home/ajoshi/for_hanna/automatic/2125c/2125c.left.mapped.mat');
% Right=load('/home/ajoshi/for_hanna/automatic/2125c/2125c.right.mapped.mat');

Left=load('/home/ajoshi/for_hanna/with_sulci/2125c/2125c.left.mat');
Right=load('/home/ajoshi/for_hanna/with_sulci/2125c/2125c.right.mat');


close all; drawnow;
%for jjj=1:length(Left.EigVal)
%for kkk=1:length(Left.EigVal)
jjj=1;kkk=1;

lgnth=2^size(Right.GPS{jjj},2);
aqq=1e100;
for kk=0:0%lgnth-1
    aaa=dec2bin(kk,size(Right.GPS{jjj},2));
    
    for k=1:length(aaa)
        bbb(k)=str2num(aaa(k));
    end
    
    aaa=diag(2*(bbb-.5));

    [aq,pm,indmov,indfxed]=l2dist(Left.GPS{jjj},Right.GPS{kkk});
    if aq<aqq
        aqq=aq;
        ind=indmov;
        ind2=indfxed;
        sgn1=aaa;
    %aqq=min(aq,aqq);
    end
end

r_param=param_curve(r{jjj});l_param=param_curve(l{kkk});

%l_param(ind);
%lll=medfilt1(l_param(ind(2:end-1))',3);
p=polyfit(1:length(ind)-2,medfilt1(l_param(ind(2:end-1))'),20);
l_param(ind(2:end-1))=polyval(p,1:length(ind(2:end-1)));

p=polyfit(1:length(ind2)-2,medfilt1(r_param(ind2(2:end-1))'),20);
r_param(ind2(2:end-1))=polyval(p,1:length(ind2(2:end-1)));



figure;line(l{1}(:,1),l{1}(:,2),l{1}(:,3));
figure;line(l{1}(ind,1),l{1}(ind,2),l{1}(ind,3));
figure;line(r{1}(:,1),r{1}(:,2),r{1}(:,3));

figure;line([r{1}(ind2,1),l{1}(:,1)],[r{1}(ind2,2),l{1}(:,2)],[r{1}(ind2,3),l{1}(:,3)]);
hold on;
plot([r{1}(ind2,1),l{1}(:,1)],[r{1}(ind2,2),l{1}(:,2)],[r{1}(ind2,3),l{1}(:,3)],'.');
dst(jjj,kkk)=aqq;

%     if length(Left.Sig{jjj}).*length(Right.Sig{jjj})>0
%         dst(jjj,kkk)=sum((1./sqrt(Left.Sig{kkk})-1./sqrt(Right.Sig{jjj})).^2)
%     end
kkk
jjj
% end
% end
% 
% for jjj=1:length(Left.Sig)
% for kkk=1:length(Left.Sig)
%     
%     
% Left.Sig{kkk}=Left.Sig{kkk}(1:2);Right.Sig{kkk}=Right.Sig{kkk}(1:2);
% l2dist()
%     if length(Left.Sig{jjj}).*length(Right.Sig{jjj})>0
%         dst(jjj,kkk)=sum((1./sqrt(Left.Sig{kkk})-1./sqrt(Right.Sig{jjj})).^2)
%     end
% end
% end
% Left.Sig
% Right.Sig
close all;figure;
imagesc(dst)


