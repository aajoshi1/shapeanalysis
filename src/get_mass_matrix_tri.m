%||AUM||
%||Shree Ganeshaya Namaha||

function M=get_mass_matrix_tri(surf1)

A=tri_area(surf1.faces,surf1.vertices);%A=ones(size(A));

V1=surf1.faces(:,1);V2=surf1.faces(:,2);V3=surf1.faces(:,3);

rows=[V1;V1;V1;V2;V2;V2;V3;V3;V3];

cols=[V1;V2;V3;V1;V2;V3;V1;V2;V3];

vals=[A./6;A./12;A./12;A./12;A./6;A./12;A./12;A./12;A./6];


M=sparse(rows,cols,vals);

