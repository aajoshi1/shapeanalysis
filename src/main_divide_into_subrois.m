% Copyright 2012 Anand A. Joshi, David W. Shattuck and Richard M. Leahy 
% This file is part SVREG.
% 

clc;close all;clear% all;
addpath(genpath('../src'));
addpath(genpath('../src/MEX_Files'));
%%% Correct the path of your data
h=tic;
NDIV=10;
%svreg 'C:\Users\ajoshi\Downloads\FewvoxEdit\FewvoxEdit\Edit Extraction\MPRAGE_high_res_lowered_res_fewvox'
l=readdfs('C:\Users\ajoshi\Documents\git_sandbox\svreg-matlab\HD_Atlas2\mri.right.mid.cortex.reg.dfs');
lab=unique(l.labels);lab(lab<100)=[];lab(lab>600)=[];
load('C:\Users\ajoshi\Documents\git_sandbox\svreg-matlab\HD_Atlas2\mri.right.mid.cortex.reg.ROIwise_GPS.mat');
l2=l;
for kk=1:length(lab)
    ind=find(l.labels==lab(kk));
    sdiv=linspace(min(GPS_surf(ind ,1)),max(GPS_surf(ind,1)),NDIV+1);
    for jj=1:NDIV
        ind2=find(GPS_surf(ind,1)>sdiv(jj)& GPS_surf(ind,1)<sdiv(jj+1));
        l2.labels(ind(ind2)) = 10*l.labels(ind(ind2))+jj;
    end
end

h=figure;
patch('faces',l2.faces,'vertices',l.vertices,'facevertexcdata',l2.labels,'edgecolor','none','facecolor','flat');axis equal;colormap colorcube; view(-90,0);
camlight;material dull; axis off;zoom(2);
saveas(h,'atlas_10xrois.png');
view(90,0);camlight;axis off;%zoom(2);
saveas(h,'atlas_10xrois_2.png');
labels=l2.labels;
save right.mat labels
writedfs('C:\Users\ajoshi\Documents\git_sandbox\svreg-matlab\HD_Atlas2\mri.right.mid.cortex.reg10.dfs',l2);
