% Copyright 2010 Anand A. Joshi, David W. Shattuck and Richard M. Leahy 
% This file is part SVREG.
% 
% SVREG is free software: you can redistribute it and/or modify
% it under the terms of the GNU Lesser General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
% 
% SVREG is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU Lesser General Public License for more details.
% 
% You should have received a copy of the GNU Lesser General Public License
% along with BSE.  If not, see <http://www.gnu.org/licenses/>.

function [Curves,hdr]=readdfc_sipi(fname)
% Author Anand A. Joshi
% Reads curves from DFC files
%disp(sprintf('saved xml file in the same dir as dfc file...'));

fid=fopen(fname,'rb','ieee-le');

if (fid<0) error('unable to open file'); end;

%hdr.magic = ['D' 'U' 'F' 'F' 'S' 'U' 'R' 'F']';

hdr.magic=char(fread(fid,8,'char'));
hdr.version=fread(fid,4,'char');
hdr.hdrsize=fread(fid,1,'int32');
hdr.dataStart=fread(fid,1,'int32');
hdr.mdoffset=fread(fid,1,'int32');
hdr.pdoffset=fread(fid,1,'int32');
hdr.nContours=fread(fid,1,'int32');

fseek(fid,hdr.mdoffset,'bof');
Mdata=char(fread(fid,hdr.dataStart-hdr.mdoffset,'char'));
if exist([fname(1:end-4),'.xml'],'file')
    delete([fname(1:end-4),'.xml']);
end
fod=fopen([fname(1:end-4),'.xml'],'w','ieee-le');
fprintf(fod,Mdata);
fclose(fod);

fseek(fid,hdr.dataStart,'bof');
Curves = cell(hdr.nContours,1); % one empty cell per curve
for ctno=1:hdr.nContours
    nopts=fread(fid,1,'int32');
    XYZ=fread(fid,3*nopts,'float');
    XYZ=(reshape(XYZ,3,nopts))';
    Curves{ctno}=XYZ;
end


fclose(fid);

if length(Curves)==28
    fprintf('The file %s is traced using 28 curve protocol\n',fname);
    Curves2 = cell(26,1); % one empty cell per curve
    for cnum=1:25
        Curves2{cnum}=Curves{cnum};
    end
    for cnum=15:26
        Curves2{cnum}=Curves{cnum+1};
    end
    writedfc([fname(1:end-4),'26.dfc'],Curves2,[fname(1:end-4),'.xml']);
    Curves=Curves2;
elseif length(Curves)==26
    fprintf('The file %s is traced using 26 curve protocol\n',fname);
end

%Curves=orderCurves([fname(1:end-4),'.xml'],Curves);