%||AUM||
%||Shree Ganeshaya Namaha||
opengl software
clc;clear all;close all;

theta=linspace(0,2*pi,500);theta=theta(1:end-1);
   x=cos(theta+1);%+.01*rand(size(theta));
   y=2*sin(theta);
   curve.vertices=[x',y'];
   curve.faces=[[1:length(x)-1]',[2:length(x)]'];curve.faces=[curve.faces;[length(x),1]];
   

 cuv=  curvature_curve(curve);cuv(1)=cuv(2);cuv(end)=cuv(end-1);%cuv=abs(cuv);
   d=sum(cuv(curve.faces),2);%d=ones(size((d)));
   M=get_mass_matrix_curve_aniso(curve,d);
   S=get_stiffness_matrix_curve_aniso(curve,d);
  % S=get_stiffness_matrix_curve(curve);
   
 %cuv=10*x  ;
 %cuv=cuv;
 %C=diag(cuv);S=C*S;
   opts.tol=1e-300;
   [E,lambda]=eigs(S,M,15,'sm',opts);
   E=real(E);
   for kk=1:size(E,2)
       E(:,kk)=(E(:,kk))./norm(E(:,kk));
        E2(:,kk)= E(:,kk)./sqrt(lambda(kk,kk));
   end
   colr=jet(1000);
    E1=E-min(E(:));E1=1000*E1./max(E1(:));E1=round(E1); E1=max(min(E1,1000),1);
 for kk=1:size(E,2)
    plot_curve(curve,colr(E1(:,kk),:));axis off;
 end
 
 
 aa=kmeans(E2(:,2:end),2);
 
 figure;hold on;plot(x(aa==1),y(aa==1),'.r');plot(x(aa==2),y(aa==2),'.b');
hold on;plot(x(aa==3),y(aa==3),'.g');plot(x(aa==4),y(aa==4),'.k');axis equal;
diag(lambda)
%    
%    row=[1:length(x)-1,2:length(x),1,length(x)];col=[2:length(x),1:length(x)-1,length(x),1]; dat=1*ones(size(row));
%    
%    L=sparse(row,col,dat);
%    
%    L=L-2*speye(size(L));
%    opt.tol=1e-100;
%    [E,l]=eigs(-L,10,'sm',opt);
%    diag(l)
%    
%       E=E-min(E(:));E=1000*E./max(E(:));E=round(E); E=max(min(E,1000),1);
%    plot_curve(curve,colr(E(:,10),:));axis off;
%  