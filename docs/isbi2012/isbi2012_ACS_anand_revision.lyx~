#LyX 2.0 created this file. For more info see http://www.lyx.org/
\lyxformat 413
\begin_document
\begin_header
\textclass spconf
\use_default_options true
\maintain_unincluded_children false
\language english
\language_package default
\inputencoding auto
\fontencoding global
\font_roman default
\font_sans default
\font_typewriter default
\font_default_family default
\use_non_tex_fonts false
\font_sc false
\font_osf false
\font_sf_scale 100
\font_tt_scale 100

\graphics default
\default_output_format default
\output_sync 0
\bibtex_command default
\index_command default
\paperfontsize default
\spacing single
\use_hyperref false
\papersize letterpaper
\use_geometry false
\use_amsmath 1
\use_esint 1
\use_mhchem 1
\use_mathdots 1
\cite_engine basic
\use_bibtopic false
\use_indices false
\paperorientation portrait
\suppress_date false
\use_refstyle 0
\index Index
\shortcut idx
\color #008000
\end_index
\secnumdepth 3
\tocdepth 3
\paragraph_separation indent
\paragraph_indentation default
\quotes_language english
\papercolumns 1
\papersides 1
\paperpagestyle default
\tracking_changes false
\output_changes false
\html_math_output 0
\html_css_as_file 0
\html_be_strict false
\end_header

\begin_body

\begin_layout Title
Geodesic curvature flow on surfaces for automatic sulcal delineation
\end_layout

\begin_layout Author
Anand
\begin_inset space ~
\end_inset

A.
\begin_inset space ~
\end_inset

Joshi
\begin_inset Formula $^{1,3}$
\end_inset

, David
\begin_inset space ~
\end_inset

W.
\begin_inset space ~
\end_inset

Shattuck
\begin_inset Formula $^{2}$
\end_inset

, Hanna
\begin_inset space ~
\end_inset

Damasio
\begin_inset Formula $^{3}$
\end_inset

 and Richard
\begin_inset space ~
\end_inset

M.
\begin_inset space ~
\end_inset

Leahy
\begin_inset Formula $^{1}$
\end_inset


\end_layout

\begin_layout Affiliation

\size small
\begin_inset Formula $^{1}$
\end_inset

Signal and Image Processing Institute, University of Southern California,
 Los Angeles, CA 90089, USA 
\begin_inset Newline newline
\end_inset


\begin_inset Formula $^{2}$
\end_inset

Laboratory of Neuro Imaging, University of California, Los Angeles, CA 90095,
 USA 
\begin_inset Newline newline
\end_inset


\begin_inset Formula $^{3}$
\end_inset

Brain and Creativity Institute, University of Southern California, Los Angeles,
 CA 90089, USA 
\begin_inset Foot
status open

\begin_layout Plain Layout
This work is supported in part by NIH grants P41 RR013642 and R01 NS074980
 
\end_layout

\end_inset


\end_layout

\begin_layout Abstract
Sulcal folds (sulci) on the cortical surface are important landmarks of
 interest for investigating brain development and disease.
 Accurate and automatic delineation of the sulci is a challenging problem
 due to substantial variability in their shapes across populations.
 We present a geodesic curvature flow method for an automatic and accurate
 delineation of sulcal curves.
 We assume as input an atlas brain surface mesh on which a set of sulcal
 curves have been delineated.
 The sulcal curves are transferred to approximate corresponding locations
 on the subject brain using a transformation defined by an automatic surface
 based registration method.
 The locations of these curves are then refined to follow the true sulcal
 fundi more closely using geodesic curvature flow on the cortical surface.
 We present a level set based formulation of this flow on non-flat surfaces
 which represents the sulcal curves as zero level sets.
 We also incorporate a curvature based weighting that drives the sulcal
 curves to the bottoms of the sulcal valleys in the cortical folds.
 The resulting PDE is discretized on a triangulated mesh using finite elements.
 Finally, we present a validation by comparing sets of automatically delineated
 sulcal curves with sets of manually delineated sulcal curves and show that
 the proposed method is able to find them accurately.
\end_layout

\begin_layout Standard
\begin_inset ERT
status collapsed

\begin_layout Plain Layout


\backslash
begin{keywords}
\end_layout

\end_inset

 brain imaging, cortical surface, geodesic curvature flow, sulcal curves,
 level set 
\begin_inset ERT
status collapsed

\begin_layout Plain Layout


\backslash
end{keywords}
\end_layout

\end_inset

 
\end_layout

\begin_layout Standard
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
vspace{-.3cm}
\end_layout

\end_inset


\end_layout

\begin_layout Section
Introduction
\end_layout

\begin_layout Standard
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
vspace{-.1cm}
\end_layout

\end_inset

Human cerebral cortex is often modeled as a highly convoluted sheet of gray
 matter enclosing the white matter fiber connections.
 Sulcal folds or sulci are fissures in the cortical surface and are commonly
 used as surrogates for the cytoarchitectural boundaries in the brain.
 Therefore sulcal curves are frequently used as landmarks for surface registrati
ons.
 There is also great interest in analyzing the geometry of these curves
 directly for studies of disease propagation, symmetry, development and
 group differences (e.g.
 
\begin_inset CommandInset citation
LatexCommand cite
key "prince_aging,Narr01"

\end_inset

).
 The sulcal curves required for these studies can be produced using manual
 or automatic delineation, with manual delineation often achieved using
 interactive software tools 
\begin_inset CommandInset citation
LatexCommand cite
key "shattuck_sulci_paper"

\end_inset

.
 However this can be a tedious and subjective task that also requires substantia
l knowledge of neuroanatomy.
 Additionally, intra- and inter-rater variability is introduced in these
 curves due to subjective choices made by the user.
 This variability is reduced to some extent using rigorous definitions of
 a sulcal tracing protocol and extensive training as described in 
\begin_inset CommandInset citation
LatexCommand cite
key "dimitrios_nimg_sulci,shattuck_sulci_paper"

\end_inset

.
 We previously developed a semiautomatic method using Dijkstra’s algorithm
 to find weighted shortest paths on the cortical surface where the weights
 depend on curvature 
\begin_inset CommandInset citation
LatexCommand cite
key "shattuck_sulci_paper"

\end_inset

.
 Alternative automatic approaches to sulcal delineation include active shape-bas
ed models 
\begin_inset CommandInset citation
LatexCommand cite
key "vaillant1997finding"

\end_inset

 where sulci are modeled by deforming a surface as opposed to the curves.
 Another approach involved active shape models 
\begin_inset CommandInset citation
LatexCommand cite
key "tao2002using"

\end_inset

 where flow of the curves was induced on a spherical parametric representation.
 
\end_layout

\begin_layout Standard
An alternative approach to this problem is to use automatic surface registration
 to align curvature or depth 
\begin_inset CommandInset citation
LatexCommand cite
key "fischel_hbm"

\end_inset

.
 Sulcal curves can be delineated on a reference atlas brain surface, which
 is then aligned with the subject brain surface using automated registration.
 The sulci from the atlas are then transferred to the subject using the
 point correspondence defined by the surface mapping.
 While this approach can find the sulcal location approximately, there is
 often a significant residual error 
\begin_inset CommandInset citation
LatexCommand cite
key "dimitrios_nimg_sulci"

\end_inset

.
 This is because automatic methods align the whole surface using curvature
 and do not focus specifically on the sulcal locations.
 Also, the variability of the atlas and subject folds can result in the
 misalignment of sulcal curves, thus the transferred sulcal curves may not
 lie at the valleys of the subject surfaces.
 A refinement of these curves can alleviate this problem.
 This paper describes a new method based on geodesic curvature flow on surfaces
 where the sulcal curves are represented as curvature weighted geodesics.
 Geodesic curvature flow was described for parametric surfaces in 
\begin_inset CommandInset citation
LatexCommand cite
key "spira2002geodesic"

\end_inset

.
 Here we present a level set based formulation similar to 
\begin_inset CommandInset citation
LatexCommand cite
key "wu2010level,laiautomated"

\end_inset

 and apply it to the sulcal detection problem.
 The sulcal curve evolution is defined in terms of evolution of a zero level
 set.
 The flow is discretized in the surface geometry using a finite element
 method.
 
\end_layout

\begin_layout Standard
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
vspace{-.4cm}
\end_layout

\end_inset


\end_layout

\begin_layout Section
Materials and Methods
\end_layout

\begin_layout Standard
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
vspace{-.2cm}
\end_layout

\end_inset

We assume, as input, an atlas brain surface mesh with manually delineated
 sulcal landmarks and a subject brain surface mesh.
 The goal is to delineate the corresponding sulci on the subject surface.
 First, we briefly describe an automated curvature based registration approach
 that performs alignment of the atlas surface and the subject surface.
 The sulci from the atlas surface are then transferred to the subject surface,
 which we refer to as `RT sulci' (
\bar under
R
\bar default
egistration 
\bar under
T
\bar default
ransfered sulci) henceforth.
 Then, the RT sulci are adjusted to their correct locations using geodesic
 curvature flow.
 We refer to these transferred sulci as `GCF sulci'.
 
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
vspace{-.3cm}
\end_layout

\end_inset


\end_layout

\begin_layout Standard
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
vspace{-.2cm}
\end_layout

\end_inset


\end_layout

\begin_layout Subsection
Cortical surface registration
\begin_inset CommandInset label
LatexCommand label
name "sub:Cortical-Surface-Registration"

\end_inset


\end_layout

\begin_layout Standard
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
vspace{-.2cm}
\end_layout

\end_inset

We briefly describe our automatic surface registration method, to be published
 elsewhere, as the main focus of this paper is geodesic curvature flow for
 sulcal detection.
 Other automated registration methods could also be used (e.g.
 FreeSurfer, BrainVISA or BrainVoyager) to transfer the atlas sulcal curves
 to their initial locations on the subject.
 We describe a method that establishes one-to-one correspondence between
 the atlas surface 
\begin_inset Formula $\mathcal{A}$
\end_inset

 and the subject surface 
\begin_inset Formula $\mathcal{S}$
\end_inset

.
 The method for surface registration has two stages: (i) for each subject,
 parameterize the surface of each cortical hemisphere to a unit square (ii)
 ﬁnd a vector ﬁeld with respect to this parameterization that aligns curvature
 of the surfaces.
 In order to generate such a parameterization, we model the cortical surface
 as an elastic sheet and solve the associated linear elastic equilibrium
 equation using the Finite Element Method (FEM) as described in 
\begin_inset CommandInset citation
LatexCommand cite
key "ajoshitmi07"

\end_inset

.
 We constrain the corpus callosum to lie on the boundary of the unit square
 mapped as a uniform speed curve.
 Then, an elastic energy minimization yields flat maps of the two cortical
 hemisphere surfaces to a plane (Fig.
 
\begin_inset CommandInset ref
LatexCommand ref
reference "Flo:Fig_curvature_based_automatic_surf_reg"

\end_inset

).
 A multiresolution representation of curvature for the subject and atlas
 is calculated.
 This is then aligned by minimizing a cost function with elastic energy
 as a regularizing penalty.
 The registration is made more robust by adding 3D coordinate matching as
 a mismatch penalty along with the curvature.

\series bold
 
\series default
This performs reparameterization of the cortical hemisphere surfaces and
 establishes a one to one point correspondence between subject and atlas
 surfaces.
 We used the BrainSuite software 
\begin_inset CommandInset citation
LatexCommand cite
key "bs2"

\end_inset

 to extract the tessellated cortical surface meshes for the atlas and for
 each subject from T1-weighted MRI volumes, as well as to interactively
 trace 26 candidate sulcal curves on the atlas brain according to the protocol
 described in 
\begin_inset CommandInset citation
LatexCommand cite
key "dimitrios_nimg_sulci"

\end_inset

.
 By using the point correspondence established using the registration, these
 curves are then transferred to the subject surfaces.
 The objective of the geodesic curvature flow method presented in the next
 section is to perform refinement of these RT sulci to find accurate sulcal
 curves in the subjects surface.
 
\end_layout

\begin_layout Standard
\begin_inset Float figure
wide false
sideways false
status open

\begin_layout Plain Layout
\begin_inset Graphics
	filename figures_isbi2012_fig1_new.png
	lyxscale 40
	width 75col%

\end_inset


\end_layout

\begin_layout Plain Layout
\begin_inset Caption

\begin_layout Plain Layout
Curvature based automatic surface registration
\end_layout

\end_inset


\begin_inset CommandInset label
LatexCommand label
name "Flo:Fig_curvature_based_automatic_surf_reg"

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Standard
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
vspace{-.1cm}
\end_layout

\end_inset


\end_layout

\begin_layout Subsection
Finite element method
\begin_inset CommandInset label
LatexCommand label
name "sub:Finite-element-method"

\end_inset


\end_layout

\begin_layout Standard
We use an FEM-based level set approach for computing the curvature flow,
 as we describe in Sec 
\begin_inset CommandInset ref
LatexCommand ref
reference "sub:Geodesic-curvature-flow"

\end_inset

.
 We first describe how we perform FEM discretization on a triangulated mesh
 of the partial derivatives 
\begin_inset CommandInset citation
LatexCommand cite
key "sadiku2000numerical"

\end_inset

.
 Let 
\begin_inset Formula $\alpha$
\end_inset

 be any piecewise linear real-valued scalar function deﬁned over the surface,
 and let 
\begin_inset Formula $x,y$
\end_inset

 denote local coordinates for triangle 
\begin_inset Formula $i$
\end_inset

.
 We can also denote the local coordinates of the three vertices of the triangle
 as 
\begin_inset Formula $(x_{1},y_{1})$
\end_inset

, 
\begin_inset Formula $(x_{2},y_{2})$
\end_inset

 and 
\begin_inset Formula $(x_{3},y_{3})$
\end_inset

 respectively.
 Since 
\begin_inset Formula $\alpha$
\end_inset

 is linear on the 
\begin_inset Formula $i^{th}$
\end_inset

 triangle, we can write,
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
vspace{-.3cm}
\end_layout

\end_inset


\end_layout

\begin_layout Standard

\size scriptsize
\begin_inset Formula 
\begin{eqnarray}
\alpha(x,y) & = & a_{0}^{i}+a_{1}^{i}x+a_{2}^{i}y.\label{eq:lineq}
\end{eqnarray}

\end_inset


\size default
Writing this expression at three vertices of the triangle 
\begin_inset Formula $i$
\end_inset

 in matrix form yields 
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
vspace{-.2cm}
\end_layout

\end_inset


\end_layout

\begin_layout Standard

\size scriptsize
\begin_inset Formula 
\begin{equation}
\underbrace{\begin{pmatrix}1 & x_{1}^{i} & y_{1}^{i}\\
1 & x_{2}^{i} & y_{2}^{i}\\
1 & x_{3}^{i} & y_{3}^{i}
\end{pmatrix}}_{D^{i}}\begin{pmatrix}a_{0}^{i}\\
a_{1}^{i}\\
a_{2}^{i}
\end{pmatrix}=\begin{pmatrix}\alpha^{i}(x_{1},y_{1})\\
\alpha^{i}(x_{2},y_{2})\\
\alpha^{i}(x_{3},y_{3})
\end{pmatrix}.\label{eq:mateq}
\end{equation}

\end_inset


\size default
The coefficients 
\begin_inset Formula $a_{0}^{i},a_{1}^{i}$
\end_inset

 and 
\begin_inset Formula $a_{2}^{i}$
\end_inset

 can be obtained by inverting the matrix 
\begin_inset Formula $D^{i}$
\end_inset

.
 From Eq.
 
\begin_inset CommandInset ref
LatexCommand ref
reference "eq:lineq"

\end_inset

 and by inverting the matrix in Eq.
 
\begin_inset CommandInset ref
LatexCommand ref
reference "eq:mateq"

\end_inset

, we obtain
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
vspace{-.2cm}
\end_layout

\end_inset


\end_layout

\begin_layout Standard

\size footnotesize
\begin_inset Formula 
\[
\begin{pmatrix}\frac{\partial\alpha^{i}}{\partial x}\\
\frac{\partial\alpha^{i}}{\partial y}
\end{pmatrix}=\begin{pmatrix}a_{1}^{i}\\
a_{2}^{i}
\end{pmatrix}=\frac{1}{|D^{i}|}\begin{pmatrix}y_{2}^{i}-y_{1}^{i} & y_{3}^{i}-y_{1}^{i} & y_{1}^{i}-y_{2}^{i}\\
x_{1}^{i}-x_{2}^{i} & x_{1}^{i}-x_{3}^{i} & x_{2}^{i}-x_{1}^{i}
\end{pmatrix}\begin{pmatrix}\alpha^{i}(x_{1},y_{1})\\
\alpha^{i}(x_{2},y_{2})\\
\alpha^{i}(x_{3},y_{3})
\end{pmatrix}.
\]

\end_inset


\size default
Denote the discretization of 
\begin_inset Formula $\frac{\partial}{\partial x}$
\end_inset

 and 
\begin_inset Formula $\frac{\partial}{\partial y}$
\end_inset

 at triangle 
\begin_inset Formula $i$
\end_inset

 by 
\begin_inset Formula $D_{x}^{i}$
\end_inset

 and 
\begin_inset Formula $D_{y}^{i}$
\end_inset

 respectively.
 Also note that 
\begin_inset Formula $|D^{i}|=2A_{i}$
\end_inset

 where 
\begin_inset Formula $A_{i}$
\end_inset

 is area of the 
\begin_inset Formula $i^{th}$
\end_inset

 triangle.
 Then we have:
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
vspace{-.5cm}
\end_layout

\end_inset


\end_layout

\begin_layout Standard

\size footnotesize
\begin_inset Formula 
\begin{eqnarray*}
D_{x}^{i} & = & \frac{1}{2A_{i}}\begin{pmatrix}y_{2}^{i}-y_{1}^{i} & y_{3}^{i}-y_{1}^{i} & y_{1}^{i}-y_{2}^{i}\end{pmatrix},\\
D_{y}^{i} & = & \frac{1}{2A_{i}}\begin{pmatrix}x_{1}^{i}-x_{2}^{i} & x_{1}^{i}-x_{3}^{i} & x_{2}^{i}-x_{1}^{i}\end{pmatrix}.
\end{eqnarray*}

\end_inset


\size default
These triangle-wise derivative operators can be assembled over the entire
 surface and can be written as 
\begin_inset Formula $D_{x}$
\end_inset

 and 
\begin_inset Formula $D_{y}$
\end_inset

 matrices.
 The derivative operator matrix is thus denoted by 
\begin_inset Formula $D=[D_{x},D_{y}]^{T}$
\end_inset

.
\end_layout

\begin_layout Subsection
Geodesic curvature flow on surfaces
\begin_inset CommandInset label
LatexCommand label
name "sub:Geodesic-curvature-flow"

\end_inset


\end_layout

\begin_layout Standard
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
vspace{-.1cm}
\end_layout

\end_inset

The RT curves are typically in the correct sulcal valley, but are not exactly
 at their desired locations at the bottoms of these valleys.
 We have found that sulci propagated by automatic registration generally
 lie within 3cm of the true sulcal valleys 
\begin_inset CommandInset citation
LatexCommand cite
key "dimitrios_nimg_sulci"

\end_inset

.
 Therefore, to reduce the computational load, we calculate a surface patch
 around the sulcus of interest using front propagation for 3cm (Fig.
 
\begin_inset CommandInset ref
LatexCommand ref
reference "Flo:Fig_phi0_curv"

\end_inset

).
 
\end_layout

\begin_layout Standard
\begin_inset Float figure
wide false
sideways false
status open

\begin_layout Plain Layout
\begin_inset Graphics
	filename fig1.png
	width 65col%

\end_inset


\end_layout

\begin_layout Plain Layout
\begin_inset Caption

\begin_layout Plain Layout
(a) Initial sulcal curve and signed distance function; (b) curvature weighting
 function 
\begin_inset Formula $f$
\end_inset

 shown as color-coded overlay on the surface patch around that sulcus.
\end_layout

\end_inset


\begin_inset CommandInset label
LatexCommand label
name "Flo:Fig_phi0_curv"

\end_inset


\end_layout

\end_inset

The geodesic curvature flow is performed over this surface patch around
 the sulcus of interest using a level set formulation.
 The approach presented here is based on 
\begin_inset CommandInset citation
LatexCommand cite
key "laiautomated"

\end_inset

, but in our case we add curvature weighting when computing minimizing geodesics.
 Assume 
\begin_inset Formula $\mathcal{M}$
\end_inset

 is a general 2D manifold representing the surface patch embedded in 
\begin_inset Formula $\mathbb{R}^{3}$
\end_inset

 and let 
\begin_inset Formula $\Gamma$
\end_inset

 be the sulcal curve on the surface.
 Let the curve 
\begin_inset Formula $\Gamma$
\end_inset

 be represented by the zero level set of a function 
\begin_inset Formula $\phi:\mathcal{M}\rightarrow\mathbb{R}$
\end_inset

, i.e., 
\begin_inset Formula $\Gamma=\{s:\phi(s)=0\}$
\end_inset

.
 Suppose that 
\begin_inset Formula $c:\mathcal{M}\rightarrow\mathbb{R}$
\end_inset

 is the curvature of the surface 
\begin_inset Formula $\mathcal{M}$
\end_inset

.
 We have found that, for sulcal tracing, a sigmoid function of the curvature
 works well as weights on the paths for sulcal tracing 
\begin_inset CommandInset citation
LatexCommand cite
key "shattuck_sulci_paper"

\end_inset

.
 Therefore, we define 
\begin_inset Formula $f(s)=\frac{1}{1+e^{-2c(s)}}$
\end_inset

 as the curvature based weights on the surfaces.
 We seek to minimize the weighted length of 
\begin_inset Formula $\Gamma$
\end_inset

 given by 
\begin_inset Formula $E(\Gamma)=\int_{\Gamma:\phi=0}fdS$
\end_inset

, where integration is computed over the surface at the curve points.
 Following 
\begin_inset CommandInset citation
LatexCommand cite
key "laiautomated,spira2002geodesic,wu2010level"

\end_inset

, the Euler-Lagrange equations for the energy functional minimization of
 
\begin_inset Formula $E(\Gamma)$
\end_inset

 yield 
\end_layout

\begin_layout Standard

\size small
\begin_inset Formula 
\[
\begin{cases}
-\text{div}\left(f\frac{\nabla\phi}{|\nabla\phi|}\right) & =0\\
\frac{\partial\phi}{\partial\overrightarrow{n}}|_{\partial\mathcal{M}} & =0,
\end{cases}
\]

\end_inset


\size default
 where 
\begin_inset Formula $\partial\mathcal{M}$
\end_inset

 is the boundary of 
\begin_inset Formula $\mathcal{M}$
\end_inset

 and 
\begin_inset Formula $\overrightarrow{n}$
\end_inset

 is the intrinsic outward normal of 
\begin_inset Formula $\partial\mathcal{M}$
\end_inset

.
 A gradient descent flow of the equation 
\begin_inset CommandInset citation
LatexCommand cite
key "cheng2002motion"

\end_inset

 is given by:
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
vspace{-.2cm}
\end_layout

\end_inset


\size footnotesize

\begin_inset Formula 
\[
\begin{cases}
\frac{\partial\phi}{\partial t}=|\nabla\phi|\text{div}\left(f\frac{\nabla\phi}{|\nabla\phi|}\right)\\
\frac{\partial\phi}{\partial\overrightarrow{n}}|_{\partial\mathcal{M}}=0\\
\phi(0)=\phi_{0}
\end{cases}
\]

\end_inset


\size default
where we choose 
\begin_inset Formula $\phi_{0}$
\end_inset

 to be a signed distance function from the initial sulcal curve.
 The boundary condition is discretized in a standard way in the finite element
 formulation and we refer the reader to 
\begin_inset CommandInset citation
LatexCommand cite
key "sadiku2000numerical"

\end_inset

 for details.
 Next, we use the divergence identity,
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
vspace{-.2cm}
\end_layout

\end_inset

 
\size footnotesize

\begin_inset Formula 
\[
\text{\text{div}}\left(f\nabla\phi\right)=\text{\text{div}}\left(f\nabla\phi\frac{|\nabla\phi|}{|\nabla\phi|}\right)=f\frac{\nabla\phi\cdot\nabla(|\nabla\phi|)}{|\nabla\phi|}+|\nabla\phi|\text{div}\left(f\frac{\nabla\phi}{|\nabla\phi|}\right).
\]

\end_inset


\size default
This gives
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
vspace{-.2cm}
\end_layout

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\begin{cases}
\frac{\partial\phi}{\partial t}=\text{div}\left(f\nabla\phi\right)+g(\phi)\\
g(\phi)=-f\frac{\nabla\phi}{|\nabla\phi|}\cdot\nabla\left(|\nabla\phi|\right)\\
\phi(0)=\phi_{0}
\end{cases}
\]

\end_inset

By using discretization of derivatives in Sec.
 
\begin_inset CommandInset ref
LatexCommand ref
reference "sub:Finite-element-method"

\end_inset

 and after performing implicit-explicit discretization of the time derivative,
 we get:
\family roman
\series medium
\shape up
\size normal
\emph off
\bar no
\noun off
\color none

\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
vspace{-.3cm}
\end_layout

\end_inset


\end_layout

\begin_layout Standard

\size footnotesize
\begin_inset Formula 
\[
\frac{\phi^{n}-\phi^{n-1}}{\Delta t}+\frac{D\cdot(fD\phi^{n})+D\cdot(fD\phi^{n-1})}{2}=Ag(\phi^{n-1}).
\]

\end_inset


\end_layout

\begin_layout Standard
Let 
\begin_inset Formula $B=D\cdot fD$
\end_inset

 be the weighted mass matrix and 
\begin_inset Formula $A$
\end_inset

 be the load matrix, such that for triangle 
\begin_inset Formula $i$
\end_inset

,
\size tiny

\begin_inset Formula $A^{i}=\frac{Area_{i}}{12}\begin{pmatrix}2 & 1 & 1\\
1 & 2 & 1\\
1 & 1 & 2
\end{pmatrix}.$
\end_inset


\end_layout

\begin_layout Standard
Therefore, in matrix form, we get
\family roman
\series medium
\shape up
\size normal
\emph off
\bar no
\noun off
\color none

\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
vspace{-.3cm}
\end_layout

\end_inset


\end_layout

\begin_layout Standard

\size scriptsize
\begin_inset Formula 
\[
B\phi^{n}+\frac{1}{2}\Delta tA\phi^{n}=B\phi^{n-1}-\frac{1}{2}\Delta tA\phi^{n-1}+\Delta tBg(\phi^{n-1}).
\]

\end_inset


\end_layout

\begin_layout Standard
Finally, after simplification, the curve evolution problem reduces to solving
 a linear matrix equation given by:
\family roman
\series medium
\shape up
\size normal
\emph off
\bar no
\noun off
\color none

\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
vspace{-.3cm}
\end_layout

\end_inset


\end_layout

\begin_layout Standard

\size scriptsize
\begin_inset Formula 
\[
\phi^{n}=(B+\frac{1}{2}\Delta tA)^{-1}\left((B-\frac{1}{2}\Delta tA)\phi^{n-1}+\Delta tBg(\phi^{n-1})\right).
\]

\end_inset


\end_layout

\begin_layout Standard
This system of equations is solved using a preconditioned conjugate gradient
 method with Jacobi preconditioner.
 The algorithm was implemented in Matlab.
 We choose 
\begin_inset Formula $\Delta t=.5$
\end_inset

 and the number of iterations 
\begin_inset Formula $N_{iter}=20$
\end_inset

.
 The algorithm takes approximately 1 hour per subject hemisphere for the
 refinement of all the sulci on an 8 core Intel i7 computer.
 The final GCF curves were extracted by finding the zero level set of the
 function 
\begin_inset Formula $\phi$
\end_inset

 after 
\begin_inset Formula $20$
\end_inset

 iterations.
\end_layout

\begin_layout Standard
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
vspace{-.3cm}
\end_layout

\end_inset


\end_layout

\begin_layout Section
Results
\end_layout

\begin_layout Standard
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
vspace{-.3cm}
\end_layout

\end_inset


\end_layout

\begin_layout Standard
In order to evaluate the performance of the method, we performed validation
 on a set of 
\begin_inset Formula $12$
\end_inset

 subject brains.
 We used the ICBM Single Subject Template as our atlas (
\begin_inset Flex URL
status open

\begin_layout Plain Layout

http://www.loni.ucla.edu/Atlases/Atlas_Detail.jsp?atlas_id=5
\end_layout

\end_inset

).
 We applied the BrainSuite software 
\begin_inset CommandInset citation
LatexCommand cite
key "bs2"

\end_inset

 to extract cortical surface meshes from the subject and atlas MRI data.
 BrainSuite includes a multistage cortical modeling sequence.
 First the brain is extracted from the surrounding skull and scalp tissues
 using a combination of edge detection and mathematical morphology.
 Next the intensities of the MRI are corrected for shading artifacts.
 Each voxel in the corrected image is then labeled according to tissue type
 using a statistical classiﬁer.
 A standard atlas with associated structure labels is aligned to the subject
 volume, providing a label for cerebellum, cerebrum, brainstem, and subcortical
 regions.
 These labels are combined with the tissue classiﬁcation to automatically
 identify the cerebral white matter, to ﬁll the ventricular spaces, and
 to remove the brainstem and cerebellum.
 This produces a volume whose boundary surface represents the outer white-matter
 surface of the cerebral cortex.
 Prior to tessellation, the topological defects are identiﬁed and removed
 automatically from the binary volume using a graph based approach.
 A tessellated isosurface of the resulting mask is then extracted to produce
 a genus zero surface.
 This surface is then expanded to identify the pial surface, i.e., the boundary
 between grey matter and CSF.
 The surfaces are then split into left and right hemispheres based on the
 registered atlas labels.
 
\end_layout

\begin_layout Standard
We delineated sulcal curves using BrainSuite’s interactive delineation tools
 
\begin_inset CommandInset citation
LatexCommand cite
key "shattuck_sulci_paper"

\end_inset

 following a sulcal protocol with 26 sulcal curves 
\begin_inset CommandInset citation
LatexCommand cite
key "dimitrios_nimg_sulci"

\end_inset

.
 These sulci are consistently seen in normal brains, and are distributed
 throughout the entire cortical surface.
 A thorough description of the sulcal curves with instructions on how to
 trace them is available on our web site (http://neuroimage.usc.edu/CurveProtocol.h
tml).
 We traced the curves on the midcortical surface because it provides better
 access to the depth of the sulci than the pial surface, and the valleys
 of the sulci are more convex than the white matter surface allowing more
 stable tracing of the curves.
 The same procedure was repeated on the single subject atlas.
 Next, we performed the subject to atlas registration as described in Sec.
 
\begin_inset CommandInset ref
LatexCommand ref
reference "sub:Cortical-Surface-Registration"

\end_inset

 and transferred the curves of the atlas to the subject brains.
 The transferred curves were refined using the geodesic curvature flow as
 discussed in Sec 
\begin_inset CommandInset ref
LatexCommand ref
reference "sub:Geodesic-curvature-flow"

\end_inset

.
 The evolution of one sulcal curve in shown in Fig.
 
\begin_inset CommandInset ref
LatexCommand ref
reference "Flo:sulcal evolution"

\end_inset

 (see http://sipi.usc.edu/~ajoshi/GCF_Sulci.html for an animation).
 
\end_layout

\begin_layout Standard
\begin_inset Float figure
wide false
sideways false
status open

\begin_layout Plain Layout
\begin_inset Graphics
	filename figures_isbi2012_fig1_2.png
	width 85col%

\end_inset


\end_layout

\begin_layout Plain Layout
\begin_inset Caption

\begin_layout Plain Layout
Evolution of the sulcal curve by geodesic curvature flow for different iteration
s.
 The curvature weighting function 
\begin_inset Formula $f$
\end_inset

 is shown as color-coded overlay
\end_layout

\end_inset


\begin_inset CommandInset label
LatexCommand label
name "Flo:sulcal evolution"

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Standard
To compare the alignment of transferred curves, as well as GCF curves, we
 mapped the 26 protocol curves from all subjects to the target surface.
 We then quantified their accuracy using their variance on the subject surface,
 which is estimated as follows.
 We use a distance measure based on the Hausdorff distance metric:
\family roman
\series medium
\shape up
\size normal
\emph off
\bar no
\noun off
\color none

\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
vspace{-.5cm}
\end_layout

\end_inset


\end_layout

\begin_layout Standard

\size footnotesize
\begin_inset Formula 
\[
d(D_{i},D_{j})=0.5\frac{1}{N}\sum_{p\in D_{i}}\underset{p\in D_{i}}{\text{min}}|p-q|+0.5\frac{1}{N}\sum_{p\in D_{j}}\underset{q\in D_{j}}{\text{min}}|p-q|
\]

\end_inset


\size default
where 
\begin_inset Formula $d(D_{i},D_{j})$
\end_inset

 is the distance between the pointsets on triangle mesh representing curves
 
\begin_inset Formula $D_{i}$
\end_inset

 and 
\begin_inset Formula $D_{j}$
\end_inset

.
 This distance is computed between subject's manual curve and RT curve,
 as well as manual curve and GCF curve.
 
\end_layout

\begin_layout Standard

\size scriptsize
\begin_inset Float table
wide false
sideways false
status open

\begin_layout Plain Layout

\series bold
\size scriptsize
\begin_inset Tabular
<lyxtabular version="3" rows="6" columns="3">
<features tabularvalignment="middle">
<column alignment="center" valignment="top" width="0pt">
<column alignment="center" valignment="top" width="0pt">
<column alignment="center" valignment="top" width="0pt">
<row>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" bottomline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\size scriptsize
manual vs RT(
\begin_inset Formula $mm$
\end_inset

)
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" bottomline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\size scriptsize
manual vs GCF(
\begin_inset Formula $mm$
\end_inset

)
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\size scriptsize
Cent.
 sulcus
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\size scriptsize
1.8(L), 2.1(R)
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\size scriptsize
1.4(L), 1.4(R)
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\size scriptsize
Sup.
 Front.
 sulcus
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\size scriptsize
2.7(L), 2.7(R)
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\size scriptsize
1.8(L), 1.3(R)
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\size scriptsize
Calc.
 sulcus
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\size scriptsize
2.3(L), 2.3(R)
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\size scriptsize
2.0(L), 2.1(R)
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\size scriptsize
Sup.
 Temp.
 sulcus
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\size scriptsize
4.2(L), 4.1(R)
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\size scriptsize
3.5(L), 3.6(R)
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" bottomline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\size scriptsize
Avg over all 26 sulci
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" bottomline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\size scriptsize
3.6(L), 3.9(R)
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" bottomline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\size scriptsize
2.7(L), 3.3(R)
\end_layout

\end_inset
</cell>
</row>
</lyxtabular>

\end_inset


\end_layout

\begin_layout Plain Layout

\size scriptsize
\begin_inset Caption

\begin_layout Plain Layout
Sulcal errors between manually delineated curves and automatically generated
 curves by registration and transfer (RT) and geodesic curvature flow refinement
 (GCF) measured by Hausdorff distance metric.
 The table shows mean error for
\series bold
 
\series default
cortical delineation of 12 subjects
\series bold
 
\series default
for both left (L) and right (R) hemispheres.
\end_layout

\end_inset


\begin_inset CommandInset label
LatexCommand label
name "Flo:table1"

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Standard
The results for some of the prominent sulci are presented in Table 
\begin_inset CommandInset ref
LatexCommand ref
reference "Flo:table1"

\end_inset

.
 It can be seen that the sulcal error is reduced substantially after geodesic
 curvature flow.
 This improvement is most pronounced in the sulci that are clearly defined
 by curvature extrema and shortest length paths on the cortex, such as central
 sulcus and superior frontal sulcus.
 
\end_layout

\begin_layout Standard
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
vspace{-.3cm}
\end_layout

\end_inset


\end_layout

\begin_layout Section
Conclusion
\end_layout

\begin_layout Standard
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
vspace{-.3cm}
\end_layout

\end_inset


\end_layout

\begin_layout Standard
This paper presents a method for accurate and automatic delineation of sulcal
 curves on human brain cortex which are important inputs for cortical shape
 analysis.
 We will test this method further with brains with disorders, but preliminary
 results show promising prospects for the method in case of small temporal
 lobe lesions.

\series bold
 
\series default
The level set approach and FEM formulation allowed us to perform geodesic
 curvature flow on the surface.
 We demonstrated the potential benefit of this method by applying it to
 a small population of brains and showed that the GCF curves are significantly
 more accurate than the RT curves.
 A more extensive validation is planned.
 
\end_layout

\begin_layout Standard

\size scriptsize
\begin_inset CommandInset bibtex
LatexCommand bibtex
bibfiles "ajoshibib"
options "IEEEbib"

\end_inset


\end_layout

\end_body
\end_document
