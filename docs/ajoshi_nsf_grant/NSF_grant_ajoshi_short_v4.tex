%% LyX 2.0.3 created this file.  For more info, see http://www.lyx.org/.
%% Do not edit unless you really know what you are doing.
\documentclass[11pt,letterpaper]{spie}
\usepackage[latin9]{inputenc}
\pagestyle{plain}
\usepackage{wrapfig}
\usepackage{amsmath}
\usepackage{graphicx}
\PassOptionsToPackage{normalem}{ulem}
\usepackage{ulem}

\makeatletter

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% LyX specific LaTeX commands.
\special{papersize=\the\paperwidth,\the\paperheight}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% User specified LaTeX commands.

%%%%%%%%% MASTER -- compiles the 4 sections



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                                                      %%
%%%%%%%%%% EXACT 1in MARGINS %%%%%%%                                   %%
\setlength{\textwidth}{6.5in}     %%                                   %%
\setlength{\oddsidemargin}{0in}   %% (It is recommended that you       %%
\setlength{\evensidemargin}{0in}  %%  not change these parameters,     %%
\setlength{\textheight}{8.5in}    %%  at the risk of having your       %%
\setlength{\topmargin}{0in}       %%  proposal dismissed on the basis  %%
\setlength{\headheight}{0in}      %%  of incorrect formatting!!!)      %%
\setlength{\headsep}{0in}         %%                                   %%
\setlength{\footskip}{.5in}       %%                                   %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%                                   %%
\newcommand{\required}[1]{\section*{\hfil #1\hfil}}                    %%
\renewcommand{\refname}{\hfil References Cited\hfil}                   %%
\bibliographystyle{plain}                                              %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%PUT YOUR MACROS HERE

%\includeonly{NSFsumm}

\makeatother

\begin{document}

\title{Brain Shape Analysis using Spectral Geometry}

\maketitle
Investigators: Anand A Joshi, David W Shattuck, Hanna Damasio and
Richard M Leahy

Program: NSF Cognitive Neuroscience: http://www.nsf.gov/funding/pgm\_summ.jsp?pims\_id=5316,
(application deadline Aug 27th) 


\subsection*{Project Summary}

The proposed research will develop a framework for invariant representations
of the shapes of brain structures using methods of spectral geometry.
Analysis of brain shape has extensive applications in the study of
disease progression, aging and brain asymmetry. A quantitative analysis
of the shapes of brain structures requires an invariant representation
the geometry of a structure rather than its Euclidean representation.
Present approaches do not reflect the shapes of these structures but
focus instead on gross features such as length, depth and 3D location.
The shape analysis of brainpresents a challenging problem since the
human cerebral cortex is a highly convoluted sheet whose folding patterns,
called sulci, offer rich anatomical information. For 1D curves (sulci)
and 2D cortical parcellations (ROI surfaces), we propose a model for
shape analysis based on spectral geometry. Shape quantification of
a manifold, often described as whether one `can hear the shape of
a drum', has led to development of spectral geometry as a subbranch
of differential geometry. In spectral geometry, scientists characterize
the shape of a manifold by studying the eigenspectrum of differential
operators on the manifold. For computer vision, the Global Point Signature
(GPS) representation of 2D surfaces \cite{Reuter2010,Rustamov2007}
helps quanitfy the geometric `shape' of an object by studying the
eigensystem of the Laplace-Beltrami operator. The method exploits
the isometry invariance of eigenfunctions, introducing an invariant
metric to quanitfy shape differences. This representation has desirable
properties, including stability, uniqueness and invariance to scaling
and isometric transformations. We propose the use of the anisotropic
Helmholtz equation to study the spectral geometry of curves and surfaces,
utilizing the anisotropy of such objects. In case of sulcal curves,
this anisotropy is introduced via the curvature of the sulcus. 

The \uline{intellectual merit} of the proposal comes from the unique
combination of theoretical, empirical, and software elements that
will substantially impact brain image analysis. The \emph{theoretical}
element relies on the development of spectral geometric methods for
analysis of curves and surfaces. We will investigate various spectral
operators and their eigenspecta to represent curves and surfaces.
We will also investigate an optimal way of utilizing these models
for specific neuroanatomical studies. The \emph{empirical} aspect
of our approach relies on using the invariant shape quantification
for applications such as hemispherical symmetry, genetic and environmental
influence (via twin studies), and alterations of sulcal patterns in
neuropathological conditions such as autism. These shape analysis
studies will evaluate the performance as well as the efficacy and
robustness of our methods. The \emph{software} element relies on the
development of open source implementations of the methods under this
grant and their integration into the BrainSuite software package.

The \uline{broader impact} of developing spectral methods for shape
analysis of the brain will be its ability to facilitate quantitative
analyses of neurological conditions marked my alterations in sulcal
patterning (e.g. autism). The proposed methods will work in conjunction
with existing methods (tensor-based / voxel-based morphometry) to
provide a powerful set of tools for quantitative shape analysis. These
tools can be applied on the cerebral cortex to study disease, development,
maturation, genetic analysis and group differences in sulcal patterning.


\subsection*{Shape Representation of Sulcal Curves and Cortical ROIs\label{sec:GPS-Coordinates-Representation}}

Spectral geometry provides the basis to study the eigenspectrum of
sulcal curves and ROI surfaces. We consider the 1D and 2D case and
propose a point-wise signature for the curves and surfaces. Motivated
by spectral theory and corresponding work on 2D surfaces \cite{Rustamov2007,Reuter2010},
we model cortical curves (surfaces) as inhomogeneous vibrating strings
(membranes). Their harmonic behavior is governed by the 1D (2D) Helmholtz
equation. Since our aim is to characterize the shape of a curve $C$
and ROI surface $S$, we use curvature $\kappa(s)$ to introduce anisotropy
into the equation. Similarly, for the surface, we use the mean curvature
$\kappa_{m}(s)$ as an anisotropy term:

\[
\begin{array}{cc}
\text{curve} & \text{surface}\\
\begin{cases}
\ensuremath{\frac{\partial}{\partial s}\kappa(s)\frac{\partial}{\partial s}\Phi_{i}(s)} & \ensuremath{\frac{\partial}{\partial s}\kappa(s)\frac{\partial}{\partial s}\Phi_{i}(s)}=\lambda_{i}\Phi_{i}(s)\\
\Phi_{i}(s)|_{\partial C} & =0
\end{cases},\forall s\in C & \begin{cases}
\ensuremath{\nabla\cdot\kappa_{m}(s)\nabla\Phi_{i}(s)} & =\lambda_{i}\Phi_{i}(s)\\
\Phi_{i}(s)|_{\partial S} & =0
\end{cases},\forall s\in S
\end{array}
\]


where $\partial C$ is the set of the endpoints of the curve $C$
and $\partial S$ is the boundary of the cortical ROI. Denote the
eigenfunctions of this equation by $\Phi_{i}$ with eigenvalues $\lambda_{i}$
ordered by magnitude. We define the embedding manifold in the spectral
domain by the map:

$\ensuremath{GPS(p)=\left(\frac{1}{\sqrt{\lambda_{1}}}\Phi_{1}(p),\frac{1}{\sqrt{\lambda_{2}}}\Phi_{2}(p),\frac{1}{\sqrt{\lambda_{3}}}\Phi_{3}(p),\text{\ensuremath{\ldots}}\right)}.$

Thus, each point of the curve is embedded into an infinite dimensional
space. Due to the fundamental theorem of curves (two unit-speed plane
curves which have the same curvature and torsion differ only by a
rigid transformation), curvature and torsion define the curve uniquely
up to rigid transformation. Furthermore, the curve can be recovered
from the embedding by first recovering the curvature and torsion,
and then using the Frenet-Serre formulas \cite{docarmo}. For 3D curves,
this requires curvature and torsion; however, the embedding defined
above is based on curvature alone because most sulcal curves have
negligible torsion. This embedding has many favorable properties.
First, its coordinates are isometry invariant as they depend only
on the derivatives and curvature, dependent only on the shape. Second,
scaling a 1D curve manifold by the factor $\alpha$ results in curvature
scaled by the factor $\alpha$. Therefore, by normalizing the eigenvalues,
we obtain scale-invariance. Third, changes of the curve's shape result
in continuous changes in its spectrum so the representation is robust.
Fourth, in the embedding space, the inner product is given by the
Green's function due to the identity: $G(x_{1},x_{2})=\sum_{i}\frac{\Phi_{i}(x_{1})\Phi_{i}(x_{2})}{\lambda_{i}}$.
As a result, the GPS representation encodes both local and global
shape information into the embedding. Finally, in this infinite dimensional
shape space, the metric is simply Euclidean. 

\begin{figure}
\centering{}\includegraphics[width=0.8\textwidth]{shape_analysis}\caption{(a) Inferior frontal sulcus highlighted in red; (b) first four color
coded GPS coordinates; (c) GPS representation plotted from end to
end of a sulcus; (d) automatically generated ROIs on cortical surface;
(e) first five color coded GPS coordinates.}
\end{figure}



\subsection*{Preliminary Results \label{sec:Results}}

In order to illustrate the potential of this method for morphometric
analysis, we applied it for symmetry detection. The preliminary study
consisted of 24 normal brains. We applied the BrainSuite surface extraction
sequence, followed by sulcal set and ROI parcellation generation \cite{ajoshiISBI12}.
This produced 24x2 cortical surface hemisphere representations with
26 sulci each. Next, the GPS coordinate representation was generated
for all curves as well as ROI surfaces. Symmetry between sulci and
between regions was estimated by comparingcorresponding sulci between
two hemispheres in the same subject \cite{ajoshi_miccai2012}. \begin{wrapfigure}[16]{o}{0.5\columnwidth}%
\includegraphics[width=0.45\textwidth]{Asymm_detection}

\caption{(a) Shape symmetry measure of the sulci plotted on a smooth representation
of an individual cortical surface. The black regions on the curves
indicate that a significant symmetry was not found for those points.
(b) Left to right hemisphere average shape difference is plotted as
a colorcoded overlay.}
\label{Fig 4: Symmetry} \end{wrapfigure}%
 The results of the symmetry mapping are shown in Fig. \ref{Fig 4: Symmetry}.
It is interesting to note that the post- and pre-central sulci, together
with the posterior segment of the superior temporal, the transverse
temporal, the middle temporal and the inferior occipital sulci in
the dorso-lateral view, show the maximal amount of left/right asymmetry.
On the mesial view the collateral, the supraorbital, the occipito-parietal
and long stretches of the cingulate sulci are also extremely asymmetric.
It is not surprising to see the cingulate sulcus (visible in the depth
of the mesial view) to show a great extent of relative symmetry. 


\subsection*{Proposed Extensions}

With the GPS representation of curves we can analyze shapes in their
native form. Thus, we can compare shapes without the effects of Euclidean
transformations or the Euclidean embedding. We propose three avenues
to increase the ability of scientists in evaluating the inherent structure
of shapes via the GPS representation.


\subsubsection*{Statistics of Shapes}

Given many samples of a shape, such as the cingulate, the Euclidean
representation does not satisfactorily aggregate and compare each
sample because it is sensitive to small changes in shape. With the
GPS representation, we have a common basis -- resistant to small shape
changes -- for estimation of the mean shape and the standard deviation
of shapes. We propose development of a statistical framework to analyze
the shape of a cortical structures. With this framework, we can then
characterize the gross features of a structure in the brain, including
its general shape and the variability, modes and principle components
of shape variation over a population. In addition, the statistics
we will provide can be used for analysis of a normal population against
a population with a specific trait, be it genetic, clinical, etc.
The statistics of shapes will allow scientists to quantify and analyze
the shape characteristics of a brain structures.


\subsubsection*{Anatomical Connectivity}

In recent years, connectivity has taken a central role in the processing
of brain data. In functional brain analysis, estimation of connectivity
provides a basis for inference on whether brain structures are related.
With the GPS representation, we can analyze the correlation between
two brain structures by their covariation in shape across a population.
If the change in shape of one structure in a subject is coupled with
the change in shape of the second, then we can infer that these two
brain areas are related in form. Since the GPS representation is a
spectral analysis tool, we can compare shapes at multiple resolutions.
We will use the GPS representation to determine structures that covary
across subjects, indicating they are anatomically linked. In addition,
with the multiple resolutions of the GPS representation, we can compare
shapes as to whether they covary at low spatial frequencies (global
shape) or high spatial frequencies (local shape).


\subsubsection*{Subcortical Shapes and Shape Bundles}

In our preliminary work, we focused on the shape of sulci on the cortex.
We will extend this work to subcortical structures such as the hippocampus.
In addition, the emerging field of diffusion tensor imaging provides
an estimate of white matter tract locations. We can extend GPS representations
to analyze the shapes of these tracts. Since the drawn tracts usually
occur in bundles, we plan to modify the GPS representation to account
for multiple tracts each representing one key white matter tract such
as the corpus callosum.

{\tiny \bibliographystyle{splncs03}
\bibliography{ajoshibib}
}
\end{document}
