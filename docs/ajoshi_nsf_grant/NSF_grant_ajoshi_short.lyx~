#LyX 2.0 created this file. For more info see http://www.lyx.org/
\lyxformat 413
\begin_document
\begin_header
\textclass spie
\begin_preamble

%%%%%%%%% MASTER -- compiles the 4 sections



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                                                      %%
%%%%%%%%%% EXACT 1in MARGINS %%%%%%%                                   %%
\setlength{\textwidth}{6.5in}     %%                                   %%
\setlength{\oddsidemargin}{0in}   %% (It is recommended that you       %%
\setlength{\evensidemargin}{0in}  %%  not change these parameters,     %%
\setlength{\textheight}{8.5in}    %%  at the risk of having your       %%
\setlength{\topmargin}{0in}       %%  proposal dismissed on the basis  %%
\setlength{\headheight}{0in}      %%  of incorrect formatting!!!)      %%
\setlength{\headsep}{0in}         %%                                   %%
\setlength{\footskip}{.5in}       %%                                   %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%                                   %%
\newcommand{\required}[1]{\section*{\hfil #1\hfil}}                    %%
\renewcommand{\refname}{\hfil References Cited\hfil}                   %%
\bibliographystyle{plain}                                              %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%PUT YOUR MACROS HERE

%\includeonly{NSFsumm}
\end_preamble
\use_default_options false
\maintain_unincluded_children false
\language english
\language_package none
\inputencoding auto
\fontencoding default
\font_roman default
\font_sans default
\font_typewriter default
\font_default_family default
\use_non_tex_fonts false
\font_sc false
\font_osf false
\font_sf_scale 100
\font_tt_scale 100

\graphics default
\default_output_format default
\output_sync 0
\bibtex_command default
\index_command default
\paperfontsize 11
\spacing single
\use_hyperref false
\papersize letterpaper
\use_geometry false
\use_amsmath 1
\use_esint 1
\use_mhchem 0
\use_mathdots 0
\cite_engine basic
\use_bibtopic false
\use_indices false
\paperorientation portrait
\suppress_date false
\use_refstyle 0
\index Index
\shortcut idx
\color #008000
\end_index
\secnumdepth 3
\tocdepth 3
\paragraph_separation indent
\paragraph_indentation default
\quotes_language english
\papercolumns 1
\papersides 1
\paperpagestyle plain
\tracking_changes false
\output_changes false
\html_math_output 0
\html_css_as_file 0
\html_be_strict false
\end_header

\begin_body

\begin_layout Title
Shape representation and statistical analysis of the human brain using Spectral
 Geometry
\end_layout

\begin_layout Section*
Project Summary
\end_layout

\begin_layout Standard
The proposed research will develop a framework for invariant representations
 for curves and surfaces that characterize the shapes of a sulcal curves
 and cortical surfaces using methods of spectral geometry.
 The human cerebral cortex is a highly convoluted sheet with rich and detailed
 folding patterns represented by sulci.
 A quantitative analysis of shape of the cortical sheet and the sulcal folding
 patterning requires an invariant framework that invariantly represents
 the geometry of the sulcal and ROI shapes.
 Analysis of the cortical shapes has extensively applications to study disease
 progression 
\begin_inset CommandInset citation
LatexCommand cite
key "Narr01"

\end_inset

, aging 
\begin_inset CommandInset citation
LatexCommand cite
key "prince_aging"

\end_inset

 and brain asymmetry 
\begin_inset CommandInset citation
LatexCommand cite
key "Blanton01"

\end_inset

.
 However, the present approaches do not reflect the shapes of the cortical
 sheet but focus instead on features such as length, depth and 3D location,
 deformation tensor to align it with a reference atlas.
 In order to address these issues, we propose a model for shape analysis
 of 1D curves based on an extension of the Global Point Signature (GPS)
 
\begin_inset CommandInset citation
LatexCommand cite
key "Reuter2010,Rustamov2007"

\end_inset

 that allows the direct analysis of sulcal patterns on the cortical surface.
 The GPS representation for 2D surfaces uses the eigensystem of the Laplace-Belt
rami operator.
 
\end_layout

\begin_layout Standard
The problem of shape quantification of a manifold by studying eigenspectrum
 of a differential operators such as Laplace-Beltrami operator defined on
 the manifold, often stated as 'can one hear the shape of a drum' has led
 to development of spectral geometry as a subbranch of differential geometry.
 In computer vision, corresponding Global Point Signature (GPS) representation
 of 2D surfaces and corresponding methods have been developed to quantify
 geometric 'shape' of an object by studying the eigensystem of the Laplace-Beltr
ami operator.
 The method exploits the isometry invariance of eigenfunctions and introduces
 an invariant metric that quantifies geometric shape differences.
 This representation has desirable properties, including stability, uniqueness
 and invariance to scaling and isometric transformation.
 We have proposed the use of anisotropic helmholtz equation for studying
 curves as well as surfaces.
 In case of sulcal curves, we have used curvature term as an anisotry.
 
\end_layout

\begin_layout Standard
The 
\bar under
intellectual merit
\bar default
 of the proposal comes from the unique combination of theoretical, empirical,
 and software elements that will take brain image analysis into new directions.
\end_layout

\begin_layout Itemize
The 
\emph on
theoretical
\emph default
 element relies on the development of spectral geometric methods for representat
iona and analysis of curves and surfaces.
 We will investigate various spectral operators and their eigenspectrum
 to represent curves and surfaces and will develop fast and efficient numerical
 implementations of the resulting partial differential equations.
 We will also investigate an optimal way of combining these models and their
 utility for specific neuroanatomical studies.
\end_layout

\begin_layout Itemize
The 
\emph on
empirical
\emph default
 aspect of our approach relies on application of the representations for
 shape quantification for applications such as symmetry, twin studies for
 quantifying genetic and environmental infludences, and alterations of sulcal
 folding patterning in neuropathological conditions such as autism.
 We will perform the shape analysis studies to evaluate the performance
 as well as the efficacy and robustness of our methods.
\end_layout

\begin_layout Itemize
The 
\emph on
software
\emph default
 element relies on the development of open source implementations of the
 methods under this grant and their integration into the BrainSuite software
 package developed and distributed from USC by Dr.
 Leahy’s lab.
\end_layout

\begin_layout Standard
The 
\bar under
broader impacts
\bar default
 of developing spectral geometry based methods for shape analysis of brain
 will facilitate a quantitative analysis of neurological analysis of conditions
 marked my alterations in sulcal patterning (e.g.
 autism).
 The proposed methods will work in conjunction with the existing methods
 (tensor-based / voxel-based morphometry) and will allow a powerful set
 of tools for quantitative shape analysis of the cerebral cortex for a variety
 of neuropathological conditions such as disease, development, maturation,
 genetic analysis and group differences in sulcal patterning.
\end_layout

\begin_layout Section
Automatic delineation of Sulci and ROI 
\begin_inset CommandInset label
LatexCommand label
name "sec:Sulcal-Curves-Generation"

\end_inset


\end_layout

\begin_layout Standard
We briefly review our method for automatic generation of sulci on a cortical
 surface, which are described in more detail in 
\begin_inset CommandInset citation
LatexCommand cite
key "ajoshiISBI12"

\end_inset

 .
 We assume as input a triangulated mesh that represents the cortical surface.
 We use the BrainSuite software 
\begin_inset CommandInset citation
LatexCommand cite
key "bs2"

\end_inset

 to extract the cortical surface meshes from T1-weighted MRI volumes for
 the atlas and for each subject.
 We then identify sulcal landmarks on the cortex automatically.
 We compute a one-to-one correspondence between the atlas surface and the
 subject surface in two stages: (i) for each subject, the surface of each
 cortical hemisphere is parameterized to a unit square, and (ii) a vector
 field is found with respect to this parameterization that aligns curvature
 of the surfaces.
 In order to generate such a parameterization, we model the cortical surface
 as an elastic sheet and solve the associated linear elastic equilibrium
 equation using finite elements.
 We constrain the corpus callosum to lie on the boundary of the unit square
 mapped as a uniform speed curve.
 The elastic energy minimization yields flat maps of the two cortical hemisphere
 surfaces to a plane (Fig.
 
\begin_inset CommandInset ref
LatexCommand ref
reference "Fig: surf reg"

\end_inset

).
 Multiresolution representations of curvature for the subject and atlas
 are calculated and then aligned by minimizing a cost function with elastic
 energy as a regularizing penalty.
 This step performs reparameterization of the cortical hemisphere surfaces
 and establishes a one to one point correspondence between subject and atlas
 surfaces.
 A set of 26 sulcal curves per hemisphere on the atlas to which we registered
 were traced interactively in BrainSuite 
\begin_inset CommandInset citation
LatexCommand cite
key "bs2"

\end_inset

 using the protocol described in 
\begin_inset CommandInset citation
LatexCommand cite
key "dimitrios_nimg_sulci"

\end_inset

.
 By using the point correspondence established with the registration, these
 sulci are transferred to the subject surface.
 The locations of the transferred sulci are subsequently refined to better
 follow the true sulcal fundi using geodesic curvature flow on the cortical
 surface as described in 
\begin_inset CommandInset citation
LatexCommand cite
key "ajoshiISBI12"

\end_inset

.
 This is done using a level set based formulation of flow on non-flat surfaces
 with the sulci as the zero level sets.
 
\end_layout

\begin_layout Section
Shape Representation of Sulcal Curves and Cortical ROIs
\begin_inset CommandInset label
LatexCommand label
name "sec:GPS-Coordinates-Representation"

\end_inset


\end_layout

\begin_layout Subsection
Representation of Sulcal Curves
\end_layout

\begin_layout Standard
Spectral geometry provides the basis to study the eigenspectrum of the sulcal
 curves.
 In this paper, we consider the 1D case and propose a point-wise signature
 for the 1D curve.
 Motivated by spectral theory and corresponding work on 2D surfaces 
\begin_inset CommandInset citation
LatexCommand cite
key "Rustamov2007,Reuter2010"

\end_inset

, we model the 1D curves as inhomogeneous vibrating strings.
 Their harmonic behavior is governed by the 1D Helmholtz equation.
 Since our aim is to characterize the shape of the curves 
\begin_inset Formula $C$
\end_inset

, we use curvature 
\begin_inset Formula $\kappa(s)$
\end_inset

 to introduce an anisotropy into the equation:
\end_layout

\begin_layout Standard
\begin_inset Formula 
\begin{equation}
\begin{cases}
\ensuremath{\frac{\partial}{\partial s}\kappa(s)\frac{\partial}{\partial s}\Phi_{i}(s)} & =\lambda_{i}\Phi_{i}(s)\\
\Phi_{i}(s)|_{\partial C} & =0
\end{cases},\forall s\in C\label{eq:main_equation}
\end{equation}

\end_inset

 where 
\begin_inset Formula $\partial C$
\end_inset

 is the set of the endpoints of the curve 
\begin_inset Formula $C$
\end_inset

.
 Denote the of eigenfunctions of this equation by 
\begin_inset Formula $\Phi_{i}$
\end_inset

 and the eigenvalues 
\begin_inset Formula $\lambda_{i}$
\end_inset

 ordered by magnitude.
 We define the embedding manifold in the spectral domain by the map:
\end_layout

\begin_layout Standard
\begin_inset Formula $\ensuremath{GPS(p)=\left(\frac{1}{\sqrt{\lambda_{1}}}\Phi_{1}(p),\frac{1}{\sqrt{\lambda_{2}}}\Phi_{2}(p),\frac{1}{\sqrt{\lambda_{3}}}\Phi_{3}(p),\text{\ensuremath{\ldots}}\right)}.$
\end_inset


\end_layout

\begin_layout Standard
Thus, each point of the curve is embedded into an infinite dimensional space.
 It should be noted that we cannot use the 1D Laplacian directly for this
 purpose because 1D shapes do not have a non-trivial intrinsic geometry.
 However, due to the fundamental theorem of curves (two unit-speed plane
 curves which have the same curvature and torsion differ only by a rigid
 transformation), curvature and torsion define the curve uniquely up to
 rigid transformation.
 Furthermore, the curve can be recovered from the embedding by first recovering
 the curvature and torsion, and then using the Frenet-Serre formulas 
\begin_inset CommandInset citation
LatexCommand cite
key "docarmo"

\end_inset

.
 For 3D curves, this requires curvature and torsion.
 However, the embedding defined above is based on curvature alone, as the
 sulcal curves analyzed in this paper had negligible torsion.
 Some of the important and useful properties of this embedding are defined
 below.
 
\end_layout

\begin_layout Enumerate
The GPS coordinates are isometry invariant as they depend only on the derivative
s and curvature, which in turn are known to be dependent only on the shape.
 
\end_layout

\begin_layout Enumerate
Scaling a 1D curve manifold by the factor 
\begin_inset Formula $\alpha$
\end_inset

 results in curvature scaled by the factor 
\begin_inset Formula $\alpha$
\end_inset

.
 Therefore, by normalizing the eigenvalues, shape can be compared regardless
 of the object's scale (and position as mentioned earlier).
 
\end_layout

\begin_layout Enumerate
Changes of the curve's shape result in continuous changes in its spectrum.
 Consequently the representation presented here is robust.
 
\end_layout

\begin_layout Enumerate
It can be proven that, in the embedding space, the inner product is given
 by the Green's function due to the identity: 
\begin_inset Formula $G(x_{1},x_{2})=\sum_{i}\frac{\Phi_{i}(x_{1})\Phi_{i}(x_{2})}{\lambda_{i}}$
\end_inset

.
 As a result, the GPS representation encodes both local and global curvature
 and shape information into the embedding.
 Additionally, in this infinite dimensional shape space, the metric is euclidean
, which simplifies analysis.
 
\end_layout

\begin_layout Standard
\begin_inset Float figure
wide false
sideways false
status open

\begin_layout Plain Layout
\align center
\begin_inset Graphics
	filename sulcal_analysis.png
	lyxscale 10
	width 95text%

\end_inset


\begin_inset Caption

\begin_layout Plain Layout
(a) Automatic atlas to subject registration and parameterization of cortical
 surfaces and sulcal curves; and (b) geodesic curvature flow refinement
 of sulcal curves (c) Inferior frontal sulcus highlighted in red; (d) first
 four color coded GPS coordinates; (e) GPS representation plotted from end
 to end of a sulcus; (f) automatically generated ROIs on cortical surface;
 (g) first five color coded GPS coordinates.
\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Subsection
Representation of cortical ROIs
\end_layout

\begin_layout Standard
As an extension of the representation presented in the previous section,
 for vibrating membranes, eigenvalues are the resonant frequencies.
 Their harmonic behavior is governed by the 2D Helmholtz equation.
 Similar to previous case, to characterize the shape of the cortical ROI
 
\begin_inset Formula $S$
\end_inset

, we use the mean curvature 
\begin_inset Formula $\kappa_{m}(s)$
\end_inset

 to introduce an anisotropy into the equation:
\end_layout

\begin_layout Standard
\begin_inset Formula 
\begin{equation}
\begin{cases}
\ensuremath{\nabla\cdot\kappa_{m}(s)\nabla\Phi_{i}(s)} & =\lambda_{i}\Phi_{i}(s)\\
\Phi_{i}(s)|_{\partial S} & =0
\end{cases},\forall s\in S\label{eq:main_equation-1}
\end{equation}

\end_inset

 where 
\begin_inset Formula $\partial S$
\end_inset

 is the set of the endpoints of the curve 
\begin_inset Formula $C$
\end_inset

.
 Denote the of eigenfunctions of this equation by 
\begin_inset Formula $\Phi_{i}$
\end_inset

 and the eigenvalues 
\begin_inset Formula $\lambda_{i}$
\end_inset

 ordered by magnitude.
 We define the embedding manifold in the spectral domain by the map:
\end_layout

\begin_layout Standard
\begin_inset Formula $\ensuremath{GPS(p)=\left(\frac{1}{\sqrt{\lambda_{1}}}\Phi_{1}(p),\frac{1}{\sqrt{\lambda_{2}}}\Phi_{2}(p),\frac{1}{\sqrt{\lambda_{3}}}\Phi_{3}(p),\text{\ensuremath{\ldots}}\right)}.$
\end_inset


\end_layout

\begin_layout Section
Results 
\begin_inset CommandInset label
LatexCommand label
name "sec:Results"

\end_inset


\end_layout

\begin_layout Standard
We performed symmetry detection on data from 24 subjects, divided into two
 cohorts of 12 subjects.
 These data were used previously in 
\begin_inset CommandInset citation
LatexCommand cite
key "dimitrios_nimg_sulci"

\end_inset

.
 The first cohort was scanned at ANONYMIZED using a 3T Siemens MAGNETOM
 Trio scanner.
 High-resolution T1-weighted anatomical volumes were acquired for each subject
 with an MPRAGE scan using the following protocol: TR = 2350 ms, TE = 4.13
 ms, 192 slices, field-of-view = 256 mm, voxel size = 1.0 × 1.0 × 1.0 mm.
 The second cohort was scanned at ANONYMIZED using thin-cut MR coronal images
 obtained in a General Electric Signa Scanner operating at 1.5 Tesla, using
 the following protocol: SPGR/50, TR 24, TE 7, NEX 1 Matrix 256 × 192, FOV
 24 cm, which yielded 124 contiguous coronal slices, 1.5 or 1.6 mm thick,
 with an interpixel distance of 0.94 mm.
 Three data sets were obtained for each subject.
 These were co-registered and averaged post-hoc using Automated Image Registrati
on (AIR 3.03, Woods et al.
 (1998)).
 The averaged image data for each subject had anisotropic voxels with an
 interpixel spacing of 0.7 mm and interslice spacing of 1.5–1.6 mm.
\end_layout

\begin_layout Standard
We applied the BrainSuite surface extraction sequence followed by sulcal
 set generation as outlined in Sec.
 
\begin_inset CommandInset ref
LatexCommand ref
reference "sec:Sulcal-Curves-Generation"

\end_inset

.
 This produced 24x2 cortical surface hemisphere representations with 26
 sulci each.
 The sulci were denoised by fitting a 12th order polynomial to the curves.
 The degree of the polynomial was selected using L-curve analysis of curve
 fitting for each curve and selecting the maximum degree necessary for all
 26 curves.
 Next, the GPS coordinate representation was generated for all curves as
 described in Sec.
 
\begin_inset CommandInset ref
LatexCommand ref
reference "sec:GPS-Coordinates-Representation"

\end_inset

 and Sec.
 
\begin_inset CommandInset ref
LatexCommand ref
reference "sec:Discretization-using-Finite"

\end_inset

.
 The symmetry between the sulci was then estimated using the method in Sec
 
\begin_inset CommandInset ref
LatexCommand ref
reference "sec:Shape-Matching"

\end_inset

.
 The results of the symmetry mapping are shown in Fig.
 
\begin_inset CommandInset ref
LatexCommand ref
reference "Fig 4: Symmetry"

\end_inset

.
 It is interesting to note that the post- and pre-central sulci, together
 with the posterior segment of the superior temporal, the transverse temporal,
 the middle temporal and the inferior occipital sulci in the dorso-lateral
 view, show the maximal amount of left/right asymmetry; on the mesial view
 the collateral, the supraorbital, the occipito-parietal and long stretches
 of the cingulate sulci are also extremely asymmetric.
 It is not surprising to see the cingulate sulcus (visible in the depth
 of the mesial view) to show a great extent of relative symmetry.
 
\begin_inset Float figure
wide false
sideways false
status open

\begin_layout Plain Layout
\begin_inset Graphics
	filename fig1.png
	lyxscale 10
	width 45line%

\end_inset


\begin_inset Graphics
	filename C:/Users/ajoshi/Dropbox/shape_representation/ShapeAsymm1.png
	lyxscale 15
	width 45text%

\end_inset


\begin_inset Caption

\begin_layout Plain Layout
Shape symmetry measure of the sulci plotted on a smooth representation of
 an individual cortical surface.
 The black regions on the curves indicate that a significant symmetry was
 not found for those points.
 
\end_layout

\end_inset


\end_layout

\begin_layout Plain Layout
\begin_inset CommandInset label
LatexCommand label
name "Fig 4: Symmetry"

\end_inset

 
\end_layout

\end_inset


\end_layout

\begin_layout Standard
This model has a variety of potential applications in computer vision as
 well as brain image analysis.
 Many of the existing methods for brain morphometry focus on point-wise
 features such as 3D location, curvature, thickness, deformation, and image
 intensity.
 Conversely, the framework we have presented captures directly the geometric
 shape of the folding pattern.
 By using this method, we can study the cortical folding pattern quantitatively,
 and therefore use it for quantitative analysis of brain shapes in a variety
 of neuro-developmental conditions (e.g.
 autism) and in other neurological conditions characterized by changes in
 sulcal patterns.
\end_layout

\begin_layout Standard

\size scriptsize
\begin_inset CommandInset bibtex
LatexCommand bibtex
bibfiles "ajoshibib"
options "splncs03"

\end_inset


\end_layout

\begin_layout Standard
\begin_inset ERT
status collapsed

\begin_layout Plain Layout


\backslash
setcounter{page}{1}
\end_layout

\end_inset


\end_layout

\begin_layout Standard
\begin_inset ERT
status collapsed

\begin_layout Plain Layout

%%%%%%%%% BIOGRAPHICAL SKETCH -- 2 pages
\end_layout

\begin_layout Plain Layout

\end_layout

\end_inset


\end_layout

\begin_layout Standard
\begin_inset ERT
status collapsed

\begin_layout Plain Layout


\backslash
required{
\end_layout

\end_inset

Biographical Sketch: Your Name
\begin_inset ERT
status collapsed

\begin_layout Plain Layout

}
\end_layout

\end_inset


\end_layout

\begin_layout Standard
\begin_inset ERT
status collapsed

\begin_layout Plain Layout

% Your Bio should be divided into the following sections
\end_layout

\begin_layout Plain Layout

\end_layout

\end_inset


\begin_inset ERT
status collapsed

\begin_layout Plain Layout

% (a) Professional Preparation (education):
\end_layout

\begin_layout Plain Layout

\end_layout

\end_inset


\begin_inset ERT
status collapsed

\begin_layout Plain Layout

% Undergrad, Major, Year
\end_layout

\begin_layout Plain Layout

\end_layout

\end_inset


\begin_inset ERT
status collapsed

\begin_layout Plain Layout

% Graduate, Major, Year
\end_layout

\begin_layout Plain Layout

\end_layout

\end_inset


\begin_inset ERT
status collapsed

\begin_layout Plain Layout

% Postdoc, Area, Years-Inclusive
\end_layout

\begin_layout Plain Layout

\end_layout

\end_inset


\begin_inset ERT
status collapsed

\begin_layout Plain Layout

% (b) Appointments:  most recent first.
\end_layout

\begin_layout Plain Layout

\end_layout

\end_inset


\begin_inset ERT
status collapsed

\begin_layout Plain Layout

% (c) Publications:  5 related to the proposal, and 5 "Other Significant
 Publications"
\end_layout

\begin_layout Plain Layout

\end_layout

\end_inset


\begin_inset ERT
status collapsed

\begin_layout Plain Layout

% (d) Synergistic Activities (math-enhancing activities that were not
\end_layout

\begin_layout Plain Layout

\end_layout

\end_inset


\begin_inset ERT
status collapsed

\begin_layout Plain Layout

% part of your main job description, like editorial boards and
\end_layout

\begin_layout Plain Layout

\end_layout

\end_inset


\begin_inset ERT
status collapsed

\begin_layout Plain Layout

% conference organizing - any Math-related volunteer work.
\end_layout

\begin_layout Plain Layout

\end_layout

\end_inset


\begin_inset ERT
status collapsed

\begin_layout Plain Layout

% these are often similar to Broader Impacts
\end_layout

\begin_layout Plain Layout

\end_layout

\end_inset


\end_layout

\begin_layout Standard
\begin_inset ERT
status collapsed

\begin_layout Plain Layout

% (e) Collaborators & Other Affiliations: (use the following sections)
\end_layout

\begin_layout Plain Layout

\end_layout

\end_inset


\begin_inset ERT
status collapsed

\begin_layout Plain Layout

% list in alphabetical order, and include current affiliations parenthetically
\end_layout

\begin_layout Plain Layout

\end_layout

\end_inset


\begin_inset ERT
status collapsed

\begin_layout Plain Layout

% Collaborators and Co-editors: past 48 months.
  If none, write "none"
\end_layout

\begin_layout Plain Layout

\end_layout

\end_inset


\begin_inset ERT
status collapsed

\begin_layout Plain Layout

% Graduate Advisors and Postdoctoral sponsors: (your own, no matter how
 long ago)
\end_layout

\begin_layout Plain Layout

\end_layout

\end_inset


\begin_inset ERT
status collapsed

\begin_layout Plain Layout

% Thesis advisor and postgraduate scholar-sponsor:  those you have advised
\end_layout

\begin_layout Plain Layout

\end_layout

\end_inset


\begin_inset ERT
status collapsed

\begin_layout Plain Layout

% in the past 5 years.
  
\end_layout

\begin_layout Plain Layout

\end_layout

\end_inset


\begin_inset ERT
status collapsed

\begin_layout Plain Layout

% Total number of graduate students advised: X (all time)
\end_layout

\begin_layout Plain Layout

\end_layout

\end_inset


\begin_inset ERT
status collapsed

\begin_layout Plain Layout

% Total number of postdoctoral scholars sponsored: Y (all time)
\end_layout

\begin_layout Plain Layout

\end_layout

\end_inset


\end_layout

\end_body
\end_document
