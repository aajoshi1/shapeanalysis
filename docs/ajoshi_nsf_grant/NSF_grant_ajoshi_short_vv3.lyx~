#LyX 2.0 created this file. For more info see http://www.lyx.org/
\lyxformat 413
\begin_document
\begin_header
\textclass spie
\begin_preamble

%%%%%%%%% MASTER -- compiles the 4 sections



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                                                      %%
%%%%%%%%%% EXACT 1in MARGINS %%%%%%%                                   %%
\setlength{\textwidth}{6.5in}     %%                                   %%
\setlength{\oddsidemargin}{0in}   %% (It is recommended that you       %%
\setlength{\evensidemargin}{0in}  %%  not change these parameters,     %%
\setlength{\textheight}{8.5in}    %%  at the risk of having your       %%
\setlength{\topmargin}{0in}       %%  proposal dismissed on the basis  %%
\setlength{\headheight}{0in}      %%  of incorrect formatting!!!)      %%
\setlength{\headsep}{0in}         %%                                   %%
\setlength{\footskip}{.5in}       %%                                   %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%                                   %%
\newcommand{\required}[1]{\section*{\hfil #1\hfil}}                    %%
\renewcommand{\refname}{\hfil References Cited\hfil}                   %%
\bibliographystyle{plain}                                              %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%PUT YOUR MACROS HERE

%\includeonly{NSFsumm}
\end_preamble
\use_default_options false
\maintain_unincluded_children false
\language english
\language_package none
\inputencoding auto
\fontencoding default
\font_roman default
\font_sans default
\font_typewriter default
\font_default_family default
\use_non_tex_fonts false
\font_sc false
\font_osf false
\font_sf_scale 100
\font_tt_scale 100

\graphics default
\default_output_format default
\output_sync 0
\bibtex_command default
\index_command default
\paperfontsize 11
\spacing single
\use_hyperref false
\papersize letterpaper
\use_geometry false
\use_amsmath 1
\use_esint 1
\use_mhchem 0
\use_mathdots 0
\cite_engine basic
\use_bibtopic false
\use_indices false
\paperorientation portrait
\suppress_date false
\use_refstyle 0
\index Index
\shortcut idx
\color #008000
\end_index
\secnumdepth 3
\tocdepth 3
\paragraph_separation indent
\paragraph_indentation default
\quotes_language english
\papercolumns 1
\papersides 1
\paperpagestyle plain
\tracking_changes false
\output_changes false
\html_math_output 0
\html_css_as_file 0
\html_be_strict false
\end_header

\begin_body

\begin_layout Title
Brain Shape Analysis using Spectral Geometry
\end_layout

\begin_layout Standard
Investigators: Anand A Joshi, David W Shattuck, Hanna Damasio and Richard
 M Leahy
\end_layout

\begin_layout Standard
Program: NSF Cognitive Neuroscience: http://www.nsf.gov/funding/pgm_summ.jsp?pims_i
d=5316, (application deadline Aug 27th) 
\end_layout

\begin_layout Subsection*
Project Summary
\end_layout

\begin_layout Standard
The proposed research will develop a framework for invariant representations
 for curves and surfaces that characterize the shapes of a sulcal curves
 and cortical surfaces using methods of spectral geometry.
 The human cerebral cortex is a highly convoluted sheet with rich and detailed
 folding patterns represented by sulci.
 A quantitative analysis of shape of the cortical sheet and the sulcal folding
 patterning requires an invariant framework that invariantly represents
 the geometry of the sulcal and ROI shapes.
 Analysis of the cortical shapes has extensively applications to study disease
 progression 
\begin_inset CommandInset citation
LatexCommand cite
key "Narr01"

\end_inset

, aging 
\begin_inset CommandInset citation
LatexCommand cite
key "prince_aging"

\end_inset

 and brain asymmetry 
\begin_inset CommandInset citation
LatexCommand cite
key "Blanton01"

\end_inset

.
 However, the present approaches do not reflect the shapes of the cortical
 sheet but focus instead on features such as length, depth and 3D location,
 deformation tensor to align it with a reference atlas.
 In order to address these issues, we propose a model for shape analysis
 of 1D curves and 2D cortical parcellations (ROI surfaces) based on spectral
 geometry.
 The problem of shape quantification of a manifold by studying eigenspectrum
 of a differential operators such as Laplace-Beltrami operator defined on
 the manifold, often stated as `can one hear the shape of a drum' has led
 to development of spectral geometry as a subbranch of differential geometry.
 In computer vision, corresponding Global Point Signature (GPS) representation
 of 2D surfaces 
\begin_inset CommandInset citation
LatexCommand cite
key "Reuter2010,Rustamov2007"

\end_inset

 and corresponding methods have been developed to quantify geometric 'shape'
 of an object by studying the eigensystem of the Laplace-Beltrami operator.
 The method exploits the isometry invariance of eigenfunctions and introduces
 an invariant metric that quantifies geometric shape differences.
 This representation has desirable properties, including stability, uniqueness
 and invariance to scaling and isometric transformation.
 We have proposed the use of anisotropic helmholtz equation for studying
 curves as well as surfaces.
 In case of sulcal curves, we have used curvature term as an anisotry.
 
\end_layout

\begin_layout Standard
The 
\bar under
intellectual merit
\bar default
 of the proposal comes from the unique combination of theoretical, empirical,
 and software elements that will substantially impact brain image analysis.
\end_layout

\begin_layout Itemize
The 
\emph on
theoretical
\emph default
 element relies on the development of spectral geometric methods for representat
iona and analysis of curves and surfaces.
 We will investigate various spectral operators and their eigenspectrum
 to represent curves and surfaces and will develop fast and efficient numerical
 implementations of the resulting partial differential equations.
 We will also investigate an optimal way of combining these models and their
 utility for specific neuroanatomical studies.
\end_layout

\begin_layout Itemize
The 
\emph on
empirical
\emph default
 aspect of our approach relies on application of the representations for
 shape quantification for applications such as symmetry, twin studies for
 quantifying genetic and environmental infludences, and alterations of sulcal
 folding patterning in neuropathological conditions such as autism.
 We will perform the shape analysis studies to evaluate the performance
 as well as the efficacy and robustness of our methods.
\end_layout

\begin_layout Itemize
The 
\emph on
software
\emph default
 element relies on the development of open source implementations of the
 methods under this grant and their integration into the BrainSuite software
 package developed and distributed from USC by Dr.
 Leahy’s lab.
\end_layout

\begin_layout Standard
The 
\bar under
broader impacts
\bar default
 of developing spectral geometry based methods for shape analysis of brain
 will facilitate a quantitative analysis of neurological analysis of conditions
 marked my alterations in sulcal patterning (e.g.
 autism).
 The proposed methods will work in conjunction with the existing methods
 (tensor-based / voxel-based morphometry) and will allow a powerful set
 of tools for quantitative shape analysis of the cerebral cortex for a variety
 of neuropathological conditions such as disease, development, maturation,
 genetic analysis and group differences in sulcal patterning.
\end_layout

\begin_layout Subsection*
Shape Representation of Sulcal Curves and Cortical ROIs
\begin_inset CommandInset label
LatexCommand label
name "sec:GPS-Coordinates-Representation"

\end_inset


\end_layout

\begin_layout Standard
We have developed a method for automatic generation of sulci and ROIs on
 a cortical surface 
\begin_inset CommandInset citation
LatexCommand cite
key "ajoshiISBI12"

\end_inset

 using automatic cortical surface registration to a reference atlas.
 
\end_layout

\begin_layout Subsection*
Representation of sulcal curves and cortical ROI surfaces
\end_layout

\begin_layout Standard
Spectral geometry provides the basis to study the eigenspectrum of the sulcal
 curves and ROI surfaces.
 We consider the 1D and 2D case and propose a point-wise signature for the
 curves and surfaces.
 Motivated by spectral theory and corresponding work on 2D surfaces 
\begin_inset CommandInset citation
LatexCommand cite
key "Rustamov2007,Reuter2010"

\end_inset

, we model the cortical curves and surfaces as inhomogeneous vibrating strings
 and membranes.
 Their harmonic behavior is governed by the 1D and 2D Helmholtz equation.
 Since our aim is to characterize the shape of a curve 
\begin_inset Formula $C$
\end_inset

 and ROI surface 
\begin_inset Formula $S$
\end_inset

, we use curvature 
\begin_inset Formula $\kappa(s)$
\end_inset

 to introduce an anisotropy into the equation.
 Similarly, for the surface, we use the mean curvature 
\begin_inset Formula $\kappa_{m}(s)$
\end_inset

 as an anisotropy term:
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\begin{array}{cc}
\text{curve} & \text{surface}\\
\begin{cases}
\ensuremath{\frac{\partial}{\partial s}\kappa(s)\frac{\partial}{\partial s}\Phi_{i}(s)} & \ensuremath{\frac{\partial}{\partial s}\kappa(s)\frac{\partial}{\partial s}\Phi_{i}(s)}=\lambda_{i}\Phi_{i}(s)\\
\Phi_{i}(s)|_{\partial C} & =0
\end{cases},\forall s\in C & \begin{cases}
\ensuremath{\nabla\cdot\kappa_{m}(s)\nabla\Phi_{i}(s)} & =\lambda_{i}\Phi_{i}(s)\\
\Phi_{i}(s)|_{\partial S} & =0
\end{cases},\forall s\in S
\end{array}
\]

\end_inset


\end_layout

\begin_layout Standard
where 
\begin_inset Formula $\partial C$
\end_inset

 is the set of the endpoints of the curve 
\begin_inset Formula $C$
\end_inset

 and 
\begin_inset Formula $\partial S$
\end_inset

 is the boundary of the cortical ROI.
 Denote the of eigenfunctions of this equation by 
\begin_inset Formula $\Phi_{i}$
\end_inset

 and the eigenvalues 
\begin_inset Formula $\lambda_{i}$
\end_inset

 ordered by magnitude.
 We define the embedding manifold in the spectral domain by the map:
\end_layout

\begin_layout Standard
\begin_inset Formula $\ensuremath{GPS(p)=\left(\frac{1}{\sqrt{\lambda_{1}}}\Phi_{1}(p),\frac{1}{\sqrt{\lambda_{2}}}\Phi_{2}(p),\frac{1}{\sqrt{\lambda_{3}}}\Phi_{3}(p),\text{\ensuremath{\ldots}}\right)}.$
\end_inset


\end_layout

\begin_layout Standard
Thus, each point of the curve is embedded into an infinite dimensional space.
 
\begin_inset Float figure
wide false
sideways false
status collapsed

\begin_layout Plain Layout
\begin_inset Graphics
	filename shape_analysis.png
	lyxscale 10
	width 55text%

\end_inset


\end_layout

\begin_layout Plain Layout
\begin_inset Caption

\begin_layout Plain Layout
(a) Inferior frontal sulcus highlighted in red; (b) first four color coded
 GPS coordinates; (c) GPS representation plotted from end to end of a sulcus;
 (d) automatically generated ROIs on cortical surface; (e) first five color
 coded GPS coordinates.
\end_layout

\end_inset


\end_layout

\end_inset

 It should be noted that we cannot use the 1D Laplacian directly for this
 purpose because 1D shapes do not have a non-trivial intrinsic geometry.
 However, due to the fundamental theorem of curves (two unit-speed plane
 curves which have the same curvature and torsion differ only by a rigid
 transformation), curvature and torsion define the curve uniquely up to
 rigid transformation.
 Furthermore, the curve can be recovered from the embedding by first recovering
 the curvature and torsion, and then using the Frenet-Serre formulas 
\begin_inset CommandInset citation
LatexCommand cite
key "docarmo"

\end_inset

.
 For 3D curves, this requires curvature and torsion.
 However, the embedding defined above is based on curvature alone, as the
 sulcal curves analyzed in this paper had negligible torsion.
 Some of the important and useful properties of this embedding are defined
 below.
 
\end_layout

\begin_layout Enumerate
The GPS coordinates are isometry invariant as they depend only on the derivative
s and curvature, which in turn are known to be dependent only on the shape.
 
\end_layout

\begin_layout Enumerate
Scaling a 1D curve manifold by the factor 
\begin_inset Formula $\alpha$
\end_inset

 results in curvature scaled by the factor 
\begin_inset Formula $\alpha$
\end_inset

.
 Therefore, by normalizing the eigenvalues, shape can be compared regardless
 of the object's scale (and position as mentioned earlier).
 
\end_layout

\begin_layout Enumerate
Changes of the curve's shape result in continuous changes in its spectrum.
 Consequently the representation presented here is robust.
 
\end_layout

\begin_layout Enumerate
It can be proven that, in the embedding space, the inner product is given
 by the Green's function due to the identity: 
\begin_inset Formula $G(x_{1},x_{2})=\sum_{i}\frac{\Phi_{i}(x_{1})\Phi_{i}(x_{2})}{\lambda_{i}}$
\end_inset

.
 As a result, the GPS representation encodes both local and global curvature
 and shape information into the embedding.
 Additionally, in this infinite dimensional shape space, the metric is euclidean
, which simplifies analysis.
 
\end_layout

\begin_layout Subsection*
Preliminary Results 
\begin_inset CommandInset label
LatexCommand label
name "sec:Results"

\end_inset


\end_layout

\begin_layout Standard
In order to illustrate the potential of this method for morphometric analysis,
 we applied it for symmetry detection.
\begin_inset Wrap figure
lines 0
placement o
overhang 0col%
width "50col%"
status collapsed

\begin_layout Plain Layout
\begin_inset Graphics
	filename Asymm_detection.png
	lyxscale 15
	width 45text%

\end_inset


\end_layout

\begin_layout Plain Layout
\begin_inset Caption

\begin_layout Plain Layout
Shape symmetry measure of the sulci plotted on a smooth representation of
 an individual cortical surface.
 The black regions on the curves indicate that a significant symmetry was
 not found for those points.
 
\end_layout

\end_inset


\begin_inset CommandInset label
LatexCommand label
name "Fig 4: Symmetry"

\end_inset

 
\end_layout

\end_inset

 The preliminary study consisted of 24 normal brains.
 We applied the BrainSuite surface extraction sequence followed by sulcal
 set and ROI parcellation generation 
\begin_inset CommandInset citation
LatexCommand cite
key "ajoshiISBI12"

\end_inset

.
 This produced 24x2 cortical surface hemisphere representations with 26
 sulci each.
 Next, the GPS coordinate representation was generated for all curves as
 well as ROI surfaces.
 The symmetry between the sulci was then estimated by comparing intra-hemisphere
 left.
 The results of the symmetry mapping are shown in Fig.
 
\begin_inset CommandInset ref
LatexCommand ref
reference "Fig 4: Symmetry"

\end_inset

, both for sulcal curves as well as cortical surfaces.
 It is interesting to note that the post- and pre-central sulci, together
 with the posterior segment of the superior temporal, the transverse temporal,
 the middle temporal and the inferior occipital sulci in the dorso-lateral
 view, show the maximal amount of left/right asymmetry; on the mesial view
 the collateral, the supraorbital, the occipito-parietal and long stretches
 of the cingulate sulci are also extremely asymmetric.
 It is not surprising to see the cingulate sulcus (visible in the depth
 of the mesial view) to show a great extent of relative symmetry.
 
\end_layout

\begin_layout Subsection*
Proposed Extensions
\end_layout

\begin_layout Standard
With the GPS representation of curves we can analyze shapes in their native
 form.
 Thus, we can perform comparisons of shapes without worrying about the effects
 of Euclidean transformations or the Euclidean embedding on such comparisons.
 We propose two avenues that we will pursue to increase the ability of scientist
s to evaluate the inherent structure of shapes via the GPS representation
 of curves.
\end_layout

\begin_layout Subsubsection*
Statistics of Shapes
\end_layout

\begin_layout Standard
Given many samples of, say, the shape of the cingulate, the Euclidean representa
tion is unable to satisfactorily aggregate and compare each sample because
 it is sensitive to small changes in shape.
 With the GPS representation of curves, we have a common basis -- resistant
 to small shape changes -- for calculation of the mean shape and the standard
 deviation of shape.
 We propose development of a statistical framework to analyze the shape
 of a cortical region over many samples.
 With this framework, we can then characterize the gross features of an
 structure in the brain, including its general shape and the variability
 in that region over a population.
 In addition, the statistics we will provide can be used for analysis of
 a normal population against a population with a specific trait, be it genetic,
 clinical, etc.
 The statistics of shapes will allow scientists to analyze the behavior
 of a brain structure within the framework of shapes, unhindered by the
 3D embedding with which the shape is drawn.
\end_layout

\begin_layout Subsubsection*
Anatomical Connectivity
\end_layout

\begin_layout Standard
In recent years, connectivity has taken a central role in the processing
 of brain data.
 In functional brain analysis, estimation of connectivity provides a basis
 for inference on whether brain structures are related.
 With the GPS representation of curves, we can analyze the correlation between
 two brain structures by their covariation across a population.
 If two structures covary -- so that the change in shape of one structure
 in a subject is coupled with the change in shape of the second -- then
 we can infer that these two brain areas are related in their form and function.
 Since the GPS representation of curves is a spectral analysis tool, it
 provides a way to compare shapes at multiple resolutions, similar to the
 benefits of coherence in functional connectivity analysis.
 We will use the GPS representation of curves to determine structures that
 covary across subjects, indicating they are anatomically linked.
 In addition, with the multiple resolutions of the GPS representation, we
 can compare shapes as to whether they covary at low spatial frequencies,
 representing gross shape relationships, or high spatial frequencies, representi
ng finer shape changes.
\end_layout

\begin_layout Subsubsection*
Subcortical Shapes and Shape Bundles
\end_layout

\begin_layout Standard
In our work, we focused on the shape of sulci on the cortex.
 We will extend this work to subcortical structures such as the hippocampus.
 The shape of subcortical structures plays an important role in cognitive
 ability as well as human behavior.
 With the GPS representation, we can analyze subcortical structures in the
 clinical setting to determine whether diseases such as autism are linked
 to deformations in brain structures.
 In addition, the emerging field of diffusion tensor imaging provides an
 estimate of white matter tract locations.
 We can extend GPS representations to look at these tracts.
 Since the drawn tracts usually occur in bundles, we plan to modify the
 GPS representation to account for multiple tracts each representing one
 key white matter tract such as the corpus callosum.
\end_layout

\begin_layout Standard

\size scriptsize
\begin_inset CommandInset bibtex
LatexCommand bibtex
bibfiles "ajoshibib"
options "splncs03"

\end_inset


\end_layout

\end_body
\end_document
