#LyX 2.0 created this file. For more info see http://www.lyx.org/
\lyxformat 413
\begin_document
\begin_header
\textclass spie
\begin_preamble

%%%%%%%%% MASTER -- compiles the 4 sections



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                                                      %%
%%%%%%%%%% EXACT 1in MARGINS %%%%%%%                                   %%
\setlength{\textwidth}{6.5in}     %%                                   %%
\setlength{\oddsidemargin}{0in}   %% (It is recommended that you       %%
\setlength{\evensidemargin}{0in}  %%  not change these parameters,     %%
\setlength{\textheight}{8.5in}    %%  at the risk of having your       %%
\setlength{\topmargin}{0in}       %%  proposal dismissed on the basis  %%
\setlength{\headheight}{0in}      %%  of incorrect formatting!!!)      %%
\setlength{\headsep}{0in}         %%                                   %%
\setlength{\footskip}{.5in}       %%                                   %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%                                   %%
\newcommand{\required}[1]{\section*{\hfil #1\hfil}}                    %%
\renewcommand{\refname}{\hfil References Cited\hfil}                   %%
\bibliographystyle{plain}                                              %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%PUT YOUR MACROS HERE

%\includeonly{NSFsumm}
\end_preamble
\use_default_options false
\maintain_unincluded_children false
\language english
\language_package none
\inputencoding auto
\fontencoding default
\font_roman default
\font_sans default
\font_typewriter default
\font_default_family default
\use_non_tex_fonts false
\font_sc false
\font_osf false
\font_sf_scale 100
\font_tt_scale 100

\graphics default
\default_output_format default
\output_sync 0
\bibtex_command default
\index_command default
\paperfontsize 11
\spacing single
\use_hyperref false
\papersize letterpaper
\use_geometry false
\use_amsmath 1
\use_esint 1
\use_mhchem 0
\use_mathdots 0
\cite_engine basic
\use_bibtopic false
\use_indices false
\paperorientation portrait
\suppress_date false
\use_refstyle 0
\index Index
\shortcut idx
\color #008000
\end_index
\secnumdepth 3
\tocdepth 3
\paragraph_separation indent
\paragraph_indentation default
\quotes_language english
\papercolumns 1
\papersides 1
\paperpagestyle plain
\tracking_changes false
\output_changes false
\html_math_output 0
\html_css_as_file 0
\html_be_strict false
\end_header

\begin_body

\begin_layout Title
Brain Shape Analysis using Spectral Geometry
\end_layout

\begin_layout Subsection*
Project Summary
\end_layout

\begin_layout Standard
The proposed research will develop a framework for invariant representations
 of the shapes of cortical anatomy, the sulci and gyri, using methods of
 spectral geometry.
 Analysis of brain shape has applications in the study of brain asymmetry
 and studies of differences in populations, such studies also have potential
 applications in investigations of disease progression and aging.
 A quantitative analysis of the shapes of brain structures requires a representa
tion of the geometry of a structure that is invariant to Euclidean motion.
 Present approaches do not reflect the shapes of these structures but focus
 instead on features such as length and depth.
 Shape analysis of the brain presents a challenging problem since the human
 cerebral cortex is a highly convoluted sheet whose folding patterns are
 important structures because they allow the identification of gyri that
 indirectly provide information about the cytoarchitecture of the cortex.
 For sulci (curves) and gyri (surface patches between sulci), we propose
 an invariant representation of their shapes for quantitative shape analysis.
 Shape quantification of a manifold (curves, surfaces) has led to development
 of spectral geometry as a subbranch of differential geometry.
 In spectral geometry, the shape of a manifold is characterized by the eigenspec
trum of differential operators on the manifold.
 In computer vision applications, the Global Point Signature (GPS) representatio
n of 2D surfaces 
\begin_inset CommandInset citation
LatexCommand cite
key "Reuter2010,Rustamov2007"

\end_inset

 helps to quantify the geometric `shape' of an object by studying the eigensyste
m of the Laplace-Beltrami operator.
 The method exploits the isometry invariance of eigenfunctions, introducing
 an invariant metric to quantify shape differences.
 This representation has desirable properties, including stability, uniqueness,
 invariance to scaling and isometric transformations.
 We propose the use of the anisotropic Helmholtz equation to study the spectral
 geometry of curves and surfaces.
 In the case of sulcal curves, this anisotropy is introduced via the curvature
 of the sulcus whereas for gyral surfaces, we use mean curvature for this
 purpose.
 
\end_layout

\begin_layout Standard
The 
\bar under
intellectual merit
\bar default
 of the proposal comes from the unique combination of theory, experimental
 studies, and software addressing a novel approach to brain image analysis.
 The 
\emph on
theory
\emph default
 will focus on the development of spectral geometric methods for analysis
 of curves and surfaces.
 We will investigate various spectral operators and their eigenspectra to
 represent curves and surfaces.
 We will also investigate an optimal way of utilizing these models for specific
 neuroanatomical studies.
 The 
\emph on
studies
\emph default
 will use the invariant shape quantification for applications to investigate
 hemispherical symmetry and genetic and environmental influence on sulcal
 patterning (using twin populations).
 The designed methods will potentially have a broader impact by allowing
 investigators to study possible alterations of sulcal patterns among population
s and neuropathological conditions.
 The 
\emph on
software
\emph default
 aspect of the project will involve an open source implementations of the
 methods developed under this grant and their integration into the BrainSuite
 software package 
\begin_inset CommandInset citation
LatexCommand cite
key "bs2"

\end_inset

.
\end_layout

\begin_layout Standard
The 
\bar under
broader impact
\bar default
 of developing spectral methods for shape analysis of the brain will be
 reflected by its ability to allow quantitative analyses of neurological
 conditions characterized by alterations in sulcal patterns.
 
\bar under
The proposed methods will allow an exploration of an important and interesting
 aspect of the brain (sulcal and gyral patterns) to provide a powerful set
 of tools for quantitative brain shape analysis.

\bar default
 These methods can potentially be applied to study the influence of disease,
 development, maturation, and genes on sulcal and gyral patterns.
\end_layout

\begin_layout Subsection*
Shape Representation of Sulcal Curves and Gyral Surfaces 
\begin_inset CommandInset label
LatexCommand label
name "sec:GPS-Coordinates-Representation"

\end_inset


\end_layout

\begin_layout Standard
Spectral geometry provides the basis to study the eigenspectrum of sulcal
 curves and gyral surfaces.
\end_layout

\begin_layout Standard
\begin_inset Float figure
wide false
sideways false
status collapsed

\begin_layout Plain Layout
\align center
\begin_inset Graphics
	filename shape_analysis4.png
	lyxscale 5
	width 100text%

\end_inset


\begin_inset Caption

\begin_layout Plain Layout
(a) Inferior frontal sulcus highlighted in red; (b) first four color coded
 GPS coordinates; (c) GPS representation plotted from end to end of a sulcus;
 (d) automatically generated gyral parcellations on cortical surface; (e)
 first five color coded GPS coordinates of the left superior-frontal gyrus.
\end_layout

\end_inset


\end_layout

\end_inset

 We consider the 1D and 2D case and propose a point-wise signature for the
 curves and surfaces.
 Motivated by spectral theory and corresponding work on 2D surfaces 
\begin_inset CommandInset citation
LatexCommand cite
key "Rustamov2007,Reuter2010"

\end_inset

, we model cortical curves (surfaces) as inhomogeneous vibrating strings
 (membranes).
 Their harmonic behavior is governed by the 1D (2D) Helmholtz equation.
 Since our aim is to characterize the shape of a curve 
\begin_inset Formula $C$
\end_inset

 (gyral surface 
\begin_inset Formula $S$
\end_inset

), we use curvature 
\begin_inset Formula $\kappa(s)$
\end_inset

 to introduce anisotropy into the equation.
 Similarly, for the surface 
\begin_inset Formula $S$
\end_inset

, we use the mean curvature 
\begin_inset Formula $\kappa_{m}(s)$
\end_inset

 as an anisotropy term:
\begin_inset Formula 
\[
\begin{array}{cc}
\text{curve} & \text{surface}\\
\begin{cases}
\ensuremath{\frac{\partial}{\partial s}\kappa(s)\frac{\partial}{\partial s}\Phi_{i}(s)}=\lambda_{i}\Phi_{i}(s)\\
\Phi_{i}(s)|_{\partial C} & =0
\end{cases},\forall s\in C & \begin{cases}
\ensuremath{\nabla\cdot\kappa_{m}(s)\nabla\Phi_{i}(s)} & =\lambda_{i}\Phi_{i}(s)\\
\Phi_{i}(s)|_{\partial S} & =0
\end{cases},\forall s\in S
\end{array}
\]

\end_inset

where 
\begin_inset Formula $\partial C$
\end_inset

 is the set of the endpoints of the curve 
\begin_inset Formula $C$
\end_inset

 and 
\begin_inset Formula $\partial S$
\end_inset

 is the boundary of the gyri.
 Denote the eigenfunctions of this equation by 
\begin_inset Formula $\Phi_{i}$
\end_inset

 with eigenvalues 
\begin_inset Formula $\lambda_{i}$
\end_inset

 ordered by magnitude.
 We define the embedding manifold in the spectral domain by the map: 
\begin_inset Formula 
\[
\ensuremath{GPS(p)=\left(\frac{1}{\sqrt{\lambda_{1}}}\Phi_{1}(p),\frac{1}{\sqrt{\lambda_{2}}}\Phi_{2}(p),\frac{1}{\sqrt{\lambda_{3}}}\Phi_{3}(p),\text{\ensuremath{\ldots}}\right)}.
\]

\end_inset

 Thus, each point of the manifold (curve or surface) is embedded into an
 infinite dimensional space.
 This embedding has many favorable properties.
 First, its coordinates are isometry invariant as they depend only on the
 derivatives and curvature, dependent only on the shape.
 Second, scaling a manifold by the factor 
\begin_inset Formula $\alpha$
\end_inset

 results in curvature scaled by the factor 
\begin_inset Formula $\alpha$
\end_inset

.
 Therefore, by normalizing the eigenvalues, we obtain scale-invariance.
 Third, changes of the manifold's shape results in continuous changes in
 its spectrum so the representation is robust.
 Fourth, in the embedding space, the inner product is given by the Green's
 function due to the identity: 
\begin_inset Formula $G(x_{1},x_{2})=\sum_{i}\frac{\Phi_{i}(x_{1})\Phi_{i}(x_{2})}{\lambda_{i}}$
\end_inset

.
 As a result, the GPS representation encodes both local and global shape
 information into the embedding.
 Finally, in this infinite dimensional shape space, the metric is simply
 Euclidean.
 Shape analysis can then be performed in this embedded space as it is invariant
 to the Euclidean representation.
\end_layout

\begin_layout Subsection*
Preliminary Results 
\begin_inset CommandInset label
LatexCommand label
name "sec:Results"

\end_inset


\end_layout

\begin_layout Standard
In order to illustrate the potential of this method for morphometric analysis,
 we applied it as a tool to detect symmetry.
 The preliminary study consisted of 24 normal brains.
 We applied the BrainSuite surface extraction sequence, followed by automated
 generation of sulci and gyri 
\begin_inset CommandInset citation
LatexCommand cite
key "ajoshiISBI12"

\end_inset

.
 This produced 26 sulci and 24 gyral parcellations per cortical hemisphere.
 Next, the GPS coordinate representation was generated for all curves and
 surfaces.
 Symmetry between sulci (gyral regions) was estimated by comparing corresponding
 sulci (gyral regions) between the two hemispheres of a subject 
\begin_inset CommandInset citation
LatexCommand cite
key "ajoshi_miccai2012"

\end_inset

.
 
\end_layout

\begin_layout Standard
\begin_inset Float figure
wide false
sideways false
status open

\begin_layout Plain Layout
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
centering
\end_layout

\end_inset


\begin_inset Graphics
	filename fig21.png
	lyxscale 5
	width 75text%
	rotateOrigin center

\end_inset


\end_layout

\begin_layout Plain Layout
\begin_inset Caption

\begin_layout Plain Layout
(a) Shape symmetry measure of the sulci plotted on a smooth representation
 of an individual cortical surface.
 (b) Left to right hemisphere average shape difference is plotted as a color-cod
ed overlay.
 It is interesting to note that the post- and pre-central sulci show the
 maximal amount of left/right asymmetry.
 It is not surprising to see the cingulate sulcus (visible in the depth
 of the mesial view) show a great extent of relative symmetry.
 Note that the curves and surface representations capture different aspects
 of the folding pattern.
\end_layout

\end_inset


\begin_inset CommandInset label
LatexCommand label
name "Fig 2: Symmetry-2"

\end_inset

 
\end_layout

\end_inset


\end_layout

\begin_layout Standard
The results of the symmetry mapping are shown in Fig.
 
\begin_inset CommandInset ref
LatexCommand ref
reference "Fig 2: Symmetry-2"

\end_inset

.
 
\end_layout

\begin_layout Subsection*
Research Plan
\end_layout

\begin_layout Standard
The proposed research in this project is categorized into three topics:
 (1) theory, (2) experimental studies and (3) software, constituting a unique
 framework for brain image analysis.
\end_layout

\begin_layout Subsubsection*
(1) Theory
\end_layout

\begin_layout Standard
We have proposed usage of anisotropic operators both for curves as well
 as surface representations.
 This operator allows us to model the shapes of curves 
\begin_inset CommandInset citation
LatexCommand cite
key "ajoshi_miccai2012"

\end_inset

.
 For the surfaces, using mean curvature as the anisotropy term will allow
 us to distinguish the developable transformations and represent the cortical
 folds in the representation.
 We will investigate various properties of this proposed embedding, involving
 a way to numerically reconstruct the curves and surfaces from their GPS
 representations.
 Additionally, we will investigate the following extensions of the proposed
 methods:
\end_layout

\begin_layout Enumerate

\series bold
Statistics of Shapes:
\series default
 For a highly variable sulcus, such as the cingulate, the Euclidean representati
on does not satisfactorily aggregate and compare shapes.
 With the GPS representation, there is a common space to estimate the mean
 shape and the standard deviation of shapes.
 We propose to develop a statistical framework to analyze the shape of a
 cortical structures.
 With this framework, we can characterize the gross features of a structure,
 including its general shape and variability, modes and principle components
 of shape variation in a population.
 In addition, the statistics we will provide can be used to analyze a population
 with a specific trait against a normal population.
 
\end_layout

\begin_layout Enumerate

\series bold
Structural Correlation:
\series default
 In recent years, connectivity has taken a central role in the processing
 of brain data.
 In functional brain analysis, estimation of functional connectivity provides
 a basis for inference on whether brain structures form part of a network.
 The GPS representation allows the analysis of the correlation between two
 brain structures using their covariation in shape across a population.
 If the shapes of structures are correlated then this is an indication of
 structural connection, functional interaction or a common driving process
 in their development.
 Since the GPS representation is a spectral analysis tool, it allows the
 comparison of shapes at multiple resolutions.
 We will use the GPS representation to identify structures that covary across
 subjects.
 In addition, the multiple resolutions of the GPS representation allows
 comparison of shapes as to whether they covary with respect to global shape
 (low spatial frequencies) or local shape details (high spatial frequencies).
\end_layout

\begin_layout Subsubsection*
(2) Experimental studies
\end_layout

\begin_layout Standard
The experimental part will use a twins dataset available to us through our
 collaboration with the Laboratory of Neuro Imaging (LONI) at UCLA.
 This dataset consists of a 372 brain scans, 194 dizygotic and 178 monozygotic
 young adults, acquired at University of Queensland.
 We will perform two types of studies; both the studies will be performed
 both for the sulcal curves as well as the gyral surfaces for the whole
 cortex.
 
\end_layout

\begin_layout Enumerate

\series bold
Symmetry analysis:
\series default
 Similar to the data presented in the preliminary results, we will investigate
 symmetry properties of the cortical structures.
 In addition to the larger population will allow us to perform a hypothesis
 testing for symmetry by comparing interhemispherical difference within
 subjects to differences in randomly chosen subjects and hemispheres.
 The null hypothesis would be that the two groups are the same, and the
 rejection of null hypothesis would indicate symmetry.
 This study will 
\emph on
map symmetry of the sulcal and gyral shape
\emph default
s.
\end_layout

\begin_layout Enumerate

\series bold
Genetic influence:
\series default
 For this purpose, we will use twins dataset available to us.
 This study will use Falconer's heritability estimate in order to measure
 the additive-genetic influence on the cortical structures.
 This statistic compares within twin-pair variance to across the population
 variance to generate an intraclass correlation statistic (ICC).
 Falconer's heritability is a function of this ICC value.
 The properties of the representation described above allow us to define
 an inner product in the representation space, and hence variance, ICC and
 Falconer's statistics can be generalized to this space.
 We will perform a study using this framework to generate maps of 
\emph on
heritability of cortical shapes 
\emph default
and will compare them with the existing map of cortical thickness heritability.
 This will help in understanding the role played by additive genetics in
 the development of the cortex.
\end_layout

\begin_layout Paragraph

\series medium
The proposed experimental studies will enhance out understanding of the
 cortical folding patterns.
 They will also be indicative of the strength of the method allowing us
 to evaluate its wider applicability for other applications involving population
s.
\end_layout

\begin_layout Subsubsection*
(3) Software
\end_layout

\begin_layout Standard
We have developed a BrainSuite software pipeline for structural analysis
 of the brain that starts with a T1 MRI image and generates a triangulated
 surface representations of inner and outer cortical surfaces representing
 gray-white and gray-csf boundaries respectively.
 We also have developed SVREG software that takes these surfaces as inputs
 and generates a set of sulcal curves and gyral parcellation, automatically,
 by performing a coregistration to a single subject atlas.
 As a part of the proposed project, we will implement the shape analysis
 framework into the software suite and will make it freely available as
 a part of the BrainSuite software.
\end_layout

\begin_layout Subsection*
Summary
\end_layout

\begin_layout Standard
With the GPS representation of curves and surfaces we can analyze shapes
 quantitatively.
 The current techniques that are widely used in brain image analysis such
 as voxel and tensor based morphometry do not capture quantitatively the
 cortical shapes.
 The proposed methods will allow the investigators to capture and study
 this important aspect of the brain anatomy and therefore will have a broad
 applicability.
 It can potentially be applied to study the influence of disease, development,
 maturation, and genes on sulcal and gyral patterns.
 
\end_layout

\begin_layout Standard

\size tiny
\begin_inset CommandInset bibtex
LatexCommand bibtex
bibfiles "ajoshibib"
options "splncs03"

\end_inset


\end_layout

\end_body
\end_document
