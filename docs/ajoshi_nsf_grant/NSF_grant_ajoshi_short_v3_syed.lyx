#LyX 2.0 created this file. For more info see http://www.lyx.org/
\lyxformat 413
\begin_document
\begin_header
\textclass spie
\begin_preamble

%%%%%%%%% MASTER -- compiles the 4 sections



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                                                      %%
%%%%%%%%%% EXACT 1in MARGINS %%%%%%%                                   %%
\setlength{\textwidth}{6.5in}     %%                                   %%
\setlength{\oddsidemargin}{0in}   %% (It is recommended that you       %%
\setlength{\evensidemargin}{0in}  %%  not change these parameters,     %%
\setlength{\textheight}{8.5in}    %%  at the risk of having your       %%
\setlength{\topmargin}{0in}       %%  proposal dismissed on the basis  %%
\setlength{\headheight}{0in}      %%  of incorrect formatting!!!)      %%
\setlength{\headsep}{0in}         %%                                   %%
\setlength{\footskip}{.5in}       %%                                   %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%                                   %%
\newcommand{\required}[1]{\section*{\hfil #1\hfil}}                    %%
\renewcommand{\refname}{\hfil References Cited\hfil}                   %%
\bibliographystyle{plain}                                              %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%PUT YOUR MACROS HERE

%\includeonly{NSFsumm}
\end_preamble
\use_default_options false
\maintain_unincluded_children false
\language english
\language_package none
\inputencoding auto
\fontencoding default
\font_roman default
\font_sans default
\font_typewriter default
\font_default_family default
\use_non_tex_fonts false
\font_sc false
\font_osf false
\font_sf_scale 100
\font_tt_scale 100

\graphics default
\default_output_format default
\output_sync 0
\bibtex_command default
\index_command default
\paperfontsize 11
\spacing single
\use_hyperref false
\papersize letterpaper
\use_geometry false
\use_amsmath 1
\use_esint 1
\use_mhchem 0
\use_mathdots 0
\cite_engine basic
\use_bibtopic false
\use_indices false
\paperorientation portrait
\suppress_date false
\use_refstyle 0
\index Index
\shortcut idx
\color #008000
\end_index
\secnumdepth 3
\tocdepth 3
\paragraph_separation indent
\paragraph_indentation default
\quotes_language english
\papercolumns 1
\papersides 1
\paperpagestyle plain
\tracking_changes false
\output_changes false
\html_math_output 0
\html_css_as_file 0
\html_be_strict false
\author 743323549 "Syed Ashrafulla" 
\end_header

\begin_body

\begin_layout Title
Brain Shape Analysis using Spectral Geometry
\end_layout

\begin_layout Standard
Investigators: Anand A Joshi, David W Shattuck, Hanna Damasio and Richard
 M Leahy
\end_layout

\begin_layout Standard
Program: NSF Cognitive Neuroscience: http://www.nsf.gov/funding/pgm_summ.jsp?pims_i
d=5316, (application deadline Aug 27th) 
\end_layout

\begin_layout Subsection*
Project Summary
\end_layout

\begin_layout Standard
The proposed research will develop a framework for invariant representations
 
\change_deleted 743323549 1340083649
for
\change_inserted 743323549 1340083650
of
\change_unchanged
 
\change_deleted 743323549 1340084199
curves and surfaces that characterize 
\change_unchanged
the shapes of 
\change_deleted 743323549 1340083115
a sulcal curves and cortical surfaces
\change_inserted 743323549 1340083117
brain structures
\change_unchanged
 using 
\change_deleted 743323549 1340083120
methods of 
\change_unchanged
spectral geometry.

\change_inserted 743323549 1340084242
 Analysis of brain shapes has extensive applications in the study of disease
 progression, aging and brain asymmetry.

\change_unchanged
 
\change_deleted 743323549 1340083127
The human cerebral cortex is a highly convoluted sheet with rich and detailed
 folding patterns represented by sulci.
 
\change_unchanged
A quantitative analysis of 
\change_deleted 743323549 1340083133
shape of the cortical sheet and the sulcal folding patterning
\change_inserted 743323549 1340083139
the shapes of brain structures
\change_unchanged
 requires an invariant framework 
\change_deleted 743323549 1340083178
that invariantly represents the geometry of the sulcal and ROI shapes
\change_inserted 743323549 1340084213
capturing the geometry of a structure rather than its Euclidean representation
\change_unchanged
.
 
\change_deleted 743323549 1340084232
Analysis of the cortical shapes has extensively applications to study disease
 progression 
\begin_inset CommandInset citation
LatexCommand cite
key "Narr01"

\end_inset

, aging 
\begin_inset CommandInset citation
LatexCommand cite
key "prince_aging"

\end_inset

 and brain asymmetry 
\begin_inset CommandInset citation
LatexCommand cite
key "Blanton01"

\end_inset

.

\change_unchanged
 
\change_deleted 743323549 1340084262
However, the p
\change_inserted 743323549 1340084262
P
\change_unchanged
resent approaches do not reflect the shapes of the
\change_deleted 743323549 1340083682
 cortical sheet
\change_inserted 743323549 1340083317
se structures
\change_unchanged
 but focus instead on 
\change_inserted 743323549 1340083325
gross 
\change_unchanged
features such as length, depth and 3D location
\change_deleted 743323549 1340083338
, deformation tensor to align it with a reference atlas
\change_unchanged
.
 
\change_inserted 743323549 1340084406
This is a problem in the brain, where the human cerebral cortex is a highly
 convoluted sheet whose folding patterns, called sulci, offer rich anatomical
 information.
 
\change_deleted 743323549 1340083406
In order to address these issues
\change_inserted 743323549 1340083764
For 1D curves (sulci) and 2D cortical parcellations (ROI surfaces)
\change_unchanged
, we propose a model for shape analysis
\change_deleted 743323549 1340083417
 of
\change_unchanged
 
\change_deleted 743323549 1340083406
1D curves and 2D cortical parcellations (ROI surfaces)
\change_unchanged
 based on spectral geometry.
 
\change_deleted 743323549 1340083428
The problem of s
\change_inserted 743323549 1340083428
S
\change_unchanged
hape quantification of a manifold
\change_deleted 743323549 1340083786
 by studying eigenspectrum of a differential operators such as Laplace-Beltrami
 operator defined on the manifold
\change_unchanged
, often 
\change_deleted 743323549 1340084304
stated as
\change_inserted 743323549 1340084305
described as whether one
\change_unchanged
 `can 
\change_deleted 743323549 1340084430
one 
\change_unchanged
hear the shape of a drum'
\change_inserted 743323549 1340083458
,
\change_unchanged
 has led to development of spectral geometry as a subbranch of differential
 geometry.
 
\change_inserted 743323549 1340084345
In spectral geometry, scientists characterize the shape of a manifold by
 studying the eigenspectrum of differential operators on the manifold.
 
\change_deleted 743323549 1340084456
In
\change_inserted 743323549 1340084456
For
\change_unchanged
 computer vision, 
\change_deleted 743323549 1340083500
corresponding
\change_inserted 743323549 1340083500
the
\change_unchanged
 Global Point Signature (GPS) representation of 2D surfaces 
\begin_inset CommandInset citation
LatexCommand cite
key "Reuter2010,Rustamov2007"

\end_inset

 
\change_deleted 743323549 1340084466
and corresponding methods have been developed to quantify
\change_inserted 743323549 1340084468
helps quanitfy
\change_unchanged
 
\change_inserted 743323549 1340083510
the 
\change_unchanged
geometric 
\change_deleted 743323549 1340083513
'
\change_inserted 743323549 1340083513
`
\change_unchanged
shape' of an object by studying the eigensystem of the Laplace-Beltrami
 operator.
 The method exploits the isometry invariance of eigenfunctions
\change_deleted 743323549 1340083544
 and introduces
\change_inserted 743323549 1340083548
, introducing
\change_unchanged
 an invariant metric 
\change_deleted 743323549 1340084489
that quantifies
\change_inserted 743323549 1340084490
to quanitfy
\change_deleted 743323549 1340084495
 geometric
\change_unchanged
 shape differences.
 This representation has desirable properties, including stability, uniqueness
 and invariance to scaling and isometric transformation
\change_inserted 743323549 1340083562
s
\change_unchanged
.
 We 
\change_deleted 743323549 1340083572
have 
\change_unchanged
propose
\change_deleted 743323549 1340083574
d
\change_unchanged
 the use of 
\change_inserted 743323549 1340083567
the 
\change_unchanged
anisotropic 
\change_deleted 743323549 1340083569
h
\change_inserted 743323549 1340083569
H
\change_unchanged
elmholtz equation 
\change_deleted 743323549 1340083605
for studying curves as well as surfaces
\change_inserted 743323549 1340083619
to study the spectral geometry of curves and surfaces, utilizing the anisotropy
 of such objects
\change_unchanged
.
 In case of sulcal curves, 
\change_deleted 743323549 1340083624
we have used curvature term as an anisotry
\change_inserted 743323549 1340084532
this anisotropy is introduced via the curvature of the sulcus
\change_unchanged
.
 
\end_layout

\begin_layout Standard
The 
\bar under
intellectual merit
\bar default
 of the proposal comes from the unique combination of theoretical, empirical,
 and software elements that will substantially impact brain image analysis
\change_inserted 743323549 1340084024
.
 
\change_unchanged
The 
\emph on
theoretical
\emph default
 element relies on the development of spectral geometric methods for 
\change_deleted 743323549 1340084560
representationa and 
\change_unchanged
analysis of curves and surfaces.
 We will investigate various spectral operators and their eigenspect
\change_deleted 743323549 1340083830
rum
\change_inserted 743323549 1340083830
a
\change_unchanged
 to represent curves and surfaces
\change_inserted 743323549 1340083854
.

\change_unchanged
 
\change_deleted 743323549 1340083913
and will develop fast and efficient numerical implementations of the resulting
 partial differential equations.
 
\change_unchanged
We will also investigate an optimal way of 
\change_deleted 743323549 1340083884
combining
\change_inserted 743323549 1340083885
utilizing
\change_unchanged
 these models 
\change_deleted 743323549 1340083888
and their utility 
\change_unchanged
for specific neuroanatomical studies.

\change_inserted 743323549 1340084031
 
\change_unchanged
The 
\emph on
empirical
\emph default
 aspect of our approach relies on 
\change_deleted 743323549 1340084602
application of
\change_inserted 743323549 1340084602
using
\change_unchanged
 the
\change_deleted 743323549 1340084613
 representations for
\change_inserted 743323549 1340084614
invariant
\change_unchanged
 shape quantification for applications such as 
\change_inserted 743323549 1340083943
hemispherical 
\change_unchanged
symmetry, 
\change_deleted 743323549 1340083955
twin studies for quantifying 
\change_unchanged
genetic and environmental 
\change_deleted 743323549 1340083956
infludences
\change_inserted 743323549 1340083962
influence (via twin studies)
\change_unchanged
, and alterations of sulcal 
\change_deleted 743323549 1340083971
folding patterning 
\change_inserted 743323549 1340083972
patterns 
\change_unchanged
in neuropathological conditions such as autism.

\change_deleted 743323549 1340083984
 We will perform the 
\change_inserted 743323549 1340083987
These 
\change_unchanged
shape analysis studies 
\change_deleted 743323549 1340084633
to 
\change_inserted 743323549 1340084634
will 
\change_unchanged
evaluate the performance as well as the efficacy and robustness of our methods.

\change_inserted 743323549 1340084037
 
\change_unchanged
The 
\emph on
software
\emph default
 element relies on the development of open source implementations of the
 methods under this grant and their integration into the BrainSuite software
 package
\change_deleted 743323549 1340084067
 developed and distributed from USC by Dr.
 Leahy’s lab
\change_unchanged
.
\end_layout

\begin_layout Standard
The 
\bar under
broader impact
\change_deleted 743323549 1340084078
s
\change_unchanged

\bar default
 of developing spectral 
\change_deleted 743323549 1340084103
geometry based 
\change_unchanged
methods for shape analysis of 
\change_inserted 743323549 1340084107
the 
\change_unchanged
brain will 
\change_inserted 743323549 1340084125
be its ability to 
\change_unchanged
facilitate 
\change_deleted 743323549 1340084128
a 
\change_unchanged
quantitative analys
\change_deleted 743323549 1340084130
i
\change_inserted 743323549 1340084130
e
\change_unchanged
s of neurological 
\change_deleted 743323549 1340084136
analysis of 
\change_unchanged
conditions marked my alterations in sulcal patterning (e.g.
 autism).
 The proposed methods will work in conjunction with 
\change_deleted 743323549 1340084142
the 
\change_unchanged
existing methods (tensor-based / voxel-based morphometry)
\change_inserted 743323549 1340084683
 to provide
\change_unchanged
 
\change_deleted 743323549 1340084149
and will allow 
\change_unchanged
a powerful set of tools for quantitative shape analysis
\change_inserted 743323549 1340084694
.
 These tools can be applied on
\change_deleted 743323549 1340084696
 of
\change_unchanged
 the cerebral cortex 
\change_deleted 743323549 1340084170
for a variety of neuropathological conditions such as
\change_inserted 743323549 1340084701
to study
\change_unchanged
 disease, development, maturation, genetic analysis and group differences
 in sulcal patterning.
\end_layout

\begin_layout Subsection*
Shape Representation of Sulcal Curves and Cortical ROIs
\begin_inset CommandInset label
LatexCommand label
name "sec:GPS-Coordinates-Representation"

\end_inset


\end_layout

\begin_layout Standard

\change_deleted 743323549 1340084744
We have developed a method for automatic generation of sulci and ROIs on
 a cortical surface 
\begin_inset CommandInset citation
LatexCommand cite
key "ajoshiISBI12"

\end_inset

 using automatic cortical surface registration to a reference atlas.
 
\end_layout

\begin_layout Subsection*

\change_deleted 743323549 1340084753
Representation of sulcal curves and cortical ROI surfaces
\end_layout

\begin_layout Standard
Spectral geometry provides the basis to study the eigenspectrum of 
\change_deleted 743323549 1340084762
the 
\change_unchanged
sulcal curves and ROI surfaces.
 We consider the 1D and 2D case and propose a point-wise signature for the
 curves and surfaces.
 Motivated by spectral theory and corresponding work on 2D surfaces 
\begin_inset CommandInset citation
LatexCommand cite
key "Rustamov2007,Reuter2010"

\end_inset

, we model 
\change_deleted 743323549 1340084775
the 
\change_unchanged
cortical curves
\change_deleted 743323549 1340084782
 and 
\change_inserted 743323549 1340084782
(
\change_unchanged
surfaces
\change_inserted 743323549 1340084783
)
\change_unchanged
 as inhomogeneous vibrating strings
\change_deleted 743323549 1340084788
 and
\change_inserted 743323549 1340084788
 (
\change_unchanged
 membranes
\change_inserted 743323549 1340084790
)
\change_unchanged
.
 Their harmonic behavior is governed by the 1D
\change_deleted 743323549 1340084795
 and 
\change_inserted 743323549 1340084795
(
\change_unchanged
2D
\change_inserted 743323549 1340084796
)
\change_unchanged
 Helmholtz equation.
 Since our aim is to characterize the shape of a curve 
\begin_inset Formula $C$
\end_inset

 and ROI surface 
\begin_inset Formula $S$
\end_inset

, we use curvature 
\begin_inset Formula $\kappa(s)$
\end_inset

 to introduce 
\change_deleted 743323549 1340084805
an 
\change_unchanged
anisotropy into the equation.
 Similarly, for the surface, we use the mean curvature 
\begin_inset Formula $\kappa_{m}(s)$
\end_inset

 as an anisotropy term:
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\begin{array}{cc}
\text{curve} & \text{surface}\\
\begin{cases}
\ensuremath{\frac{\partial}{\partial s}\kappa(s)\frac{\partial}{\partial s}\Phi_{i}(s)} & \ensuremath{\frac{\partial}{\partial s}\kappa(s)\frac{\partial}{\partial s}\Phi_{i}(s)}=\lambda_{i}\Phi_{i}(s)\\
\Phi_{i}(s)|_{\partial C} & =0
\end{cases},\forall s\in C & \begin{cases}
\ensuremath{\nabla\cdot\kappa_{m}(s)\nabla\Phi_{i}(s)} & =\lambda_{i}\Phi_{i}(s)\\
\Phi_{i}(s)|_{\partial S} & =0
\end{cases},\forall s\in S
\end{array}
\]

\end_inset


\end_layout

\begin_layout Standard
where 
\begin_inset Formula $\partial C$
\end_inset

 is the set of the endpoints of the curve 
\begin_inset Formula $C$
\end_inset

 and 
\begin_inset Formula $\partial S$
\end_inset

 is the boundary of the cortical ROI.
 Denote the 
\change_deleted 743323549 1340084819
of 
\change_unchanged
eigenfunctions of this equation by 
\begin_inset Formula $\Phi_{i}$
\end_inset

 
\change_deleted 743323549 1340084826
and the
\change_inserted 743323549 1340084826
with
\change_unchanged
 eigenvalues 
\begin_inset Formula $\lambda_{i}$
\end_inset

 ordered by magnitude.
 We define the embedding manifold in the spectral domain by the map:
\end_layout

\begin_layout Standard
\begin_inset Formula $\ensuremath{GPS(p)=\left(\frac{1}{\sqrt{\lambda_{1}}}\Phi_{1}(p),\frac{1}{\sqrt{\lambda_{2}}}\Phi_{2}(p),\frac{1}{\sqrt{\lambda_{3}}}\Phi_{3}(p),\text{\ensuremath{\ldots}}\right)}.$
\end_inset


\end_layout

\begin_layout Standard
Thus, each point of the curve is embedded into an infinite dimensional space.
 
\change_deleted 743323549 1340084871
It should be noted that we cannot use the 1D Laplacian directly for this
 purpose because 1D shapes do not have a non-trivial intrinsic geometry.
 However, d
\change_inserted 743323549 1340084871
D
\change_unchanged
ue to the fundamental theorem of curves (two unit-speed plane curves which
 have the same curvature and torsion differ only by a rigid transformation),
 curvature and torsion define the curve uniquely up to rigid transformation.
 Furthermore, the curve can be recovered from the embedding by first recovering
 the curvature and torsion, and then using the Frenet-Serre formulas 
\begin_inset CommandInset citation
LatexCommand cite
key "docarmo"

\end_inset

.
 For 3D curves, this requires curvature and torsion
\change_deleted 743323549 1340084899
.
 H
\change_inserted 743323549 1340084899
; h
\change_unchanged
owever, the embedding defined above is based on curvature alone
\change_inserted 743323549 1340084905
 
\change_deleted 743323549 1340084907
, as
\change_inserted 743323549 1340084907
because
\change_deleted 743323549 1340084912
 the
\change_inserted 743323549 1340084913
 most
\change_unchanged
 sulcal curves 
\change_deleted 743323549 1340084917
analyzed in this paper had
\change_inserted 743323549 1340084917
have
\change_unchanged
 negligible torsion.
 
\change_deleted 743323549 1340084922
Some of the important and useful properties of this embedding are defined
 below.
 
\change_inserted 743323549 1340085104
This embedding has many favorable properties.
 First, its 
\change_unchanged
coordinates are isometry invariant as they depend only on the derivatives
 and curvature, 
\change_deleted 743323549 1340084947
which in turn are known to be 
\change_unchanged
dependent only on the shape.
 
\change_inserted 743323549 1340085113
Second, 
\change_deleted 743323549 1340085113
S
\change_inserted 743323549 1340085113
s
\change_unchanged
caling a 1D curve manifold by the factor 
\begin_inset Formula $\alpha$
\end_inset

 results in curvature scaled by the factor 
\begin_inset Formula $\alpha$
\end_inset

.
 Therefore, by normalizing the eigenvalues, 
\change_deleted 743323549 1340084963
shape can be compared regardless of the object's scale (and position as
 mentioned earlier)
\change_inserted 743323549 1340084974
we obtain scale-invariance
\change_unchanged
.
 
\change_inserted 743323549 1340085122
Third, 
\change_deleted 743323549 1340085123
C
\change_inserted 743323549 1340085123
c
\change_unchanged
hanges of the curve's shape result in continuous changes in its spectrum
\change_deleted 743323549 1340085130
.
 Consequently
\change_inserted 743323549 1340085130
 so
\change_unchanged
 the representation 
\change_deleted 743323549 1340084983
presented here 
\change_unchanged
is robust.
 
\change_inserted 743323549 1340085148
Fourth, i
\change_unchanged
n the embedding space, the inner product is given by the Green's function
 due to the identity: 
\begin_inset Formula $G(x_{1},x_{2})=\sum_{i}\frac{\Phi_{i}(x_{1})\Phi_{i}(x_{2})}{\lambda_{i}}$
\end_inset

.
 As a result, the GPS representation encodes both local and global 
\change_deleted 743323549 1340085003
curvature and 
\change_unchanged
shape information into the embedding.
 
\change_deleted 743323549 1340085151
Additionally
\change_inserted 743323549 1340085151
Finally
\change_unchanged
, in this infinite dimensional shape space, the metric is 
\change_inserted 743323549 1340085014
simply 
\change_deleted 743323549 1340085008
e
\change_inserted 743323549 1340085012
E
\change_unchanged
uclidean
\change_deleted 743323549 1340085019
, which simplifies analysis
\change_unchanged
.
 
\end_layout

\begin_layout Standard
\begin_inset Float figure
wide false
sideways false
status open

\begin_layout Plain Layout
\align center
\begin_inset Graphics
	filename shape_analysis.png
	lyxscale 10
	width 80text%

\end_inset


\begin_inset Caption

\begin_layout Plain Layout
(a) Inferior frontal sulcus highlighted in red; (b) first four color coded
 GPS coordinates; (c) GPS representation plotted from end to end of a sulcus;
 (d) automatically generated ROIs on cortical surface; (e) first five color
 coded GPS coordinates.
\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Subsection*
Preliminary Results 
\begin_inset CommandInset label
LatexCommand label
name "sec:Results"

\end_inset


\end_layout

\begin_layout Standard
In order to illustrate the potential of this method for morphometric analysis,
 we applied it for symmetry detection.
 The preliminary study consisted of 24 normal brains.
 We applied the BrainSuite surface extraction sequence
\change_inserted 743323549 1340085360
,
\change_unchanged
 followed by sulcal set and ROI parcellation generation 
\begin_inset CommandInset citation
LatexCommand cite
key "ajoshiISBI12"

\end_inset

.
 This produced 24x2 cortical surface hemisphere representations with 26
 sulci each.
 Next, the GPS coordinate representation was generated for all curves as
 well as ROI surfaces.
 
\change_deleted 743323549 1340085378
The s
\change_inserted 743323549 1340085378
S
\change_unchanged
ymmetry between 
\change_deleted 743323549 1340085384
the 
\change_unchanged
sulci
\change_inserted 743323549 1340085392
 and between regions
\change_unchanged
 was 
\change_deleted 743323549 1340085397
then 
\change_unchanged
estimated by comparing
\change_deleted 743323549 1340085176
 intra-hemisphere lef
\change_inserted 743323549 1340085189
corresponding sulci between two hemispheres in the same subjec
\change_unchanged
t
\change_inserted 743323549 1340085406

\begin_inset Note Note
status open

\begin_layout Plain Layout

\change_inserted 743323549 1340085411
cite MICCAI or journal
\change_unchanged

\end_layout

\end_inset


\change_unchanged
.
 
\begin_inset Wrap figure
lines 16
placement o
overhang 0in
width "50col%"
status collapsed

\begin_layout Plain Layout
\begin_inset Graphics
	filename Asymm_detection.png
	lyxscale 15
	width 45text%

\end_inset


\end_layout

\begin_layout Plain Layout
\begin_inset Caption

\begin_layout Plain Layout
(a) Shape symmetry measure of the sulci plotted on a smooth representation
 of an individual cortical surface.
 The black regions on the curves indicate that a significant symmetry was
 not found for those points.
 (b) Left to right hemisphere average shape difference is plotted as a colorcode
d overlay.
\end_layout

\end_inset


\begin_inset CommandInset label
LatexCommand label
name "Fig 4: Symmetry"

\end_inset

 
\end_layout

\end_inset

 The results of the symmetry mapping are shown in Fig.
 
\begin_inset CommandInset ref
LatexCommand ref
reference "Fig 4: Symmetry"

\end_inset


\change_deleted 743323549 1340085426
, both for sulcal curves as well as cortical surfaces
\change_unchanged
.
 It is interesting to note that the post- and pre-central sulci, together
 with the posterior segment of the superior temporal, the transverse temporal,
 the middle temporal and the inferior occipital sulci in the dorso-lateral
 view, show the maximal amount of left/right asymmetry
\change_deleted 743323549 1340085201
;
\change_inserted 743323549 1340085201
.

\change_unchanged
 
\change_deleted 743323549 1340085203
o
\change_inserted 743323549 1340085203
O
\change_unchanged
n the mesial view the collateral, the supraorbital, the occipito-parietal
 and long stretches of the cingulate sulci are also extremely asymmetric.
 It is not surprising to see the cingulate sulcus (visible in the depth
 of the mesial view) to show a great extent of relative symmetry.
 
\end_layout

\begin_layout Subsection*
Proposed Extensions
\end_layout

\begin_layout Standard
With the GPS representation of curves we can analyze shapes in their native
 form.
 Thus, we can 
\change_deleted 743323549 1340085453
perform comparisons of
\change_inserted 743323549 1340085454
compare
\change_unchanged
 shapes without 
\change_deleted 743323549 1340085459
worrying about
\change_unchanged
 the effects of Euclidean transformations or the Euclidean embedding
\change_deleted 743323549 1340085464
 on such comparisons
\change_unchanged
.
 We propose 
\change_deleted 743323549 1340085467
two
\change_inserted 743323549 1340085467
three
\change_unchanged
 avenues 
\change_deleted 743323549 1340085473
that we will pursue 
\change_unchanged
to increase the ability of scientists 
\change_deleted 743323549 1340085478
to evaluat
\change_inserted 743323549 1340085479
in 
\change_unchanged
e
\change_inserted 743323549 1340085482
valuating
\change_unchanged
 the inherent structure of shapes via the 
\change_inserted 743323549 1340085490

\begin_inset Note Note
status open

\begin_layout Plain Layout

\change_inserted 743323549 1340085494
maybe 
\begin_inset Quotes eld
\end_inset

anisotropic
\begin_inset Quotes erd
\end_inset

?
\change_unchanged

\end_layout

\end_inset


\change_unchanged
GPS representation
\change_deleted 743323549 1340085487
 of curves
\change_unchanged
.
\end_layout

\begin_layout Subsubsection*
Statistics of Shapes
\end_layout

\begin_layout Standard
Given many samples of
\change_deleted 743323549 1340085505
, say, the shape
\change_inserted 743323549 1340085512
a shape, such as
\change_deleted 743323549 1340085509
 of
\change_unchanged
 the cingulate, the Euclidean representation 
\change_deleted 743323549 1340085518
is unable to
\change_inserted 743323549 1340085519
does not
\change_unchanged
 satisfactorily aggregate and compare each sample because it is sensitive
 to small changes in shape.
 With the GPS representation
\change_deleted 743323549 1340085527
 of curves
\change_unchanged
, we have a common basis -- resistant to small shape changes -- for 
\change_deleted 743323549 1340085538
calculation
\change_inserted 743323549 1340085539
estimation
\change_unchanged
 of the mean shape and the standard deviation of shape.
 We propose development of a statistical framework to analyze the shape
 of a cortical region over many samples.
 With this framework, we can then characterize the gross features of a
\change_deleted 743323549 1340085559
n
\change_unchanged
 structure in the brain, including its general shape and the variability
 in that 
\change_deleted 743323549 1340085566
region
\change_inserted 743323549 1340085567
shape
\change_unchanged
 over a population.
 In addition, the statistics we will provide can be used for analysis of
 a normal population against a population with a specific trait, be it genetic,
 clinical, etc.
 The statistics of shapes will allow scientists to analyze the 
\change_deleted 743323549 1340085592
behavior
\change_inserted 743323549 1340085600
characteristics
\change_unchanged
 of a brain structure 
\change_deleted 743323549 1340085589
within the framework of shapes
\change_unchanged
, unhindered by the 3D embedding 
\change_deleted 743323549 1340085606
with which the shape is drawn
\change_inserted 743323549 1340085609
where that structure is drawn
\change_unchanged
.
\end_layout

\begin_layout Subsubsection*
Anatomical Connectivity
\end_layout

\begin_layout Standard
In recent years, connectivity has taken a central role in the processing
 of brain data.
 In functional brain analysis, estimation of connectivity provides a basis
 for inference on whether brain structures are related.
 With the GPS representation
\change_deleted 743323549 1340085626
 of curves
\change_unchanged
, we can analyze the correlation between two brain structures by their covariati
on 
\change_inserted 743323549 1340085633
in shape 
\change_unchanged
across a population.
 If 
\change_deleted 743323549 1340085641
two structures covary -- so that 
\change_unchanged
the change in shape of one structure in a subject is coupled with the change
 in shape of the second
\change_deleted 743323549 1340085646
 --
\change_inserted 743323549 1340085646
,
\change_unchanged
 then we can infer that these two brain areas are related in 
\change_deleted 743323549 1340085652
their 
\change_unchanged
form
\change_deleted 743323549 1340085663
 and function
\change_unchanged
.
 Since the GPS representation
\change_deleted 743323549 1340085671
 of curves
\change_unchanged
 is a spectral analysis tool, 
\change_deleted 743323549 1340085676
it provides a way to
\change_inserted 743323549 1340085677
we can
\change_unchanged
 compare shapes at multiple resolutions, similar to the benefits of coherence
 in functional connectivity analysis.
 We will use the GPS representation
\change_deleted 743323549 1340085691
 of curves
\change_unchanged
 to determine structures that covary across subjects, indicating they are
 anatomically linked.
 In addition, with the multiple resolutions of the GPS representation, we
 can compare shapes as to whether they covary at low spatial frequencies
\change_deleted 743323549 1340085710
, representing
\change_unchanged
 
\change_inserted 743323549 1340085712
(
\change_deleted 743323549 1340085714
gross
\change_inserted 743323549 1340085715
global
\change_unchanged
 shape
\change_deleted 743323549 1340085720
 relationships,
\change_inserted 743323549 1340085720
)
\change_unchanged
 or high spatial frequencies
\change_deleted 743323549 1340085724
, representing finer
\change_unchanged
 
\change_inserted 743323549 1340085727
(local 
\change_unchanged
shape
\change_deleted 743323549 1340085730
 changes
\change_inserted 743323549 1340085730
)
\change_unchanged
.
\end_layout

\begin_layout Subsubsection*
Subcortical Shapes and Shape Bundles
\end_layout

\begin_layout Standard
In our 
\change_inserted 743323549 1340085737
preliminary 
\change_unchanged
work, we focused on the shape of sulci on the cortex.
 We will extend this work to subcortical structures such as the hippocampus.
 The shape of subcortical structures plays an important role in cognitive
 ability as well as human behavior.
 With the GPS representation, we can analyze subcortical structures in the
 clinical setting to determine whether diseases such as autism are linked
 to deformations in brain structures.
 In addition, the emerging field of diffusion tensor imaging provides an
 estimate of white matter tract locations.
 We can extend GPS representations to look at these tracts.
 Since the drawn tracts usually occur in bundles, we plan to modify the
 GPS representation to account for multiple tracts each representing one
 key white matter tract such as the corpus callosum.
\end_layout

\begin_layout Standard

\size tiny
\begin_inset CommandInset bibtex
LatexCommand bibtex
bibfiles "ajoshibib"
options "splncs03"

\end_inset


\end_layout

\end_body
\end_document
