%% LyX 2.0.6 created this file.  For more info, see http://www.lyx.org/.
%% Do not edit unless you really know what you are doing.
\documentclass[english]{llncs}
\usepackage{lmodern}
\usepackage[T1]{fontenc}
\usepackage[latin9]{inputenc}
\usepackage{refstyle}
\usepackage{amsmath}
\usepackage{graphicx}
\usepackage{esint}

\makeatletter

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% LyX specific LaTeX commands.

\AtBeginDocument{\providecommand\figref[1]{\ref{fig:#1}}}
\AtBeginDocument{\providecommand\eqref[1]{\ref{eq:#1}}}
\AtBeginDocument{\providecommand\subref[1]{\ref{sub:#1}}}
\RS@ifundefined{subref}
  {\def\RSsubtxt{section~}\newref{sub}{name = \RSsubtxt}}
  {}
\RS@ifundefined{thmref}
  {\def\RSthmtxt{theorem~}\newref{thm}{name = \RSthmtxt}}
  {}
\RS@ifundefined{lemref}
  {\def\RSlemtxt{lemma~}\newref{lem}{name = \RSlemtxt}}
  {}


\makeatother

\usepackage{babel}
\begin{document}

\title{Cortical Shape Analysis using the Anisotropic Global Point Signature }


\author{}


\author{Anand A Joshi$^{1,3}$, Syed Ashrafulla$^{1}$, David W Shattuck$^{2}$,
Hanna Damasio$^{3}$ and Richard M Leahy$^{1}$ %
\thanks{This work was supported by NIH grants P41 EB015922 and R01 NS074980.%
}}


\institute{$^{1}$ Signal and Image Processing Institute, University of Southern
California, Los Angeles, CA\\
$^{2}$ Laboratory of Neuro Imaging, University of California, Los
Angeles, CA\\
$^{3}$ Brain and Creativity Institute, University of Southern California,
Los Angeles, CA }
\maketitle
\begin{abstract}
We present a novel shape representation that characterizes the shape
of a surface in terms of a coordinate system based on the eigensystem
of the anisotropic Laplace-Beltrami operator.  In contrast to the
existing techniques, our representation can capture developable transformations
and is therefore useful for analysis of cortical folding patterns.
This representation has desirable properties including stability,
uniqueness and invariance to scaling as well as isometric transformations.
Additionally, the resulting shape space has a standard Euclidean metric
simplifying shape analysis. We also present an approach that provides
a fast and accurate computational method for solving the eigensystem
using a finite element formulation. We demonstrate the utility of
this representation for two brain shape analysis applications: quantifying
symmetries in shape between the two cortical hemispheres and finding
variance of cortical surface shapes across populations.
\end{abstract}

\section{Introduction \label{sec:Introduction}}

Quantification, matching and comparison of cortical shapes are challenging
problems with wide utility \cite{thompson2000mathematical,prince_aging}.
Most of the traditional approaches for analyzing brain shapes are
deformation-based. Quantitative analysis of anatomical shape differences
is performed with these approaches by analyzing the deformation required
to warp a subject brain to a template brain.  For example, tensor-based
morphometry \cite{lepore2008generalized} analyzes local linear approximations
(the deformation tensors) of the deformation field. Alternate methods
such as deformation-based morphometry \cite{ashburner1998identifying}
and pattern-based morphometry \cite{gaonkar2011pattern} use different
aspects of the deformation field. 

While quantification of shape differences by analysis of the deformation
field is a plausible approach, it suffers from a number of disadvantages.
The results of these methods depend to a large extent on the image
registration method used. Only regions where registration works well
-- typically subcortical structures -- tend to show high statistical
power \cite{lepore2008generalized}. Using the deformation field
as a shape descriptor often magnifies the effects of registration
errors. In addition, it is not clear if the shape details are indeed
encoded in the deformation tensor as this tensor is a local linear
approximation of the deformation field. This is especially the case
for large shape differences that require large deformations. Also,
there is no ground truth deformation for the purpose of aligning one
brain to another; in other words, registration may provide only one
of multiple equally accurate deformation fields from one brain image
to another. Finally, the deformation field does not define a shape
space on the cortex in the sense that sulcal and gyral shapes are
not directly encoded in the deformation field.

Recent approaches for brain shape analysis are based on spectral geometry,
in which the shape of a manifold is characterized by the eigenspectrum
of a differential operator defined on the manifold. An invariant
representation of a 2D surface can be generated using the Global Point
Signature (GPS) representation, which is based on the eigensystem
of the isotropic Laplace-Beltrami operator defined on that surface
\cite{Reuter2010,Rustamov2007}.
\begin{figure}
\centering{}\vspace{-0.1in}\includegraphics[width=1\textwidth]{figures/MeanGaussianCurvature2}\vspace{-0.1in}\caption{Absolute value of mean and Gaussian curvature of a cortical surface.\label{fig:Absolute-value-of} }
\vspace{-0.1in}
\end{figure}
 Methods using GPS are not directly applicable to cortical shape
analysis because the isotropic Laplace-Beltrami operator only captures
the intrinsic geometry of the surface. The majority of the curvature
information of the cortex is in the mean curvature, as seen in \figref{Absolute-value-of},
which is extrinsic to the surface. 3D shape descriptors such as spherical
harmonics \cite{kazhdan2003rotation} are not convenient for cortical
shape analysis since such descriptors require an impractically large
number of basis functions and do not efficiently encode shape information
related to elastic deformations of shapes.  This paper presents
an approach for shape analysis of 2D surface patches using an anisotropic
version of the Laplace-Beltrami operator as described in the next
section. 


\section{Materials and Methods }


\subsection{AGPS Shape Representation \label{sub:aGPS_sec}}

We assume as input an anatomically labeled cortical surface representation
such as obtained by BrainSuite \cite{Shattuck2002,joshi2012method}.
Motivated by spectral theory \cite{Rustamov2007,Reuter2010,ajoshi_miccai2012},
we model a surface $S$ representing a cortical region as an inhomogeneous
vibrating membrane. Its harmonic behavior is thus governed by the
2D anisotropic Helmholtz equation where the mean curvature $\kappa(s)$
is used to introduce anisotropy:
\begin{equation}
\begin{cases}
\ensuremath{\nabla\cdot\kappa(s)\nabla\Phi(s)} & =\lambda\Phi(s)\\
\frac{\partial\Phi(s)}{\partial\vec{n}}|_{\partial S} & =0
\end{cases},\forall s\in S,\label{eq:main_LB}
\end{equation}
where $\nabla$ denotes a gradient operator defined in the geometry
of the surface, $\Phi(s)$ represents an eigenfunction with eigenvalue
$\lambda$, $\partial S$ is the boundary of the surface patch and
$\vec{n}$ is the normal to the surface. We use the eigenfunctions
and eigenvalues to define the Anisotropic Global Point Signature (AGPS)
embedding of the surface $S$ in the spectral domain by the map: 
\begin{equation}
\ensuremath{\mathrm{AGPS}(s)=\left(\frac{1}{\sqrt{\lambda_{1}}}\Phi_{1}(s),\frac{1}{\sqrt{\lambda_{2}}}\Phi_{2}(s),\frac{1}{\sqrt{\lambda_{3}}}\Phi_{3}(s),\text{\ensuremath{\ldots}}\right)},\: p\in S\label{eq:aGPS_rep}
\end{equation}
where $\Phi_{1},\Phi_{2},\ldots$ are eigenfunctions with corresponding
eigenvalues $\lambda_{1},\lambda_{2},\ldots$ arranged in ascending
order. Thus, each point of the manifold is embedded into an infinite-dimensional
space. The importance of modeling anisotropy in the representation
is illustrated in \figref{need_for_anisotropy}. 
\begin{figure}
\centering{}\vspace{-0.1in}\includegraphics[width=1\textwidth]{figures/gyri_representation}\vspace{-0.1in}\caption{Introduction of an anisotropic term helps characterize developable
transformations. Use of the isotropic Laplace-Beltrami operator does
not find any shape differences between the two elliptical patches
as the transformation between the two is developable (top right).
Use of an anisotropic operator with a mean curvature anisotropy helps
in capturing the shape differences (bottom right). The resulting AGPS
coordinates are also shown in the bottom row. }
\label{fig:need_for_anisotropy} \vspace{-0.1in}
\end{figure}
  A developable transformation (bending) applied to an elliptical
surface does not affect the intrinsic geometry of the surface and
therefore the Gaussian curvature remains unchanged. However, the extrinsic
geometry is altered by this transformation resulting in changes in
mean curvature.  It can be seen that the bending does not change
the isotropic Laplace-Beltrami eigenspectrum and therefore the shape
change is not detected by GPS. However, the anisotropy introduced
via the mean curvature allows the AGPS to successfully capture the
change in the shape.

The AGPS embedding presented in \eqref{aGPS_rep} has many favorable
properties. First, its coordinates are isometry invariant as they
depend only on the derivatives and curvature, which in turn are dependent
only on the shape. Second, scaling the manifold by a factor $\alpha$
results only in scaling mean curvature by $1/\alpha$. Therefore,
we can obtain scale-invariance if desired by normalizing the eigenvalues;
however, for the specific application of cortical shape representation,
we do not want scale-invariance. Third, changes of the manifold's
shape result in continuous changes in the spectrum so the representation
is stable. Fourth, in the embedding space, the inner product is given
by the anisotropic Green's function due to the identity $G(s_{1},s_{2})=\sum_{k}\frac{\Phi_{k}(s_{1})\Phi_{k}(s_{2})}{\lambda_{k}},\, s_{1},s_{2}\in S$
\cite{Reuter2010}. As a result, the AGPS representation encodes anisotropic
diffusion distances \cite{lipman2010biharmonic} on the surface. In
addition, both local and global shape information is represented in
the embedding. Finally, in this infinite-dimensional shape space,
the metric is Euclidean, allowing standard $\ell^{2}$ space analysis. 

This invariant spectral geometric representation of surfaces has interesting
physical interpretations. The surfaces can be modeled as vibrating
membranes and the vibrations are damped proportionally to the mean
curvature at each point. The anisotropic Laplace-Beltrami eigenspectrum
corresponds to the modes of vibrations of this membrane (Fig. \ref{fig:need_for_anisotropy}
(bottom)). Thus the AGPS representation encodes information about
the modes of vibration of membranes (surface patches) as the basis
for shape modeling. The AGPS shape representation intuitively encodes
curvature characteristic at and around the points on the manifold.
Perturbations at a point in a shape lead to local changes in curvature
around that point which are captured in higher-order AGPS coordinates.
On the other hand global shape changes lead to curvature changes everywhere
in the shape which are captured by lower-order AGPS coordinates. Due
to this association between AGPS coordinates and the spatial extent
of shape changes, AGPS-based comparisons provide a natural description
of changes in shape at different scales. 


\subsection{Numerical Implementation}

To solve \eqref{main_LB} we first compute the anisotropy term represented
by mean curvature using the method described in \cite{meyer2002discrete}.
Next, we use a fi{}nite element method (FEM) to discretize the anisotropic
Helmholtz equation (\ref{eq:main_LB}). We discretize the derivative
operators using FEM directly in the geometry of the surface mesh,
and therefore we do not need to explicitly compute the Riemannian
metric coefficients as is often done if the surfaces are mapped to
a plane or sphere \cite{thompson2000mathematical}. We choose linear
FEMs for functions and Galerkin's formulation \cite{gdsmithpdebook}
for robustness to tessellation errors. Let $\Phi(s)=\sum_{i}\phi_{i}e_{i}(s)$
be an eigenfunction and $N(s)=\sum_{i}\eta_{i}e_{i}(s)$ be a `test
function', each represented as weighted sums of linear elements $e_{i}(s)$.
The eigenvalue problem from \eqref{main_LB} then becomes:

\begin{align*}
\left(\nabla\cdot\kappa(s)\nabla\right)\Phi(s)= & \lambda\Phi(s)\\
\implies\int_{S}\left(\nabla\cdot\kappa(s)\nabla\Phi(s)\right)N(s)ds= & \lambda\int_{S}\Phi(s)N(s)ds\\
\implies-\int_{S}\kappa(s)\nabla\Phi(s)\nabla\eta N(s)ds= & \lambda\int_{S}\Phi(s)N(s)ds
\end{align*}
where the latter follows using integration by parts and Neumann boundary
conditions in \eqref{main_LB}. Substituting the FEM into this equation,
we get:

\begin{align}
-\sum_{i}\sum_{j}\phi_{i}\eta_{j}\kappa_{ij}\int\nabla e_{i}(s)\nabla e_{j}(s)ds= & \lambda\sum_{i}\sum_{j}\phi_{i}\eta_{j}\int e_{i}(s)e_{j}(s)ds\nonumber \\
\implies KS\Phi= & -\lambda M\Phi\label{eq:matrixeq}
\end{align}
where $\kappa_{ij}=\frac{\kappa_{i}+\kappa_{j}}{2}$ is the average
of curvatures calculated at points $i$ and $j$, $K$ is a matrix
with $i^{th}$ row and $j^{th}$ column given by $\kappa_{ij}$, and
$\Phi$ is a column vector with $i^{th}$ entry given by $\phi_{i}$.
For a triangulated surface mesh with linear elements, the element-wise
matrix is given by $M_{el}=\frac{A_{el}}{12}\left[\begin{array}{ccc}
2 & 1 & 1\\
1 & 2 & 1\\
1 & 1 & 2
\end{array}\right]$ and the element-wise stiffness matrix is given by $S_{el}=D_{x}D_{x}+D_{y}D_{y}$
where $D_{x}$ and $D_{y}$ are discretizations of derivatives in
the $x$ and $y$ directions, respectively. The mass and stiffness
matrices $M$ and $S$ are obtained from the corresponding element-wise
matrices $M_{el}$ and $S_{el}$ respectively by finite element matrix
assembly procedures as described in \cite{gdsmithpdebook}. The matrix
equation (\ref{eq:matrixeq}) is a generalized eigenvalue problem
that can be solved using standard methods such as the QZ method in
the Matlab function $\mathtt{eigs}$. For this analysis, we chose
to approximate the infinite-dimensional AGPS by its first seven coordinates
based on the spread of the eigenvalue spectrum.  One example of the
computed $\mathrm{AGPS}$ coordinates for the surface patch representing
the left superior frontal gyrus is shown in \figref{gyral_spectrum}.
\begin{figure}[t]
\centering{}\vspace{-0.1in}\includegraphics[width=0.75\textwidth]{figures/agps_rep}\vspace{-0.1in}\caption{An AGPS example: (a) automatically generated parcellations of a cortical
surface; (b) first five color-coded AGPS coordinates of the left superior-frontal
gyrus.}
\label{fig:gyral_spectrum}\vspace{-0.1in}
\end{figure}
 


\subsection{Brain Shape Analysis using AGPS\label{sub:Brain-Shape-Analysis-using-aGPS}}

In order to illustrate the potential of the proposed representation
for cortical shape analysis, we apply AGPS for two group studies of
$N=24$ subjects: (1) asymmetry analysis and (2) variability analysis.
 For this purpose, we generated the AGPS coordinates for each vertex
using the method described in the previous section. While it is possible
to get a full description of shape change at different scales by analyzing
coordinate-wise AGPS differences, we summarize the shape difference
with $\ell^{2}$-norm since the shape space admits an Euclidean metric.


To map symmetry between the left and right hemispheres of subjects,
we first define $\mathrm{AGPS}_{L}$ and $\mathrm{AGPS}_{R}$ as the
AGPS representations of left and right cortical hemispheres of a subject
and transfer them to the common atlas space for comparison. We register
the atlas's right hemisphere $R$ to the atlas's left hemisphere $L$
forming a correspondence denoted by $\Psi:R\rightarrow L$. With
this correspondence, we can then compute mean AGPS distance between
hemispheres at each vertex $s$ in the atlas's right hemisphere by
$D\left(s\right)=\left\Vert \mathrm{AGPS}_{L}(\Psi(s))-\mathrm{AGPS}_{R}(s)\right\Vert _{2}$.
To find group asymmetry, we average $D\left(s\right)$ over all the
subjects.

Shape variability on the cortex can be found by estimating the population
variance of the AGPS coordinates. We compute the AGPS coordinates
for each subject in the native space and then transfer these coordinates
to the standard atlas space. We then estimate the population variance
at each vertex $s$ in the atlas space by $\sigma^{2}(s)=\frac{1}{N}\sum_{n=1}^{N}\left(\mathrm{AGPS}_{n}\left(s\right)-\frac{1}{N}\sum_{m=1}^{N}\mathrm{AGPS}_{m}\left(s\right)\right)^{2}$.
Note that we use the correspondence established using surface registration
for comparing the shapes in the atlas domain, but we do not use the
deformation field as a shape descriptor due to the reasons discussed
in section \ref{sec:Introduction}.


\section{Results \label{sec:Results-1}}


\subsection{Asymmetry across hemispheres\label{sub:Asymmetry-in-parcellations}}

After parcellation,
\begin{figure}[t]
\begin{centering}
\vspace{-0.1in}\includegraphics[width=0.75\textwidth]{figures/symm_mapping}\vspace{-0.1in}
\par\end{centering}

\caption{Left-to-right hemisphere average shape difference, based on gyral
AGPS. The color-coded overlay shows the degree of symmetry (blue)
and asymmetry (red).}
\label{fig:symmetrymapping}\vspace{-0.1in}
\end{figure}
 the left and right hemispheres of a subject's brain contain homologous
regions that may differ in shape. This hemispheric brain asymmetry
is possibly related to functional lateralization due to evolutionary,
hereditary and developmental factors \cite{toga2003mapping}. In order
to map cortical asymmetry, we use the procedure described in \subref{Brain-Shape-Analysis-using-aGPS}
with results shown in \figref{symmetrymapping}. The most asymmetric
regions are in the inferior sector of the pre- and post-central gyrus,
the mid portion of the middle temporal gyrus and posterior portion
of the inferior temporal gyrus, and, to a lesser extent in the mesial
sector of the superior frontal gyrus. One slightly surprising result
is that there is minimal asymmetry at the end of the Sylvian fissure
and needs further investigation. 


\subsection{Variability in Shapes \label{sub:Variability-in-parcellations}}

In addition to analyzing shape variability between cortical hemispheres
across the subject population, we can analyze the variability in cortical
shape over a population. In \figref{shape_var} on the left (lateral
aspect of the hemisphere) we see high variability in the posterior
sector of the middle and inferior temporal gyri and the inferior parietal
lobule hugging the posterior end of the Sylvian fissure, as well as
in the frontal operculum; on the right (mesial aspect of the hemisphere)
the areas of maximal variability are found in the pre-cuneus, the
anterior sector of the cingulate gyrus and in the anterior sector
of the parahippocampal gyrus (site of the maximal variance). Most
of these regions are in the association cortex. 
\begin{figure}[t]
\centering{}\vspace{-0.1in}\includegraphics[width=0.75\textwidth]{figures/surf_var}\vspace{-0.1in}\caption{Population variance of cortical shape. The color-coded overlay is
the variance of the AGPS representation, plotted on an inflated representation
of the cortex.}
\label{fig:shape_var}\vspace{-0.1in}
\end{figure}



\section{Discussion and Conclusion}

This paper presents a new invariant shape representation, AGPS, that
captures differences in surface shapes due to developable transformations,
a critical class of transformations in the analysis of cortical folds.
The shape space generated by this representation is $\ell^{2}$, readily
allowing the use of existing standard statistical techniques for shape
analysis. We illustrated the benefits of AGPS in quantifying shape
differences across hemispheres and shape variation across subjects.


The question of whether a surface is unique (within an isometry)
given its AGPS coordinates is related to the existence of Bonnet surfaces,
i.e. surfaces that are not completely defined by their metric and
mean curvature \cite{bobenko1998bonnet}. However, it is unlikely
that cortical surfaces would suffer from such an ambiguity. Another
possible ambiguity is in the order of eigenfunctions of the anisotropic
Laplace-Beltrami operator in cases where there are repeated eigenvalues,
which can occur if there are certain symmetries in the shape. An algorithm
for resolving this ambiguity can be found in \cite{Jain2006}. For
the purpose of brain shape analysis, we did not encounter any ambiguity
possibly due to shapes not having axes of symmetry.

Many aspects of the preprocessing can affect the AGPS representations
of cortical regions, thus introducing sources of error in cortical
shape analysis. More specifically, it is unclear whether different
parcellation schemes can result in different shape analysis results.
We note that recent registration and labeling methods can parcellate
the brain with accuracy approaching manual labeling \cite{dimitrios_nimg_sulci}.
In the future, we plan to explore the effect of parcellation decisions
on cortical shape analysis using AGPS.

The presented AGPS representation and analysis methods can be applied
for applications other than cortical shape analysis. AGPS representations
can help in various computer vision applications requiring shape analysis
where the intrinsic geometry does not fully capture the shape.  In
addition, this representation can also be extended to 3 or more dimensions
using approaches presented in this paper for 2D surfaces and in previous
work for 1D curves \cite{ajoshi_miccai2012}. 

{\scriptsize{\bibliographystyle{splncs03}
\bibliography{ajoshibib}
}}
\end{document}
