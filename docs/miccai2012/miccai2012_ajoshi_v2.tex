%% LyX 2.0.2 created this file.  For more info, see http://www.lyx.org/.
%% Do not edit unless you really know what you are doing.
\documentclass[english]{llncs}
\usepackage{lmodern}
\usepackage[T1]{fontenc}
\usepackage[latin9]{inputenc}
\usepackage{textcomp}
\usepackage{amsmath}
\usepackage{graphicx}
\usepackage{esint}
\usepackage{babel}
\begin{document}

\title{An Invariant Shape Representation using Anisotropic Helmholtz Equation}


\author{Anand~A.~Joshi$^{1,3}$, Syed~Ashrafulla$^{1}$, David~W.~Shattuck$^{2}$,
Hanna~D.~Damasio\textmd{\textup{$^{3}$}} and Richard~M.~Leahy$^{1}$}


\titlerunning{An Invariant Shape representation using anisotropic Helmholtz equation}


\authorrunning{Joshi AA, Ashrafulla A, Shattuck DW, Damasio HD and Leahy RM.}


\institute{$^{1}$Signal and Image Processing Institute, University of Southern
California, Los Angeles, CA 90089, USA \\
$^{2}$Laboratory of Neuro Imaging, University of California, Los
Angeles, CA 90095, USA \\
$^{3}$Brain and Creativity Institute, University of Southern California,
Los Angeles, CA 90089, USA %
\thanks{This work is supported in part by NIH grants P41 RR013642 and R01
NS074980 %
}}
\maketitle
\begin{abstract}
We present a novel shape representation for curves using anisotropic
Helmholtz equation. The shape is represented in terms of a coordinate
system based on eigensystem of the anisotropic Helmholtz equation.
The method exploits the isometry invariance of eigenfunctions and
introduces an invariant metric that quantifies the shape differences.
This representation has properties such as stability, uniqueness and
invariance to scaling and isometric transformation. Additionally,
the resulting shape space has a euclidean metric thus simplifying
the shape analysis. The metric is used for a registration method that
finds bijective and smooth point correspondences between shapes and
defines a point-wise shape distance. When the curves are irregularly
sampled, this approach also provides a fast and accurate computational
way of solving the eigensystem using a finite element formulation.
The shape presentation is applied to mapping symmetry between the
sulcal shapes of the cortex. We demonstrate that this representation
is able to quantify the shapes in an accurate and invariant manner.
\end{abstract}

\section*{Introduction}

The human cerebral cortex is a highly convoluted sheet with rich and
detailed folding patterns represented by sulci (sulcal curves). Sulci
are fissures in the cortical surface used as anatomical landmarks.
These sulcal landmarks have been extensively used for cortical registration
and lately, there is an emerging interest in analyzing the geometry
of these patterns to study disease progression\cite{Narr01}, aging
\cite{prince_aging}, brain asymmetry \cite{Blanton01}, to name just
a few, using 3D coordinates of sulci. However, these approaches do
not reflect the shapes of the sulci, but focus on features such as
length, depth and 3D location.

Quantification, matching and classification of shapes of sulci is
a challenging problem with a long history, however, it remains unsolved.
Some of the methods that use geometrical features for curve matching
include signature strings \cite{Ernst1989} and resultant string matching
\cite{Wolfson1990}. Spectral graphs \cite{Carcassoni2003} and fast
marching \cite{Frenkel2003} attempt to use graph theory to match
two curves point-by-point while minimizing the cost; the curves have
been broken up \cite{Serra1995} into line segments \cite{Sebastian2003}
for similarity matching \cite{Latecki2000}. In addition, turning
angles and distances \cite{Chen2008} and areas of enclosed regions
\cite{Xu2009} have used the local geometry of a curve to define its
true nature. Recent methods involve the distributions of distances
from all points on a curve to a reference point \cite{Viswanath2011}.
Of these distributions, the most popular one is the shape context
\cite{Mori2005} and its many variations such as the aspect space
representation \cite{Ling2010} and partial shape contexts \cite{Wang2012}.

The PDE based models include free vibration models \cite{Pentland1991},
Poisson models \cite{Gorelick2006} and gradient vector flow models
\cite{Hassouna2009} that attempt to transform one curve into another
by treating the points as particles and moving such particles in a
pre-defined medium. In addition, Radon and wavelet transforms \cite{Yao2009}
as well as Hurwitz-Radon transforms \cite{Jakobczak2010} attempt
to represent the curve in a multi-scale space where Euclidean transformations
have a structured effect (such as a shift in the coordinates). 

The deformable models for curve matching Metamorphs \cite{Huang2008},
elastic matching \cite{Srivastava2009} and Bezier curves \cite{Srivastava2010}
are among many methods to calculate a cost of deformation from one
curve to a second. In general, the method involves finding transform
parameters \cite{Tu2008} and then calculating a distance on the final
fit such as the Frechet distance \cite{Buchin2009}. However, these
methods do not accurately capture both the local and the global features
of a curve \cite{Basri1998}. 

In case of 2D surface analysis, Laplace-Beltrami eigenfunctions and
associated GPS coordinates have been used along with Morse-Smale complexes
\cite{Reuter2010}, Gromov-Hausdorff distances \cite{Bronstein2010}
and distributions of diffusion \cite{Bronstein2011}. The drawback
of these methods is that the Laplace-Beltrami operator captures only
the intrinsic geometry of the cortical surface. However the cortex
is known to have very little Gaussian curvature and most of the information
about sulcal patterning is in the mean curvature which is extrinsic
to the shape limiting the utility of methods involving only the intrinsic
geometry. 

In contrast to these methods, we present a model for shape analysis
of 1D curves that allow the direct analysis of the sulcal pattern
of the cortical surface. First, we briefly present our method for
automatically labeling the cortical surface and generating sulci on
a population of cortical surfaces.


\section{Sulci Generation \label{sec:Sulcal-Curves-Generation}}

This section briefly reviews a method used for automatic generation
of sulci on a cortical surface. Please refer to \cite{ajoshiISBI12}
for detailed description of the methods described. We assume as input,
a triangulated mesh representing the cortical surface. Here we briefly
describe our automatic surface registration method as the main focus
of this paper is shape analysis. We use BrainSuite software to first
extract the tessellated cortical surface for the atlas and for each
subject from T1-weighted MRI volumes and then to automatically identify
sulcal landmarks on the cortex. The method sets up one to one correspondence
between the atlas surface and the subject surface in two stages: (i)
for each subject, the surface of each cortical hemisphere is parameterized
to a unit square, and (ii) a vector field with respect to this parameterization
that aligns curvature of the surfaces is found. In order to generate
such a parameterization, we model the cortical surface as an elastic
sheet and solve the associated linear elastic equilibrium equation
using the Finite Element Method (FEM) as described in \cite{ajoshitmi07}.
We constrain the corpus callosum to lie on the boundary of the unit
square mapped as a uniform speed curve. An elastic energy minimization
yields flat maps of the two cortical hemisphere surfaces to a plane
(Fig. \ref{Fig: surf reg}). A multiresolution representation of curvature
for the subject and atlas is calculated which is then aligned by minimizing
a cost function with elastic energy as a regularizing penalty. This
step performs reparameterization of the cortical hemisphere surfaces
and establishes a one to one point correspondence between subject
and atlas surfaces. BrainSuite software \cite{bs2} is used to extract
the tessellated cortical surface for the atlas and for each subject
from T1-weighter MRI volumes and to interactively trace $N=26$ candidate
sulci on the atlas brain according to the protocol described in \cite{dimitrios_nimg_sulci}.
By using the point correspondence established during the registration,
these sulci are transferred to the subject surface. The locations
of the transferred sulci are subsequently refined to better follow
the true sulcal fundi using geodesic curvature flow on the cortical
surface. This is done using a level set based formulation of this
flow on non-flat surfaces that represent the sulci as zero level sets.
The resulting PDE is discretized on a triangulated mesh using finite
elements. The result is a set of sulci on the cortical surface. 

\begin{figure}
\includegraphics[width=0.98\columnwidth]{surfreg}

\caption{(a) Surface registration and (b) sulci generation}
\label{Fig: surf reg}
\end{figure}



\section{GPS Coordinates Representation \label{sec:GPS-Coordinates-Representation}}

In this section we introduce the coordinate system for the computation
of the 1D shapes or curves. Spectral theory studies eigenspectrum
of the shapes and the corresponding question `Can we hear shape of
the drum?'. In this paper, we use it for the 1D case and propose a
point signature for the 1D curve. Motivated from spectral theory and
corresponding work on 2D surfaces \cite{Reuter2010,Reuter2006} ,
we model the 1D curves as inhomogeneous vibrating strings. The solutions
of their harmonics is given by 1D Helmholtz equation. 

We add anisotropy by curvature term. 

\begin{equation}
\begin{cases}
\ensuremath{\frac{\partial}{\partial s}\kappa(s)\frac{\partial}{\partial s}f(s)} & =\lambda f(s)\\
f(s)|_{\partial D} & =0
\end{cases}\label{eq:main_equation}
\end{equation}


Let the solution to the eigenvalue problem is the eigenspectrum defined
by $\Phi_{i}$ and the eigenvalues $\lambda_{i}$ where eigenspectrum
is arranged from least to highest power. We define the embedding manifold
in spectral domain by the map:

$\ensuremath{GPS(p)=\left(\frac{1}{\sqrt{\lambda_{1}}}\Phi_{1}(p),\frac{1}{\sqrt{\lambda_{2}}}\Phi_{2}(p),\frac{1}{\sqrt{\lambda_{3}}}\Phi_{3}(p),\text{\ldots}\right)}$

Thus each point of the curve is embedded into infinite dimensional
space. It can be noted that we cannot directly use the 1D Laplacian
for this purpose because 1D shapes do not have a non-trivial intrinsic
geometry. However, due to fundamental theorem of planar curves (Two
unit-speed plane curves which have the same curvature differ only
by a Euclidean motion) curvature uniquely defines the curve upto Euclidean
motion. It can be noted that the curve can be recovered from the embedding
by first recovering the curvature and then using the Frenet-Serre
formulas. For 3D curves, we need curvature and torsion. The embedding
defined below is based on curvature alone, as the curves analyzed
in this paper (sulci) did not have significant torsion and embedding
curvature alone simplified the analysis. Some of the important and
useful properties of this embedding are defined below.
\begin{enumerate}
\item The GPS coordinates are isometry invariant as they only depends on
the derivatives and curvature which in turn are known to be dependent
only on the shape.
\item Furthermore, scaling a 1D curve manifold by the factor a results in
curvature scaled by the factor a. Therefore, by normalizing the eigenvalues,
shape can be compared regardless of the object's scale (and position
as mentioned earlier). 
\item Changes of the curve's shape result in continuous changes of its spectrum.
This shows that the representation presented here is robust. 
\item As opposed to the 2D case, where isospectral 2 manifolds exist, in
1D case, the spectrum characterizes the 1D shape completely. This
is due to the fundamental theorem of plane curves which states that
the curve can be recovered completely based on local curvature.
\item It can be proven that, in the embedding space, the inner product is
given by Green's function due to the identity: $G(x_{1},x_{2})=\sum_{i}\frac{\Phi_{i}(x_{1})\Phi_{i}(x_{2})}{\lambda_{i}}$.
Due to this property, the GPS representation encodes both local and
global curvature and shape information into the embedding. Additionally,
it is noted that in this infinite dimensional shape space, the metric
is euclidean, which simplifies the shape analysis and statistics in
this space. 
\end{enumerate}
We note that in practice there are two problems while working with
eigenvalues and eigenvectors in general \cite{jain2006robust}: the
signs of eigenvectors are undefined, and two eigenvectors may be swapped.
The sign issue is solved in this case by considering all the possible
signs of the eigenvectors when the distance minimization is performed.
The swapping of eigenvectors can occur when there is a symmetry in
shapes. This issue however does not occur in our case because we impose
additional constraint of matching the endpoints of the sulci. Further
analysis is needed to clarify the consequences of these factors for
shape processing when the GPS embedding is used. 

\begin{figure}
\includegraphics[width=0.98\textwidth]{GPS}\caption{(a) Inferior frontal sulcus highlighted in red; (b) first four color
coded GPS coordinates; (c) GPS representation plotted from end to
end of a sulcus}
\end{figure}



\section{Discretization using Finite Element Method \label{sec:Discretization-using-Finite}}

The 1-D curves are often non-uniformly sampled. Therefore we use finite
element method for discretization of the eigenvalue problem in Eq.
\ref{eq:main_equation}. We use linear elements. For a parameterized
curve represented sequentially as a set of points from $1$ to $N$,
a piecewise linear function of the curve can be represented using
linear elements, and therefore a function $f$ and an arbitrary `test
function' $\eta$ is defined as linear sums: $f(s)=\sum_{i\in[1,...,N]}f_{i}e_{i}(s)$,
$\eta(s)=\sum_{i\in[1,...,N]}\eta_{i}e_{i}(s)$. If $f$ is an eigenfunction
then the eigenvalue problem can be stated as:

\begin{eqnarray*}
Lf & = & \lambda f\\
\implies\int(\nabla\kappa(s)\nabla f(s))\eta(s)ds & = & \lambda\int f(s)\eta(s)ds\\
\implies\int\kappa(s)\nabla f(s)\nabla\eta(s)ds & = & \lambda\int f(s)\eta(s)ds\,\,\text{due to integration by parts}
\end{eqnarray*}


substituting the linear sums, we get:

\begin{eqnarray}
\sum_{i}\sum_{j}f_{i}\eta_{j}\kappa_{ij}\int\nabla e_{i}(s)\nabla e_{j}(s)ds & = & \lambda\sum_{i}\sum_{j}f_{i}\eta_{j}\int e_{i}(s)e_{j}(s)ds\nonumber \\
\kappa Sf & = & \lambda Mf\label{eq:EigVal_Matrix_Eq}
\end{eqnarray}


where $\kappa_{ij}$ represents $(\kappa_{i}+\kappa_{j})/2$ which
is the average of curvatures calculated at points $i$ and $j$. 

For 1D case and for linear elements, the element-wise load matrix
matrix $M_{el}=\left[\begin{array}{cc}
(\kappa_{ij}d_{ij})/3 & (\kappa_{ij}d_{ij})/6\\
(\kappa_{ij}d_{ij})/6 & (\kappa_{ij}d_{ij})/3
\end{array}\right]$ when $i\neq j$ and there is an edge between vertex $i$ and $j$
with length $d_{ij}$; $M_{ij}=\kappa_{ij}/3$ when $i=j$. Similarly
$S_{el}=\left[\begin{array}{cc}
1/d_{ij} & -1/d_{ij}\\
-1/d_{ij} & 1/d_{ij}
\end{array}\right]$. We exclude the derivation here but it can easily be verified. 

The matrix equation in Eq. \ref{eq:EigVal_Matrix_Eq} is a generalized
sparse eigenvalue problem which can easily be solved using standard
methods such as QZ method that is a part of Matlab function $\mathtt{eigs}$.
The point-wise curvature of the curve $\kappa_{i}$ is computed by
using the Frenet frame \cite{docarmo}.


\section{Shape Matching \label{sec:Shape-Matching}}

In this section, we describe a method for finding point correspondence
between two curves in 3D. In brain image analysis, sulci are often
represented as curves and so such a matching technique is required
for matching sulci across a subject population. In this paper, we
apply it for matching left vs right hemispherical sulci for mapping
shape asymmetry in two brains.

Let $GPS_{1}$ and $GPS_{2}$ denote the GPS coordinates for the two
sulcal sets. Our goal is to find a reparameterization function $\phi$
such that the matching energy $E(\phi)$ is minimized. 

\begin{equation}
\ensuremath{E(\phi)=\int||(GPS_{1}(s)-GPS_{2}(s+\phi(s))||^{2}ds}\label{eq:GPS}
\end{equation}


where $\phi$ is represented in terms of b-spline basis function.
The minimization of the cost function results in a 1-1 point correspondence
between the two curves. Once the optimal $\phi$ is found, the local
shape difference at point $s$ is given by $||(GPS_{1}(s)-GPS_{2}(s+\phi(s))||^{2}+||(GPS_{2}(s)-GPS_{1}(s+\phi(s))||^{2}$.

\begin{figure}
\includegraphics[width=0.98\textwidth]{shape_corr}\caption{Top: Three representative sulci from left and right hemispheres and
the point correspondence between them shown in colored lines.}
\end{figure}



\subsection{Application to mapping sulcal shape symmetry \label{sub:Application-to-mapping}}

In this section, we apply the curves matching framework described
above to mapping sulcal shape symmetry. The advantage of this application
is that we know that there is a certain degree of symmetry between
shapes of the sulci between the two cortical hemispheres but also
a high degree of asymmetry, however, the method applied is blind to
the fact that the two input sulci are from the same subject. 

For the purpose of mapping symmetry, we compute (a) point-wise GPS
distance between corresponding sulci from one hemisphere to the other,
for all subjects (b) point-wise GPS distance between corresponding
sulci for one hemisphere from a subject and from the other hemisphere
from a different subject. This process is repeated for all the subjects.

We define a measure of symmetry $Symm=-log(\frac{mean(a)}{mean(b)})$.
The measure $Symm$ ranges from $0$ to $\infty$. Additionally, we
also performed a non-parametric Mann\textendash{}Whitney\textendash{}Wilcoxon
test between statistics (a) and (b) at $\alpha=0.05$. To correct
for the multiple comparisons, false discovery rate (FDR) was applied
to adjust the $\alpha$ value. Fig. \ref{Fig 4: Symmetry} shows the
thresholded maps of $Symm$. 


\section{Results \label{sec:Results}}

We used two cohorts of twelve subjects each totaling 24 brains for
the purpose of symmetry detection. This cohort was first used in \cite{dimitrios_nimg_sulci}.
The first cohort of 12 brains was scanned at the Dornsife Cognitive
Neuroscience Imaging Center at the University of Southern California
using a 3T Siemens MAGNETOM Trio scanner. High-resolution T1-weighted
anatomical volumes were acquired for each subject with an MPRAGE scan
using the following protocol: TR = 2350 ms, TE = 4.13 ms, 192 slices,
field-of-view = 256 mm, voxel size = 1.0 \texttimes{} 1.0 \texttimes{}
1.0 mm. The second cohort was scanned at the University of Iowa using
thin-cut MR coronal images obtained in a General Electric Signa Scanner
operating at 1.5 Tesla, using the following protocol: SPGR/50, TR
24, TE 7, NEX 1 Matrix 256 \texttimes{} 192, FOV 24 cm, which yielded
124 contiguous coronal slices, 1.5 or 1.6 mm thick, with an interpixel
distance of 0.94 mm. Three data sets were obtained for each subject.
These were co-registered and averaged post-hoc using Automated Image
Registration (AIR 3.03, Woods et al. (1998)). The final data for each
subject had anisotropic voxels with an interpixel spacing of 0.7 mm
and interslice spacing of 1.5\textendash{}1.6 mm.

We executed the BrainSuite surface extraction pipeline followed by
sulcal set generation as outlined in Sec. \ref{sec:Sulcal-Curves-Generation}.
This generated 24x2 cortical surface representations with 26 sulci
each. The sulci were denoised by fitting a 12th order polynomial into
the curves. The degree of the polynomial was selected using L-curve
analysis of curve fitting for each curves and selecting a maximum
degree necessary for all the 26 curves. Next, the GPS coordinate representation
was generated for all the curves as described in Sec. \ref{sec:GPS-Coordinates-Representation}
and Sec. \ref{sec:Discretization-using-Finite}. The symmetry between
left and right sulci was then estimated by using the method in Sec
\ref{sub:Application-to-mapping}. The results of the symmetry mapping
are shown in Fig. \ref{Fig 4: Symmetry}. It is interesting to note
that the post- and pre-central sulci, together with the posterior
segment of the superior temporal, the transverse temporal, the middle
temporal and the inferior occipital sulci in the dorso-lateral view
show the maximal amount of asymmetry; on the mesial view the collateral,
the supraorbital, the occipito-parietal and long stretches of the
cingulate sulci also are extremely asymmetric; it is not surprising
to see the cingulate sulcus (visible in the depth of the mesial view
to show a great extend of relative symmetry. 

\begin{figure}
\includegraphics[width=0.95\linewidth]{fig1}\caption{Shape symmetry measure of the sulci plotted on a smooth representation
of an individual cortical surface. The black regions on the curves
indicate that a significant symmetry was not found for those points. }
\label{Fig 4: Symmetry}
\end{figure}



\section{Discussion and Conclusion}

We present an invariant shape represetation framework using eigensystem
of anisotropic Helmholtz equation and discus its properties. The shapes
are represented in terms of eigensystem of the equation. This representation
also has an interesting physical interpretation in terms of vibrating
strings. The resulting infinite dimensional shape space has a euclidean
metric simplifying the analysis of these embeddings. The model is
extended to shape registration framework which leads to a definition
of quantitative local shape differences. This framework is then applied
to the quantification of the sulcal shape analysis and symmetry detection. 

One potential drawback of the method is that errors in automatically
generated sulci can lead to inaccurate input of curves. We are in
the process of validating the sulcal generation method in a more extensive
manner on a larger data-set; initial validation is promising. It is
important to note that, if required, the BrainSuite software allows
for semi-automatic interactive corrections of the sulci providing
a way to correct inaccuracies.

This model has a variety of potential applications in computer vision
as well as brain image analysis. The majority of the existing methods
for brain morphometry focus on point-wise features such as 3D location,
curvature, thickness, deformation and image intensity. The presented
framework, on the other hand, directly captures the geometrical shape
of the folding patterning. By using this method, we can quantitatively
study the cortical folding pattern and therefore use it for quantitative
analysis of brain shapes in a variety of neuro-developmental conditions
(e.g. autism) and in other neurological conditions characterized by
changes in sulcal patterns. 

\bibliographystyle{splncs03}
\bibliography{ajoshibib}

\end{document}
