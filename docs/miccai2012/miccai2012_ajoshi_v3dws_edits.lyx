#LyX 2.0 created this file. For more info see http://www.lyx.org/
\lyxformat 413
\begin_document
\begin_header
\textclass llncs
\begin_preamble
\usepackage{babel}
\end_preamble
\use_default_options false
\maintain_unincluded_children false
\language english
\language_package default
\inputencoding latin9
\fontencoding T1
\font_roman lmodern
\font_sans default
\font_typewriter lmodern
\font_default_family default
\use_non_tex_fonts false
\font_sc false
\font_osf false
\font_sf_scale 100
\font_tt_scale 100

\graphics default
\default_output_format default
\output_sync 0
\bibtex_command default
\index_command default
\paperfontsize default
\spacing single
\use_hyperref false
\papersize default
\use_geometry false
\use_amsmath 2
\use_esint 2
\use_mhchem 0
\use_mathdots 0
\cite_engine basic
\use_bibtopic false
\use_indices false
\paperorientation portrait
\suppress_date false
\use_refstyle 0
\index Index
\shortcut idx
\color #008000
\end_index
\secnumdepth 3
\tocdepth 3
\paragraph_separation indent
\paragraph_indentation default
\quotes_language english
\papercolumns 1
\papersides 1
\paperpagestyle default
\tracking_changes false
\output_changes false
\html_math_output 0
\html_css_as_file 0
\html_be_strict false
\end_header

\begin_body

\begin_layout Title
An Invariant Shape Representation using the Anisotropic Helmholtz Equation
\end_layout

\begin_layout Author
*****
\end_layout

\begin_layout Running LaTeX Title
An Invariant Shape representation using anisotropic Helmholtz equation
\end_layout

\begin_layout Institute
* 
\begin_inset Newline newline
\end_inset

 * 
\begin_inset Newline newline
\end_inset

 *
\end_layout

\begin_layout Abstract
We present a novel shape representation for curves that characterizes the
 shape of a curve in terms of a coordinate system based on the eigensystem
 of the anisotropic Helmholtz equation.
 The method exploits the isometry invariance of eigenfunctions and introduces
 an invariant metric that quantifies shape differences.
 This representation has desirable properties, including stability, uniqueness
 and invariance to scaling and isometric transformation.
 Additionally, the resulting shape space has a Euclidean metric, thus simplifyin
g shape analysis.
 We employ this metric as part of a registration method that finds bijective
 and smooth point correspondences between shapes and defines a point-wise
 shape distance.
 When the curves are sampled irregularly, this approach also provides a
 fast and accurate computational method for solving the eigensystem using
 a finite element formulation.
 This shape representation is used to find symmetries between the sulcal
 shapes of the cortex.
 We demonstrate that this representation is able to quantify the shapes
 in an accurate and invariant manner.
 
\end_layout

\begin_layout Section*
Introduction
\end_layout

\begin_layout Standard
The human cerebral cortex is a highly convoluted sheet with rich and detailed
 folding patterns represented by sulci.
 Sulci are fissures in the cortical surface which are used frequently as
 anatomical landmarks.
 These sulcal landmarks have been extensively used for cortical registration
 and analysis of the geometry of these patterns can also be used to study
 disease progression 
\begin_inset CommandInset citation
LatexCommand cite
key "Narr01"

\end_inset

, aging 
\begin_inset CommandInset citation
LatexCommand cite
key "prince_aging"

\end_inset

 and brain asymmetry 
\begin_inset CommandInset citation
LatexCommand cite
key "Blanton01"

\end_inset

.
 However, these approaches do not reflect the shapes of the sulci, but focus
 instead on features such as length, depth and 3D location.
\end_layout

\begin_layout Standard
Quantification, matching, and classification of the shape of curves is a
 challenging problem with a long history.
 Spectral graphs 
\begin_inset CommandInset citation
LatexCommand cite
key "Carcassoni2003"

\end_inset

 use graph theory to attempt to match two curves.
 In addition, geometric features such as areas of enclosed regions 
\begin_inset CommandInset citation
LatexCommand cite
key "Xu2009"

\end_inset

 have been used for curve representation.
 Recent methods use the distributions of distances from all points on a
 curve to a reference point; the most popular is the shape context 
\begin_inset CommandInset citation
LatexCommand cite
key "Mori2005"

\end_inset

 and its variations 
\begin_inset CommandInset citation
LatexCommand cite
key "Wang2012"

\end_inset

.
\end_layout

\begin_layout Standard
PDE based models 
\begin_inset CommandInset citation
LatexCommand cite
key "Gorelick2006,Hassouna2009"

\end_inset

 attempt to transform one curve into another by treating the points as particles
 and moving such particles in a pre-defined medium.
 Metamorphs 
\begin_inset CommandInset citation
LatexCommand cite
key "Huang2008"

\end_inset

 and elastic matching 
\begin_inset CommandInset citation
LatexCommand cite
key "Srivastava2011"

\end_inset

 are among other methods to calculate a cost of deformation from one curve
 to a second.
 In general, these methods involves finding transform parameters and then
 calculating a distance on the final fit 
\begin_inset CommandInset citation
LatexCommand cite
key "Buchin2009"

\end_inset

.
 However, it is unclear whether they capture both the local and the global
 features of a curve 
\begin_inset CommandInset citation
LatexCommand cite
key "Basri1998"

\end_inset

.
\end_layout

\begin_layout Standard
In order to address these issues, we present a model for shape analysis
 of 1D curves based on an extension of the Global Point Signature (GPS)
 
\begin_inset CommandInset citation
LatexCommand cite
key "Reuter2010,Rustamov2007"

\end_inset

 that allows the direct analysis of sulcal patterns on the cortical surface.
 The GPS representation for 2D surfaces uses the eigensystem of the Laplace-Belt
rami operator.
 We first review our method for labeling the cortical surface automatically
 and for generating sulci on a population of cortical surfaces.
 We next present an invariant 1D curve representation and a method for curve
 matching.
 Finally, we present the results of applying our methodology to map sulcal
 shape symmetry between left and right brain hemispheres.
\end_layout

\begin_layout Section
Sulci Generation 
\begin_inset CommandInset label
LatexCommand label
name "sec:Sulcal-Curves-Generation"

\end_inset


\end_layout

\begin_layout Standard
We briefly review our method for automatic generation of sulci on a cortical
 surface, which are described in more detail in 
\begin_inset CommandInset citation
LatexCommand cite
key "ajoshiISBI12"

\end_inset

 .
 We assume as input a triangulated mesh that represents the cortical surface.
 We use the BrainSuite software 
\begin_inset CommandInset citation
LatexCommand cite
key "bs2"

\end_inset

 to extract the cortical surface meshes from T1-weighted MRI volumes for
 the atlas and for each subject.
 We then identify sulcal landmarks on the cortex automatically.
 We compute a one-to-one correspondence between the atlas surface and the
 subject surface in two stages: (i) for each subject, the surface of each
 cortical hemisphere is parameterized to a unit square, and (ii) a vector
 field is found with respect to this parameterization that aligns curvature
 of the surfaces.
 In order to generate such a parameterization, we model the cortical surface
 as an elastic sheet and solve the associated linear elastic equilibrium
 equation using finite elements.
 We constrain the corpus callosum to lie on the boundary of the unit square
 mapped as a uniform speed curve.
 The elastic energy minimization yields flat maps of the two cortical hemisphere
 surfaces to a plane (Fig.
 
\begin_inset CommandInset ref
LatexCommand ref
reference "Fig: surf reg"

\end_inset

).
 Multiresolution representations of curvature for the subject and atlas
 are calculated and then aligned by minimizing a cost function with elastic
 energy as a regularizing penalty.
 This step performs reparameterization of the cortical hemisphere surfaces
 and establishes a one to one point correspondence between subject and atlas
 surfaces.
\end_layout

\begin_layout Standard
For this study we performed this procedure for 
\begin_inset Formula $N=24$
\end_inset

 T1-weighted MRI volumes.
 A set of 26 sulcal curves per hemisphere on the atlas to which we registered
 were traced interactively in BrainSuite 
\begin_inset CommandInset citation
LatexCommand cite
key "bs2"

\end_inset

 using the protocol described in 
\begin_inset CommandInset citation
LatexCommand cite
key "dimitrios_nimg_sulci"

\end_inset

.
 By using the point correspondence established with the registration, these
 sulci are transferred to the subject surface.
 The locations of the transferred sulci are subsequently refined to better
 follow the true sulcal fundi using geodesic curvature flow on the cortical
 surface as described in 
\begin_inset CommandInset citation
LatexCommand cite
key "ajoshiISBI12"

\end_inset

.
 This is done using a level set based formulation of flow on non-flat surfaces
 with the sulci as the zero level sets.
 The resulting PDE is discretized on a triangulated mesh using finite elements.
 The final result is a set of sulci on each of the 
\begin_inset Formula $N=24$
\end_inset

 cortical surfaces.
\end_layout

\begin_layout Standard
\begin_inset Float figure
wide false
sideways false
status open

\begin_layout Plain Layout
\begin_inset Graphics
	filename surfreg.eps
	width 98col%

\end_inset


\end_layout

\begin_layout Plain Layout
\begin_inset Caption

\begin_layout Plain Layout
(a) Automatic atlas to subject registration and parameterization of cortical
 surfaces and sulcal curves; and (b) geodesic curvature flow refinement
 of sulcal curves
\end_layout

\end_inset


\end_layout

\begin_layout Plain Layout
\begin_inset CommandInset label
LatexCommand label
name "Fig: surf reg"

\end_inset

 
\end_layout

\end_inset


\end_layout

\begin_layout Section
Shape representation using GPS representation 
\begin_inset CommandInset label
LatexCommand label
name "sec:GPS-Coordinates-Representation"

\end_inset


\end_layout

\begin_layout Standard
In this section we introduce the coordinate system for the computation of
 the 1D shapes or curves.
 Spectral theory provides the basis to study the eigenspectrum of the sulcal
 curves.
 In this paper, we consider the 1D case and propose a point-wise signature
 for the 1D curve.
 Motivated by spectral theory and corresponding work on 2D surfaces 
\begin_inset CommandInset citation
LatexCommand cite
key "Rustamov2007,Reuter2010"

\end_inset

, we model the 1D curves as inhomogeneous vibrating strings.
 Their harmonic behavior is governed by the 1D Helmholtz equation.
 Since our aim is to characterize the shape of the curves 
\begin_inset Formula $C$
\end_inset

, we use curvature 
\begin_inset Formula $\kappa(s)$
\end_inset

 to introduce an anisotropy into the equation:
\end_layout

\begin_layout Standard
\begin_inset Formula 
\begin{equation}
\begin{cases}
\ensuremath{\frac{\partial}{\partial s}\kappa(s)\frac{\partial}{\partial s}\Phi_{i}(s)} & =\lambda_{i}\Phi_{i}(s)\\
\Phi_{i}(s)|_{\partial C} & =0
\end{cases},\forall s\in C\label{eq:main_equation}
\end{equation}

\end_inset

 where 
\begin_inset Formula $\partial C$
\end_inset

 is the set of the endpoints of the curve 
\begin_inset Formula $C$
\end_inset

.
 Denote the of eigenfunctions of this equation by 
\begin_inset Formula $\Phi_{i}$
\end_inset

 and the eigenvalues 
\begin_inset Formula $\lambda_{i}$
\end_inset

 ordered by magnitude.
 We define the embedding manifold in the spectral domain by the map:
\end_layout

\begin_layout Standard
\begin_inset Formula $\ensuremath{GPS(p)=\left(\frac{1}{\sqrt{\lambda_{1}}}\Phi_{1}(p),\frac{1}{\sqrt{\lambda_{2}}}\Phi_{2}(p),\frac{1}{\sqrt{\lambda_{3}}}\Phi_{3}(p),\text{\ensuremath{\ldots}}\right)}.$
\end_inset


\end_layout

\begin_layout Standard
Thus, each point of the curve is embedded into an infinite dimensional space.
 It should be noted that we cannot use the 1D Laplacian directly for this
 purpose because 1D shapes do not have a non-trivial intrinsic geometry.
 However, due to the fundamental theorem of curves (two unit-speed plane
 curves which have the same curvature and torsion differ only by a rigid
 transformation), curvature and torsion define the curve uniquely up to
 rigid transformation.
 Furthermore, the curve can be recovered from the embedding by first recovering
 the curvature and torsion, and then using the Frenet-Serre formulas 
\begin_inset CommandInset citation
LatexCommand cite
key "docarmo"

\end_inset

.
 For 3D curves, this requires curvature and torsion.
 However, the embedding defined above is based on curvature alone, as the
 sulcal curves analyzed in this paper had negligible torsion.
 Some of the important and useful properties of this embedding are defined
 below.
 
\end_layout

\begin_layout Enumerate
The GPS coordinates are isometry invariant as they depend only on the derivative
s and curvature, which in turn are known to be dependent only on the shape.
 
\end_layout

\begin_layout Enumerate
Scaling a 1D curve manifold by the factor 
\begin_inset Formula $\alpha$
\end_inset

 results in curvature scaled by the factor 
\begin_inset Formula $\alpha$
\end_inset

.
 Therefore, by normalizing the eigenvalues, shape can be compared regardless
 of the object's scale (and position as mentioned earlier).
 
\end_layout

\begin_layout Enumerate
Changes of the curve's shape result in continuous changes in its spectrum.
 Consequently the representation presented here is robust.
 
\end_layout

\begin_layout Enumerate
It can be proven that, in the embedding space, the inner product is given
 by the Green's function due to the identity: 
\begin_inset Formula $G(x_{1},x_{2})=\sum_{i}\frac{\Phi_{i}(x_{1})\Phi_{i}(x_{2})}{\lambda_{i}}$
\end_inset

.
 As a result, the GPS representation encodes both local and global curvature
 and shape information into the embedding.
 Additionally, in this infinite dimensional shape space, the metric is euclidean
, which simplifies analysis.
 
\end_layout

\begin_layout Standard
\align center
\begin_inset Float figure
wide false
sideways false
status open

\begin_layout Plain Layout
\align center
\begin_inset Graphics
	filename GPS.eps
	width 98text%

\end_inset


\begin_inset Caption

\begin_layout Plain Layout
(a) Inferior frontal sulcus highlighted in red; (b) first four color coded
 GPS coordinates; (c) GPS representation plotted from end to end of a sulcus
\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Section
Discretization using Finite Element Method 
\begin_inset CommandInset label
LatexCommand label
name "sec:Discretization-using-Finite"

\end_inset


\end_layout

\begin_layout Standard
The 1-D curves are often sampled non-uniformly.
 Therefore we use a finite element method for discretization of the eigenvalue
 problem in Eq.
 
\begin_inset CommandInset ref
LatexCommand ref
reference "eq:main_equation"

\end_inset

.
 Let 
\begin_inset Formula $\Phi(s)=\sum_{i}\phi_{i}e_{i}(s)$
\end_inset

 be an eigenfunction and 
\begin_inset Formula $\eta(s)=\sum_{i}\eta_{i}e_{i}(s)$
\end_inset

 be a `test function' represented as weighted sums of linear elements.
 The eigenvalue problem from Eq.
 
\begin_inset CommandInset ref
LatexCommand ref
reference "eq:main_equation"

\end_inset

 is then expressed as:
\end_layout

\begin_layout Standard
\begin_inset Formula 
\begin{eqnarray*}
\left(\frac{\partial}{\partial s}\kappa(s)\frac{\partial}{\partial s}\right)\Phi & = & \lambda\Phi\\
\implies\int\left(\frac{\partial}{\partial s}\kappa(s)\frac{\partial}{\partial s}\Phi(s)\right)\eta(s)ds & = & \lambda\int\Phi(s)\eta(s)ds\\
\implies\int\kappa(s)\frac{\partial}{\partial s}\Phi(s)\frac{\partial}{\partial s}\eta(s)ds & = & \lambda\int\Phi(s)\eta(s)ds
\end{eqnarray*}

\end_inset

 where the latter follows using integration by parts.
 Substituting the finite element model we get:
\end_layout

\begin_layout Standard
\begin_inset Formula 
\begin{eqnarray}
\sum_{i}\sum_{j}\phi_{i}\eta_{j}\kappa_{ij}\int\frac{\partial}{\partial s}e_{i}(s)\frac{\partial}{\partial s}e_{j}(s)ds & = & \lambda\sum_{i}\sum_{j}\phi_{i}\eta_{j}\int e_{i}(s)e_{j}(s)ds\nonumber \\
\kappa S\phi & = & \lambda M\phi\label{eq:EigVal_Matrix_Eq}
\end{eqnarray}

\end_inset

 where 
\begin_inset Formula $\kappa_{ij}$
\end_inset

 represents 
\begin_inset Formula $(\kappa_{i}+\kappa_{j})/2$
\end_inset

 which is the average of curvatures calculated at points 
\begin_inset Formula $i$
\end_inset

 and 
\begin_inset Formula $j$
\end_inset

.
\end_layout

\begin_layout Standard
For 1D case and for linear elements, the element-wise mass matrix is given
 by 
\begin_inset Formula $M_{el}=\left[\begin{array}{cc}
(\kappa_{ij}d_{ij})/3 & (\kappa_{ij}d_{ij})/6\\
(\kappa_{ij}d_{ij})/6 & (\kappa_{ij}d_{ij})/3
\end{array}\right]$
\end_inset

 for element 
\begin_inset Formula $el$
\end_inset

 corresponding to the edge between nodes 
\begin_inset Formula $i$
\end_inset

 and 
\begin_inset Formula $j$
\end_inset

.
 Similarly, elementwise stiffness matrix is given by 
\begin_inset Formula $S_{el}=\left[\begin{array}{cc}
1/d_{ij} & -1/d_{ij}\\
-1/d_{ij} & 1/d_{ij}
\end{array}\right]$
\end_inset

.
 We exclude the derivation here but it can easily be verified.
\end_layout

\begin_layout Standard
The matrix equation in Eq.
 
\begin_inset CommandInset ref
LatexCommand ref
reference "eq:EigVal_Matrix_Eq"

\end_inset

 is a generalized sparse eigenvalue problem that can be solved using standard
 methods, such as the QZ method that is a part of the Matlab function 
\begin_inset Formula $\mathtt{eigs}$
\end_inset

.
 The point-wise curvature of the curve 
\begin_inset Formula $\kappa_{i}$
\end_inset

 is computed using the Frenet frame 
\begin_inset CommandInset citation
LatexCommand cite
key "docarmo"

\end_inset

.
\end_layout

\begin_layout Section
Shape Matching 
\begin_inset CommandInset label
LatexCommand label
name "sec:Shape-Matching"

\end_inset


\end_layout

\begin_layout Standard
In this section, we describe a method for finding point correspondence between
 two curves in 3D.
 In brain image analysis, sulci are often represented as curves, thus a
 matching technique is required when analyzing sulcal variation across a
 population.
 In this paper, we match left vs right hemispherical sulci to investigate
 asymmetry between hemispheres.
\end_layout

\begin_layout Standard
Let 
\begin_inset Formula $GPS_{1}$
\end_inset

 and 
\begin_inset Formula $GPS_{2}$
\end_inset

 denote the GPS coordinates for the two sulcal sets.
 Our goal is to find a reparameterization function 
\begin_inset Formula $\psi$
\end_inset

 such that the matching energy 
\begin_inset Formula $E(\psi)$
\end_inset

 is minimized.
\end_layout

\begin_layout Standard
\begin_inset Formula 
\begin{equation}
\ensuremath{E(\psi)=\int||(GPS_{1}(s)-GPS_{2}(s+\psi(s))||^{2}ds}\label{eq:GPS}
\end{equation}

\end_inset

 where 
\begin_inset Formula $\phi$
\end_inset

 is represented in terms of b-spline basis function.
 Minimization of the cost function results in a 1-1 point correspondence
 between the two curves.
 Once the optimal 
\begin_inset Formula $\psi$
\end_inset

 is found, the local shape difference at point 
\begin_inset Formula $s$
\end_inset

 is given by 
\begin_inset Formula $||(GPS_{1}(s)-GPS_{2}(s+\psi(s))||$
\end_inset

.
\end_layout

\begin_layout Standard
\begin_inset Float figure
wide false
sideways false
status open

\begin_layout Plain Layout
\align center
\begin_inset Graphics
	filename shape_corr.eps
	width 75text%

\end_inset


\begin_inset Caption

\begin_layout Plain Layout
Top: Three representative sulci from left and right hemispheres and the
 point correspondence between them shown.
\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Standard
For the purpose of mapping symmetry, we compute (a) the point-wise GPS distance
 between corresponding sulci from one hemisphere to the other, for all subjects;
 (b) the point-wise GPS distance between corresponding sulci for the same
 hemisphere in two different subjects.
 We define a measure of symmetry 
\begin_inset Formula $Symm=-log(\frac{mean(a)}{mean(b)})$
\end_inset

.
 The measure 
\begin_inset Formula $Symm$
\end_inset

 ranges from 
\begin_inset Formula $0$
\end_inset

 to 
\begin_inset Formula $\infty$
\end_inset

.
 Additionally, we also performed a non-parametric Mann–Whitney–Wilcoxon
 test between statistics (a) and (b) at 
\begin_inset Formula $\alpha=0.05$
\end_inset

.
 To correct for multiple comparisons, false discovery rate (FDR) was applied
 to adjust the 
\begin_inset Formula $\alpha$
\end_inset

 value.
\end_layout

\begin_layout Section
Results 
\begin_inset CommandInset label
LatexCommand label
name "sec:Results"

\end_inset


\end_layout

\begin_layout Standard
We performed symmetry detection on data from 24 subjects, divided into two
 cohorts of 12 subjects.
 These data were used previously in 
\begin_inset CommandInset citation
LatexCommand cite
key "dimitrios_nimg_sulci"

\end_inset

.
 The first cohort was scanned at the Dornsife Cognitive Neuroscience Imaging
 Center at the University of Southern California using a 3T Siemens MAGNETOM
 Trio scanner.
 High-resolution T1-weighted anatomical volumes were acquired for each subject
 with an MPRAGE scan using the following protocol: TR = 2350 ms, TE = 4.13
 ms, 192 slices, field-of-view = 256 mm, voxel size = 1.0 × 1.0 × 1.0 mm.
 The second cohort was scanned at the University of Iowa using thin-cut
 MR coronal images obtained in a General Electric Signa Scanner operating
 at 1.5 Tesla, using the following protocol: SPGR/50, TR 24, TE 7, NEX 1
 Matrix 256 × 192, FOV 24 cm, which yielded 124 contiguous coronal slices,
 1.5 or 1.6 mm thick, with an interpixel distance of 0.94 mm.
 Three data sets were obtained for each subject.
 These were co-registered and averaged post-hoc using Automated Image Registrati
on (AIR 3.03, Woods et al.
 (1998)).
 The averaged image data for each subject had anisotropic voxels with an
 interpixel spacing of 0.7 mm and interslice spacing of 1.5–1.6 mm.
\end_layout

\begin_layout Standard
We applied the BrainSuite surface extraction sequence followed by sulcal
 set generation as outlined in Sec.
 
\begin_inset CommandInset ref
LatexCommand ref
reference "sec:Sulcal-Curves-Generation"

\end_inset

.
 This produced 24x2 cortical surface hemisphere representations with 26
 sulci each.
 The sulci were denoised by fitting a 12th order polynomial to the curves.
 The degree of the polynomial was selected using L-curve analysis of curve
 fitting for each curve and selecting the maximum degree necessary for all
 26 curves.
 Next, the GPS coordinate representation was generated for all curves as
 described in Sec.
 
\begin_inset CommandInset ref
LatexCommand ref
reference "sec:GPS-Coordinates-Representation"

\end_inset

 and Sec.
 
\begin_inset CommandInset ref
LatexCommand ref
reference "sec:Discretization-using-Finite"

\end_inset

.
 The symmetry between the sulci was then estimated using the method in Sec
 
\begin_inset CommandInset ref
LatexCommand ref
reference "sec:Shape-Matching"

\end_inset

.
 The results of the symmetry mapping are shown in Fig.
 
\begin_inset CommandInset ref
LatexCommand ref
reference "Fig 4: Symmetry"

\end_inset

.
 It is interesting to note that the post- and pre-central sulci, together
 with the posterior segment of the superior temporal, the transverse temporal,
 the middle temporal and the inferior occipital sulci in the dorso-lateral
 view, show the maximal amount of left/right asymmetry; on the mesial view
 the collateral, the supraorbital, the occipito-parietal and long stretches
 of the cingulate sulci are also extremely asymmetric.
 It is not surprising to see the cingulate sulcus (visible in the depth
 of the mesial view) to show a great extent of relative symmetry.
\end_layout

\begin_layout Standard
\begin_inset Float figure
wide false
sideways false
status open

\begin_layout Plain Layout
\begin_inset Graphics
	filename fig1.eps
	width 95line%

\end_inset


\begin_inset Caption

\begin_layout Plain Layout
Shape symmetry measure of the sulci plotted on a smooth representation of
 an individual cortical surface.
 The black regions on the curves indicate that a significant symmetry was
 not found for those points.
 
\end_layout

\end_inset


\end_layout

\begin_layout Plain Layout
\begin_inset CommandInset label
LatexCommand label
name "Fig 4: Symmetry"

\end_inset

 
\end_layout

\end_inset


\end_layout

\begin_layout Section
Discussion and Conclusion
\end_layout

\begin_layout Standard
We have presented a novel invariant shape representation framework that
 uses the eigensystem of the anisotropic Helmholtz equation.
 This representation also has an interesting physical interpretation in
 terms of vibrating strings.
 The resulting infinite dimensional shape space has a Euclidean metric,
 which simplifies the analysis of these embeddings.
 The model is extended to a shape registration framework, which leads to
 a definition of quantitative local shape differences.
 We applied this framework to the quantification of sulcal shape differences
 and symmetry detection.
\end_layout

\begin_layout Standard
One potential drawback of the method is that errors in automatically generated
 sulci can lead to inaccurate input when generating the GPS representation.
 We are in the process of validating the sulcal generation method in a more
 extensive manner on a larger data-set; initial validation is promising.
 It is important to note that, if required, the BrainSuite software allows
 for semi-automatic interactive corrections of the sulci, thereby providing
 a way to correct inaccuracies.
\end_layout

\begin_layout Standard
This model has a variety of potential applications in computer vision as
 well as brain image analysis.
 Many of the existing methods for brain morphometry focus on point-wise
 features such as 3D location, curvature, thickness, deformation, and image
 intensity.
 Conversely, the framework we have presented captures directly the geometric
 shape of the folding pattern.
 By using this method, we can study the cortical folding pattern quantitatively,
 and therefore use it for quantitative analysis of brain shapes in a variety
 of neuro-developmental conditions (e.g.
 autism) and in other neurological conditions characterized by changes in
 sulcal patterns.
\end_layout

\begin_layout Standard
 
\begin_inset CommandInset bibtex
LatexCommand bibtex
bibfiles "ajoshibib"
options "splncs03"

\end_inset


\end_layout

\end_body
\end_document
