%% LyX 2.0.2 created this file.  For more info, see http://www.lyx.org/.
%% Do not edit unless you really know what you are doing.
\documentclass[english]{llncs}
\usepackage{lmodern}
\renewcommand{\ttdefault}{lmodern}
\usepackage[T1]{fontenc}
\usepackage[latin9]{inputenc}
\usepackage{textcomp}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{graphicx}
\usepackage{esint}

\makeatletter
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% User specified LaTeX commands.
\usepackage{babel}

\makeatother

\usepackage{babel}
\begin{document}

\title{An Invariant Shape Representation using the Anisotropic Helmholtz
Equation}


\author{{*}{*}{*}{*}{*}}


\institute{{*} \\
 {*} \\
 {*}}
\maketitle
\begin{abstract}
We present a novel shape representation for curves that characterizes
the shape of a curve in terms of a coordinate system based on the
eigensystem of the anisotropic Helmholtz equation. The method exploits
the isometry invariance of eigenfunctions and introduces an invariant
metric that quantifies shape differences. This representation has
desirable properties, including stability, uniqueness and invariance
to scaling and isometric transformation. Additionally, the resulting
shape space has a Euclidean metric, thus simplifying shape analysis.
We employ this metric as part of a registration method that finds
bijective and smooth point correspondences between shapes and defines
a point-wise shape distance. When the curves are sampled irregularly,
this approach also provides a fast and accurate computational method
for solving the eigensystem using a finite element formulation. This
shape representation is used to find symmetries between the sulcal
shapes of the cortex. We demonstrate that this representation is able
to quantify the shapes in an accurate and invariant manner. 
\end{abstract}

\section*{Introduction}

The human cerebral cortex is a highly convoluted sheet with rich and
detailed folding patterns represented by sulci. Sulci are fissures
in the cortical surface which are used frequently as anatomical landmarks.
These sulcal landmarks have been extensively used for cortical registration
and analysis of the geometry of these patterns can also be used to
study disease progression \cite{Narr01}, aging \cite{prince_aging}
and brain asymmetry \cite{Blanton01}. However, these approaches do
not reflect the shapes of the sulci, but focus instead on features
such as length, depth and 3D location.

Quantification, matching, and classification of the shape of curves
is a challenging problem with a long history. Spectral graphs \cite{Carcassoni2003}
use graph theory to attempt to match two curves. In addition, geometric
features such as areas of enclosed regions \cite{Xu2009} have been
used for curve representation. Recent methods use the distributions
of distances from all points on a curve to a reference point; the
most popular is the shape context \cite{Mori2005} and its variations
\cite{Wang2012}.

PDE based models \cite{Gorelick2006,Hassouna2009} attempt to transform
one curve into another by treating the points as particles and moving
such particles in a pre-defined medium. Metamorphs \cite{Huang2008}
and elastic matching \cite{Srivastava2011} are among other methods
to calculate a cost of deformation from one curve to a second. In
general, these methods involves finding transform parameters and then
calculating a distance on the final fit \cite{Buchin2009}. However,
it is unclear whether they capture both the local and the global features
of a curve \cite{Basri1998}.

In order to address these issues, we present a model for shape analysis
of 1D curves based on an extension of the Global Point Signature (GPS)
\cite{Reuter2010,Rustamov2007} that allows the direct analysis of
sulcal patterns on the cortical surface. The GPS representation for
2D surfaces uses the eigensystem of the Laplace-Beltrami operator.
We first review our method for labeling the cortical surface automatically
and for generating sulci on a population of cortical surfaces. We
next present an invariant 1D curve representation and a method for
curve matching. Finally, we present the results of applying our methodology
to map sulcal shape symmetry between left and right brain hemispheres.


\section{Sulci Generation \label{sec:Sulcal-Curves-Generation}}

We briefly review our method for automatic generation of sulci on
a cortical surface, which are described in more detail in \cite{ajoshiISBI12}
. We assume as input a triangulated mesh that represents the cortical
surface. We use the BrainSuite software \cite{bs2} to extract the
cortical surface meshes from T1-weighted MRI volumes for the atlas
and for each subject. We then identify sulcal landmarks on the cortex
automatically. We compute a one-to-one correspondence between the
atlas surface and the subject surface in two stages: (i) for each
subject, the surface of each cortical hemisphere is parameterized
to a unit square, and (ii) a vector field is found with respect to
this parameterization that aligns curvature of the surfaces. In order
to generate such a parameterization, we model the cortical surface
as an elastic sheet and solve the associated linear elastic equilibrium
equation using finite elements. We constrain the corpus callosum to
lie on the boundary of the unit square mapped as a uniform speed curve.
The elastic energy minimization yields flat maps of the two cortical
hemisphere surfaces to a plane (Fig. \ref{Fig: surf reg}). Multiresolution
representations of curvature for the subject and atlas are calculated
and then aligned by minimizing a cost function with elastic energy
as a regularizing penalty. This step performs reparameterization of
the cortical hemisphere surfaces and establishes a one to one point
correspondence between subject and atlas surfaces.

For this study we performed this procedure for $N=24$ T1-weighted
MRI volumes. A set of 26 sulcal curves per hemisphere on the atlas
to which we registered were traced interactively in BrainSuite \cite{bs2}
using the protocol described in \cite{dimitrios_nimg_sulci}. By using
the point correspondence established with the registration, these
sulci are transferred to the subject surface. The locations of the
transferred sulci are subsequently refined to better follow the true
sulcal fundi using geodesic curvature flow on the cortical surface
as described in \cite{ajoshiISBI12}. This is done using a level set
based formulation of flow on non-flat surfaces with the sulci as the
zero level sets. The resulting PDE is discretized on a triangulated
mesh using finite elements. The final result is a set of sulci on
each of the $N=24$ cortical surfaces.

\begin{figure}
\includegraphics[width=0.98\columnwidth]{surfreg}

\caption{(a) Automatic atlas to subject registration and parameterization of
cortical surfaces and sulcal curves; and (b) geodesic curvature flow
refinement of sulcal curves}


\label{Fig: surf reg} 
\end{figure}



\section{Shape representation using GPS representation \label{sec:GPS-Coordinates-Representation}}

In this section we introduce the coordinate system for the computation
of the 1D shapes or curves. Spectral theory provides the basis to
study the eigenspectrum of the sulcal curves. In this paper, we consider
the 1D case and propose a point-wise signature for the 1D curve. Motivated
by spectral theory and corresponding work on 2D surfaces \cite{Rustamov2007,Reuter2010},
we model the 1D curves as inhomogeneous vibrating strings. Their harmonic
behavior is governed by the 1D Helmholtz equation. Since our aim is
to characterize the shape of the curves $C$, we use curvature $\kappa(s)$
to introduce an anisotropy into the equation:

\begin{equation}
\begin{cases}
\ensuremath{\frac{\partial}{\partial s}\kappa(s)\frac{\partial}{\partial s}\Phi_{i}(s)} & =\lambda_{i}\Phi_{i}(s)\\
\Phi_{i}(s)|_{\partial C} & =0
\end{cases},\forall s\in C\label{eq:main_equation}
\end{equation}
 where $\partial C$ is the set of the endpoints of the curve $C$.
Denote the of eigenfunctions of this equation by $\Phi_{i}$ and the
eigenvalues $\lambda_{i}$ ordered by magnitude. We define the embedding
manifold in the spectral domain by the map:

$\ensuremath{GPS(p)=\left(\frac{1}{\sqrt{\lambda_{1}}}\Phi_{1}(p),\frac{1}{\sqrt{\lambda_{2}}}\Phi_{2}(p),\frac{1}{\sqrt{\lambda_{3}}}\Phi_{3}(p),\text{\ensuremath{\ldots}}\right)}.$

Thus, each point of the curve is embedded into an infinite dimensional
space. It should be noted that we cannot use the 1D Laplacian directly
for this purpose because 1D shapes do not have a non-trivial intrinsic
geometry. However, due to the fundamental theorem of curves (two unit-speed
plane curves which have the same curvature and torsion differ only
by a rigid transformation), curvature and torsion define the curve
uniquely up to rigid transformation. Furthermore, the curve can be
recovered from the embedding by first recovering the curvature and
torsion, and then using the Frenet-Serre formulas \cite{docarmo}.
For 3D curves, this requires curvature and torsion. However, the embedding
defined above is based on curvature alone, as the sulcal curves analyzed
in this paper had negligible torsion. Some of the important and useful
properties of this embedding are defined below. 
\begin{enumerate}
\item The GPS coordinates are isometry invariant as they depend only on
the derivatives and curvature, which in turn are known to be dependent
only on the shape. 
\item Scaling a 1D curve manifold by the factor $\alpha$ results in curvature
scaled by the factor $\alpha$. Therefore, by normalizing the eigenvalues,
shape can be compared regardless of the object's scale (and position
as mentioned earlier). 
\item Changes of the curve's shape result in continuous changes in its spectrum.
Consequently the representation presented here is robust. 
\item It can be proven that, in the embedding space, the inner product is
given by the Green's function due to the identity: $G(x_{1},x_{2})=\sum_{i}\frac{\Phi_{i}(x_{1})\Phi_{i}(x_{2})}{\lambda_{i}}$.
As a result, the GPS representation encodes both local and global
curvature and shape information into the embedding. Additionally,
in this infinite dimensional shape space, the metric is euclidean,
which simplifies analysis. 
\begin{figure}
\centering{}\includegraphics[width=0.98\textwidth]{GPS}\caption{(a) Inferior frontal sulcus highlighted in red; (b) first four color
coded GPS coordinates; (c) GPS representation plotted from end to
end of a sulcus}
\end{figure}

\end{enumerate}

\section{Discretization using Finite Element Method \label{sec:Discretization-using-Finite}}

The 1-D curves are often sampled non-uniformly. Therefore we use a
finite element method for discretization of the eigenvalue problem
in Eq. \ref{eq:main_equation}. Let $\Phi(s)=\sum_{i}\phi_{i}e_{i}(s)$
be an eigenfunction and $\eta(s)=\sum_{i}\eta_{i}e_{i}(s)$ be a `test
function' represented as weighted sums of linear elements. The eigenvalue
problem from Eq. \ref{eq:main_equation} is then expressed as:

\begin{eqnarray*}
\left(\frac{\partial}{\partial s}\kappa(s)\frac{\partial}{\partial s}\right)\Phi & = & \lambda\Phi\\
\implies\int\left(\frac{\partial}{\partial s}\kappa(s)\frac{\partial}{\partial s}\Phi(s)\right)\eta(s)ds & = & \lambda\int\Phi(s)\eta(s)ds\\
\implies\int\kappa(s)\frac{\partial}{\partial s}\Phi(s)\frac{\partial}{\partial s}\eta(s)ds & = & \lambda\int\Phi(s)\eta(s)ds
\end{eqnarray*}
 where the latter follows using integration by parts. Substituting
the finite element model we get:

\begin{eqnarray}
\sum_{i}\sum_{j}\phi_{i}\eta_{j}\kappa_{ij}\int\frac{\partial}{\partial s}e_{i}(s)\frac{\partial}{\partial s}e_{j}(s)ds & = & \lambda\sum_{i}\sum_{j}\phi_{i}\eta_{j}\int e_{i}(s)e_{j}(s)ds\nonumber \\
\kappa S\phi & = & \lambda M\phi\label{eq:EigVal_Matrix_Eq}
\end{eqnarray}
 where $\kappa_{ij}$ represents $(\kappa_{i}+\kappa_{j})/2$ which
is the average of curvatures calculated at points $i$ and $j$.

For 1D case and for linear elements, the element-wise mass matrix
is given by $M_{el}=\left[\begin{array}{cc}
(\kappa_{ij}d_{ij})/3 & (\kappa_{ij}d_{ij})/6\\
(\kappa_{ij}d_{ij})/6 & (\kappa_{ij}d_{ij})/3
\end{array}\right]$ for element $el$ corresponding to the edge between nodes $i$ and
$j$. Similarly, elementwise stiffness matrix is given by $S_{el}=\left[\begin{array}{cc}
1/d_{ij} & -1/d_{ij}\\
-1/d_{ij} & 1/d_{ij}
\end{array}\right]$. We exclude the derivation here but it can easily be verified.

The matrix equation in Eq. \ref{eq:EigVal_Matrix_Eq} is a generalized
sparse eigenvalue problem that can be solved using standard methods,
such as the QZ method that is a part of the Matlab function $\mathtt{eigs}$.
The point-wise curvature of the curve $\kappa_{i}$ is computed using
the Frenet frame \cite{docarmo}.


\section{Shape Matching \label{sec:Shape-Matching}}

In this section, we describe a method for finding point correspondence
between two curves in 3D. In brain image analysis, sulci are often
represented as curves, thus a matching technique is required when
analyzing sulcal variation across a population. In this paper, we
match left vs right hemispherical sulci to investigate asymmetry between
hemispheres.

Let $GPS_{1}$ and $GPS_{2}$ denote the GPS coordinates for the two
sulcal sets. Our goal is to find a reparameterization function $\psi$
such that the matching energy $E(\psi)$ is minimized.

\begin{equation}
\ensuremath{E(\psi)=\int||(GPS_{1}(s)-GPS_{2}(s+\psi(s))||^{2}ds}\label{eq:GPS}
\end{equation}
 where $\phi$ is represented in terms of b-spline basis function.
Minimization of the cost function results in a 1-1 point correspondence
between the two curves. Once the optimal $\psi$ is found, the local
shape difference at point $s$ is given by $||(GPS_{1}(s)-GPS_{2}(s+\psi(s))||$.

\begin{figure}
\centering{}\includegraphics[width=0.65\textwidth]{shape_corr}\caption{Top: Three representative sulci from left and right hemispheres and
the point correspondence between them shown.}
\end{figure}


For the purpose of mapping symmetry, we compute (a) the point-wise
GPS distance between corresponding sulci from one hemisphere to the
other, for all subjects; (b) the point-wise GPS distance between corresponding
sulci for the same hemisphere in two different subjects. We define
a measure of symmetry $Symm=-log(\frac{mean(a)}{mean(b)})$. The measure
$Symm$ ranges from $0$ to $\infty$. Additionally, we also performed
a non-parametric Mann\textendash{}Whitney\textendash{}Wilcoxon test
between statistics (a) and (b) at $\alpha=0.05$. To correct for multiple
comparisons, false discovery rate (FDR) was applied to adjust the
$\alpha$ value.


\section{Results \label{sec:Results}}

We performed symmetry detection on data from 24 subjects, divided
into two cohorts of 12 subjects. These data were used previously in
\cite{dimitrios_nimg_sulci}. The first cohort was scanned at ANONYMIZED
using a 3T Siemens MAGNETOM Trio scanner. High-resolution T1-weighted
anatomical volumes were acquired for each subject with an MPRAGE scan
using the following protocol: TR = 2350 ms, TE = 4.13 ms, 192 slices,
field-of-view = 256 mm, voxel size = 1.0 \texttimes{} 1.0 \texttimes{}
1.0 mm. The second cohort was scanned at ANONYMIZED using thin-cut
MR coronal images obtained in a General Electric Signa Scanner operating
at 1.5 Tesla, using the following protocol: SPGR/50, TR 24, TE 7,
NEX 1 Matrix 256 \texttimes{} 192, FOV 24 cm, which yielded 124 contiguous
coronal slices, 1.5 or 1.6 mm thick, with an interpixel distance of
0.94 mm. Three data sets were obtained for each subject. These were
co-registered and averaged post-hoc using Automated Image Registration
(AIR 3.03, Woods et al. (1998)). The averaged image data for each
subject had anisotropic voxels with an interpixel spacing of 0.7 mm
and interslice spacing of 1.5\textendash{}1.6 mm.

We applied the BrainSuite surface extraction sequence followed by
sulcal set generation as outlined in Sec. \ref{sec:Sulcal-Curves-Generation}.
This produced 24x2 cortical surface hemisphere representations with
26 sulci each. The sulci were denoised by fitting a 12th order polynomial
to the curves. The degree of the polynomial was selected using L-curve
analysis of curve fitting for each curve and selecting the maximum
degree necessary for all 26 curves. Next, the GPS coordinate representation
was generated for all curves as described in Sec. \ref{sec:GPS-Coordinates-Representation}
and Sec. \ref{sec:Discretization-using-Finite}. The symmetry between
the sulci was then estimated using the method in Sec \ref{sec:Shape-Matching}.
The results of the symmetry mapping are shown in Fig. \ref{Fig 4: Symmetry}.
It is interesting to note that the post- and pre-central sulci, together
with the posterior segment of the superior temporal, the transverse
temporal, the middle temporal and the inferior occipital sulci in
the dorso-lateral view, show the maximal amount of left/right asymmetry;
on the mesial view the collateral, the supraorbital, the occipito-parietal
and long stretches of the cingulate sulci are also extremely asymmetric.
It is not surprising to see the cingulate sulcus (visible in the depth
of the mesial view) to show a great extent of relative symmetry. 
\begin{figure}
\includegraphics[width=0.95\linewidth]{fig1}\caption{Shape symmetry measure of the sulci plotted on a smooth representation
of an individual cortical surface. The black regions on the curves
indicate that a significant symmetry was not found for those points. }


\label{Fig 4: Symmetry} 
\end{figure}


\vspace{-1cm}


\section{Discussion and Conclusion}

We have presented a novel invariant shape representation framework
that uses the eigensystem of the anisotropic Helmholtz equation. This
representation also has an interesting physical interpretation in
terms of vibrating strings. The resulting infinite dimensional shape
space has a Euclidean metric, which simplifies the analysis of these
embeddings. The model is extended to a shape registration framework,
which leads to a definition of quantitative local shape differences.
We applied this framework to the quantification of sulcal shape differences
and symmetry detection.

One potential drawback of the method is that errors in automatically
generated sulci can lead to inaccurate input when generating the GPS
representation. We are in the process of validating the sulcal generation
method in a more extensive manner on a larger data-set; initial validation
is promising. It is important to note that, if required, the BrainSuite
software allows for semi-automatic interactive corrections of the
sulci, thereby providing a way to correct inaccuracies.

This model has a variety of potential applications in computer vision
as well as brain image analysis. Many of the existing methods for
brain morphometry focus on point-wise features such as 3D location,
curvature, thickness, deformation, and image intensity. Conversely,
the framework we have presented captures directly the geometric shape
of the folding pattern. By using this method, we can study the cortical
folding pattern quantitatively, and therefore use it for quantitative
analysis of brain shapes in a variety of neuro-developmental conditions
(e.g. autism) and in other neurological conditions characterized by
changes in sulcal patterns.

{\scriptsize \bibliographystyle{splncs03}
\bibliography{ajoshibib}
}
\end{document}
