\section{Notes}

%% Pentland 1991
%% Gorelick 2006
%% Yao 2009
%% Cui 2009
%% Hassouna 2009
\subsection{
	PDE:
	Pentland 1991
	\cite{Pentland1991}
	(model fit)
	,
	Gorelick 2006
	\cite{Gorelick2006}
	(Poisson)
	,
	Yao 2009
	\cite{Yao2009}
	(Radon/wavelet)
	,
	Cui 2009
	\cite{Cui2009}
	(cumulative integral of curvature)
	,
	Hassouna 2009
	\cite{Hassouna2009}
	(Gradient vector flow)
	,
	Jakobczak 2010
	\cite{Jakobczak2010}
	(Hurwitz-Radon)
}

Pentland: Model shape based on deformation to a known object. Recover shape from boundary edges.

Representation: PDE "free vibration model"
\begin{align}
\bK \bU
&=
\bR
\leftarrow
\text{ final shape}
\\
\bM \partial^2 \bU
+
\bC \partial \bU
+
\bK \bU
&=
\bR
\leftarrow
\text{ from edges to final shape}
\end{align}

Gorelick: Use Poisson equation to describe distance from each point in shape to boundary

Define curvature using this distance

Classify using normalized curvature

Yao: Radon or wavelet transform of thresholded image

Then use features (boundaries, etc.) to match

Cui: Represent contour as 2D function of integral of curvature from $s_1$ to $s_2$

Match cumulative integrals

Hassouna: Smooth estimation of vector field of shape to skeleton

Works given any skeleton

Jakobczak: Interplate curve using Hurwitz-Radon decoding

Other citations:
\begin{itemize}
\item Blum (8) for Blum's medial axis transform
\item Lin (37) for Fourier descriptors
\item Elad (20) for geometric shape moments
\item Zhang (56) for Zernicke moments
\item Kim (30) for angular radial transform
\item Zuliani (57) for Helmholtz PDE
\item Mokhtarian (40) for curvature scale space
\item Carlsson (14) for order structure
\item Sharon (48) for conformal mapping
\item Duci (17) for harmonic embedding
\item Klassen (31) for geodesic paths
\item Tari 1998 for shape skeleton as points of local symmetry
\end{itemize}

%% Cohen 1992
%% Huang 2008
%% Tu 2008
%% Buchin 2009
%% Srivastava 2009
%% Srivastava 2010
%% Chen 2011
\subsection{
	Curve deformations:
	Cohen 1992
	\cite{Cohen1992}
	(videos)
	,
	Huang 2008
	\cite{Huang2008}
	(Metamorphs)
	,
	Tu 2008
	\cite{Tu2008}
	(EM for shape matching)
	,
	Buchin 2009
	\cite{Buchin2009}
	(partial matching Frechet distance)
	,
	Srivastava 2009
	\cite{Srivastava2009}
	(curvilinear elastic matching)
	,
	Srivastava 2010
	\cite{Srivastava2010}
	(Bezier curves)
	,
	Chen 2011
	\cite{Chen 2011}
	NURBS
}

Cohen: Match curves in sequences of images by minimizing an energy function.

Curvature energy between curves P and Q:
$
E_{\text{curve}}
=
\frac{1}{2}
\int
\cir{
	\kappa_Q \cir{s' \cir{s}}
	-
	\kappa_P \cir{s}
}^2
ds
$

Regularization for smooth deformation:
$
E_{\text{regular}}
=
\int
\norm{
	\frac{
		\partial
		\cir{
			Q \cir{s' \cir{s}}
			-
			P \cir{s}
		}
	}{
		\partial s
	}
}^2
ds
$

Total energy:
$
E
=
E_{\text{curve}}
+
\lambda
E_{\text{regular}}
$
with $\lambda$ inversely proportional to curvature $\kappa_P$.

Huang: free-form deformation of model to shape using cubic B-spline interpolation

ML for fit of parameters of model to shape

Used to fit boundaries of closed regions

Tu: Fully Bayesian approach: priors on affine transform parameters and thin-plate-spline kernel

Buchin: Calculate Frechet for each pair of points, find shortest path.

Frechet distance:
\[
\sD \cir{P, Q}
=
\inf_{s,s'}
\norm{
	P \cir{s\cir{t}},
	Q \cir{s'\cir{t}},
}_p
\]

Srivastava: Darcyan coordinate system, distance between two points is shortest geodesic distance

Match level curves of distance while minimizing elastic energy

Srivastava: Deform Bezier curves to fit each line segment

Chen: Deform nonuniform rational B-spline curves into point cloud

Other citations:
\begin{itemize}
\item Chui (8) for mean field approximation to free energy
\item Pentecost 1999 and Lorenz 2006 for NURBS for heart
\item Alt 1995 for computation of distance
\item Efrat 2007 for cumulative sums of Frechet distances
\end{itemize}

%% Serra 1995
%% Latecki 2000
%% Sebastian 2003
\subsection{
	Local lines:
	Serra 1995
	\cite{Serra1995}
	(Dynamic prog)
	,
	Latecki 2000
	\cite{Latecki2000}
	(Similarity)
	,
	Sebastian 2003
	\cite{Sebastian2003}
	(Infinitesimal)
}

Serra: Solve least-squares deformation by approximating curve with line segments.

Representation: $\gamma: P \rightarrow Q$.

Cost function:
$
\sD \cir{\gamma}
=
\int_P
\norm{
	\frac{d}{dt}
	D \cir{\gamma, s}
}^2
ds
$
with
$
D \cir{\gamma, s}
=
$
coordinates of $Q$ at $\gamma \cir{s}$
$-$
coordinates of $P$ at $s$.

Approximate $\gamma$ as piecewise linear to get closed form for $\sD$ as sum based on parameters of each line segment.

Latecki: Calculate similarity as sum of similarities between parts.

Curve evolution: Join two line segments $s_1$ and $s_2$ if their turn angle is small
$
K \cir{s_1, s_2}
=
\angle \cir{s_1, s_2}
\frac{
	\str{s_1} \str{s_2}
}{
	\str{s_1} + \str{s_2}
}
\rightarrow
$
turn shape into low-size polygon.

Cost function: difference in tangent functions
$
S \cir{c,d}
=
\int
\norm{
	\frac{dP}{ds}
	-
	\frac{dQ}{ds'(s)}
}^2
ds
$

Sebastian: Break each curve into small line segments and align as a group.

For two small line segments $ds'$ and $ds$ with slopes $d\theta'$ and $d\theta$, cost is $\str{ds' - ds} + \str{d\theta' - d\theta}$

Integrated, cost
$
=
\int
\str{
	\frac{ds'}{ds}
	-
	1
}
+
\lambda
\str{
	\frac{d\theta'}{ds'}
	\frac{ds'}{ds}
	-
	\frac{d\theta}{ds}
}
ds
$
\note{not symmetric because no expansion of point to curve}

\textit{Alignment curve}: Parametrize $P$ and $Q$ by $h_P: [0,1] \rightarrow P$ and $h_Q: [0,1] \rightarrow Q$ which may have an interval of constancy (parametrization stops for some intervals of time).

Alignment curve is
$
\alpha \cir{s}
=
\cir{
	h_P \cir{s},
	h_Q \cir{s}
}
$
with
$\alpha \cir{0} = \cir{0,0}$
and
$\alpha \cir{1} = \cir{1,1}$.

Angle function $\psi$ is angle between tangent-to-$\alpha$ and x-axis, so
$h_P = \int \cos \psi$
and
$h_Q = \int \sin \psi$

Now, do same cost function on $h$ (which collapses all stops to a single point for differentiability):
\begin{equation}
\int_P
\str{
	\cos \psi
	-
	\sin \psi
}
+
\lambda
\str{
	\kappa \cir{h_P}
	\cos \psi
	-
	\kappa \cir{h_Q}
	\sin \psi
}
ds
\end{equation}
\note{known as "edit" distance}

%% Basri 1998
\subsection{
	Basri 1998
	\cite{Basri 1998}
	:
	Properties Required in Good Shape Similarity Methods
}

Goal: Explain the benefits/drawbacks of elastic matching as a way to copy human visual processing.

Cost function template:
$
\int_P
F
\cir{
	\kappa_P \cir{s},
	\kappa_Q \cir{s' \cir{s}},
	\frac{ds'}{ds}
}
ds
$
with goals
\begin{itemize}
\item continuity
\item metricity
\item invariance under Euclidean transformations
\item handles polygons
\item handles parts of objects changing while others don't
\item monotonicity
\item exponential cost: many small deformations cost less than one large deformation
\end{itemize}
and not all can be satisfied.

Smoothing is required because corners are hard to fix.

Spring equation between points in the parametrization:
\begin{align}
E_{\text{stretching}}
&=
\frac{\alpha}{p}
\int_{P}
\frac{
	\str{
		\frac{ds'}{ds}
		-
		1
	}^p
}{
	\cir{
		\frac{ds'}{ds}
		+
		1
	}^{p-1}
}
ds
\\
E_{\text{bending}}
&=
\frac{\alpha}{p}
\int_{P}
\frac{
	\str{
		\frac{ds'}{ds} d\theta_{s'}
		-
		d\theta_s
	}^p
}{
	\cir{
		\str{\frac{ds'}{ds} d\theta_{s'}}
		+
		\str{d\theta_s}
	}^{p-1}
}
ds
\\
E
&=
E_{\text{stretching}}
+
\lambda
E_{\text{bending}}
\end{align}

Other citations:
\begin{itemize}
\item Yoshida (56) compares differences in angles and distances between corresponding points
\item Hildreth (22) uses motion flow as cost
\item Ayache (12) uses curvature difference
\item Kass (31) uses model-based cost function (cost of known-model fit to curve)
\item Hoffman (25) breaks curve into parts based on concavity extrema
\item Koenderink (30) breaks curve into parts based on inflection points
\end{itemize}


%% Mori 2005
%% Daliri 2008
%% Ling 2010
%% Shu 2010
\subsection{
	Shape Contexts:
	Mori 2005
	\cite{Mori2005}
	,
	Daliri 2008
	\cite{Daliri2008}
	(point-to-point)
	,
	Ling 2010
	\cite{Ling2010}
	(aspect space)
	,
	Shu 2010
	\cite{Shu2010}
	(use centroid instead of 1 point)
	,
	Wang 2012
	\cite{Wang2012}
	(Clustering followed by part-matching)
}

Setup:
\begin{enumerate}
\item Find edge pixels $\set{p_1, \ldots, p_N}$
\item Find displacement vectors from $p_1$ and break into magnitude and direction:
\[
	\set{
		p_2 - p_1,
		\ldots,
		p_N - p_1
	}
	=
	\set{
		A_1 e^{j \theta_1},
		\ldots,
		A_{N-1} e^{j \theta_{N-1}}
	}
\]
\item Create 2D histogram $p_{\log A, \theta}$ of log-polar coordinates
\end{enumerate}

k-means clustering to find representatives for groups of images (shapemes)

Daliri: Procrustes method for point-to-point registration

Ling: Create aspect space (generalization) $
\sA
\cir{I, \alpha}
=
\cir{
	x,
	y,
	\alpha I \cir{x,y}
}
$
and then histogram to get feature space

Shu: Calculate shape context from centroid instead of point

Use earth mover's distance (amount of distortion in one image to get to 2nd image)

Wang: Cluster then match clusters instead of full contours

Shape contexts of clusters are more robust

Other citations:
\begin{itemize}
\item Zahn (14) for Fourier descriptors of curves
\item Sharvit (15) for skeletons using Blum's medial axis transform
\item Ling 2007 for inner-distance shape context
\item Rubner 2000 for "earth mover's distance"
\item Osada 2002 for shape descriptors, then Krim 2005 and Mahmoudi 2009 for other distances
\end{itemize}

%% Levy 2006
%% Reuter 2006
%% Rustamov 2007
%% Reuter 2009
%% Reuter 2009b
%% Gkebal 2009
%% Reuter 2010
\subsection{
	Laplace-Beltrami:
	Levy 2006
	\cite{Levy2006}
	,
	Reuter 2006
	\cite{Reuter2006}
	(Shape-DNA)
	,
	Rustamov 2007
	\cite{Rustamov2007}
	(The GPS embedding)
	,
	Reuter 2009
	\cite{Reuter2009}
	(eigenfunctions)
	,
	Reuter 2009b
	\cite{Reuter2009b}
	(discrete)
	,
	Gkebal 2009
	\cite{Gkebal2009}
	(sum)
	,
	Reuter 2010
	\cite{Reuter2010}
	(Morse-Smale complexes of eigenfunctions)
	,
	Bronstein 2010
	\cite{Bronstein2010}
	(Gromov-Hausdorff distances)
	,
	Bronstein 2011
	\cite{Bronstein2011}
	(Spectral shape distance)
}

Fiedler vector = 2nd eigenvector (follows shape)

Eigenfunctions are spherical harmonics when sphere = domain

Nth eigenfunction has at most n nodal domains (areas where function has positive/negative sign constant)

Properties of shape DNA:
\begin{itemize}
\item isometry
\item scaling
\item similarity
\item efficiency
\item completeness
\item compression
\item physicality
\end{itemize}

Shape spectrum is eigenvalues of Helmholtz equation on surface

number of eigenvalues below $\lambda$ is constant times $\lambda^{\frac{d}{2}}$ where region is in $\RR^d$.

GPS has no self-intersections if curve/surface has no self-intersections

Inner product of GPS coordinates is Green's function between surfaces

Laplacian eigenvalue problem = Helmholtz equation = spatial eigenmodes of Laplacian(-Beltramian) operator

Neumann eigenmodes faster at getting significant features than Dirichlet eigenmodes

Confidence intervals for estimated permutation test p-values

Effect of L-B discretizations on eigenfunctions

Use nodal domains of eigenfunction rather than skeletons (representation is more concise)

Gkebal: Find diffusion at each point after time $t$ (essentially sum of L-B eigenvectors rather than single eigenvector)

Reeb graph of sum for analysis

Reuter: Zero level sets do NOT align to shape features

Sign flips and switching of eigenfunctions dealt with by constructing Morse-Smale complexes (lines connecting saddles to extrema)

Bronstein: Use distance in GPS space to create metric, calculate distance between metric spaces of two GPS representations

Bronstein: Use distribution of diffusion distances where diffusion is guided by GPS coordinates

Other citations:
\begin{itemize}
\item Niethammer (9) and Reuter (10) for shape analysis of medical images
\item Shi (13) for Reeb graphs of Laplace-Beltrami operator
\item Mangin (18) for moment invariants
\item Levitt (19) for shape index
\item Gerig (20) for spherical harmonics
\item Patane 2008 for Reeb graph computation from contours
\item Jain 2006 for embedding using fitted Gaussian kernel
\item Jain 2007 and Mateus 2007 use graph Laplacian
\item Edelsbrunner 2003 for computation of Morse-Smale complexes
\end{itemize}

%% Xie 2008
%% Wang 2008
%% Tagliasacchi 2009
%% Sparks 2010
\subsection{
	Skeletons:
	Xie 2008
	\cite{Xie2008}
	(Skeletal context)
	,
	Wang 2008
	\cite{Wang2008}
	(Curve skeleton)
	,
	Tagliasacchi 2009
	\cite{Tagliasacchi2009}
	(incomplete)
	,
	Sparks 2010
	\cite{Sparks2010}
	(equidistant from boundary)
	,
	Naouai 2011
	\cite{Naouai2011}
	(NURBS)
}

Xie: Use Blum's medial axis transform.

Resample curve to points of interest: use use closest points of curve to skeleton

Wang: Shrink 3D with boundary constraint

Energy minimization:
\[
	\sum_{\text{edges}}
	\str{
		\cir{
			v_i^{t+1}
			-
			v_j^{t+1}
		}
		-
		\cir{
			\text{contraction ratio}
		}^2
		\cir{
			v_i^t
			-
			v_j^t
		}
	}^2
	+
	\lambda
	\sum_{\text{boundary}}
	\str{
		v_i^{t+1}
		-
		v_i^t
	}^2
\]

Iterative thinning in $t$ and discard branches that are smaller than contraction ratio

Tagliasacchi: Handle incomplete data.

Skeleton is "rotational symmetry axis" of shape

Prior: shapes are locally cylindrical, so using normals of points available should build local cylinders to stitch together

Fill in points, then solve for skeleton

Sparks: Find points where total distance from boundary in all directions is minimized.

Embed skeleton and morph to get distance between surfaces (curves)

Naouai: Image morphometry (erode, etc.) on NURBS estimation of data (similar to Laplacian-of-Gaussian except erode-of-Bspline)

Other citations:
\begin{itemize}
\item Pizer (19) for medial loci
\item Lee 2000 for 1D moving least squares for skeleton construction
\item Ogniewicz 1995 for skeletons from Voronoi diagrams (collapsing triangles)
\end{itemize}

%% Ernst 1989
%% Wolfson 1990
%% Carcassoni 2003
%% Frenkel 2003
%% Chen 2008
%% Ebrahim 2008
%% Xu 2009
\subsection{
	Heuristics:
	Ernst 1989
	\cite{Ernst1989}
	and	Wolfson 1990
	\cite{Wolfson1990}
	(Shape signature string matching)
	,
	Carcassoni 2003
	\cite{Carcassoni2003}
	(spectral graphs)
	,
	Frenkel 2003
	\cite{Frenkel2003}
	(fast marching)
	,
	Chen 2008
	\cite{Chen2008}
	(Smith-Waterman)
	,
	Ebrahim 2009
	\cite{Ebrahim2009}
	(Hilbert curve)
	,
	Xu 2009
	\cite{Xu2009}
	(contour flexibility)
	,
	Viswanath 2011
	\cite{Viswanath2011}
	and Gokaramaiah 2011
	\cite{Gokaramaiah2011}
	(Zernicke moments)
}

Ernst/Wolfram: Find longest matching subsets of two curves.

Representation: Instantaneous turning angle: derivative of
$
\theta \cir{s_{n+1}}
=
\theta \cir{s_n}
+
$
angle from $n$ to $n+1$.

\note{Wolfson talks about partially-occluded curves. Could we use GPS w/ automated sulci that have high rate of disconnected curves? What about GPS w/ skull extraction?}

Carcassoni: Point proximity matrix: adjacency matrix between points (same as distance along curve)

Take eigenvectors of point proximity to match.
\note{similar to us, but uses point proximity instead of curvature}

Frenkel: Create a 2D graph of possible matches from a point in $P$ to a point in $Q$ with cost between each grid point. Start at $\cir{0,0}$ and work shortest path to the corner.

Chen: distance across shape and turning angles

Partial shape matching of segment pairs

Ebrahim: Hilbert curves are iteratively space filling curves

Image is map of Hilbert curve to pixel intensity.

Sample and smooth (with wavelets) $\rightarrow$ vector of pixel intensities is descriptor

Xu: Represent shapes based on difference between inside area and outside area

Viswanath: Use distribution of distances and centroid locations as features

For comparison, compute Zernicke moments
\[
	\int \int
	I \cir{x,y}
	V_{pq} \cir{x,y}
	dx dy
\]
where V is chosen to be rotationally/scale invariant.

Other citations:
\begin{itemize}
\item Freeman (10): Shape features of critical points
\item Ayache (4): correspondence between polygonal approximations to curves
\item Turney (5): Templates of objects
\item Kalvin (6,11): Breakpoints in a curve
\end{itemize}



