#LyX 1.6.5 created this file. For more info see http://www.lyx.org/
\lyxformat 345
\begin_document
\begin_header
\textclass amsart
\use_default_options true
\begin_modules
theorems-ams
eqs-within-sections
figs-within-sections
\end_modules
\language english
\inputencoding auto
\font_roman default
\font_sans default
\font_typewriter default
\font_default_family default
\font_sc false
\font_osf false
\font_sf_scale 100
\font_tt_scale 100

\graphics default
\paperfontsize default
\spacing single
\use_hyperref false
\papersize default
\use_geometry false
\use_amsmath 1
\use_esint 1
\cite_engine basic
\use_bibtopic false
\paperorientation portrait
\secnumdepth 3
\tocdepth 3
\paragraph_separation indent
\defskip medskip
\quotes_language english
\papercolumns 1
\papersides 1
\paperpagestyle default
\tracking_changes false
\output_changes false
\author "" 
\author "" 
\end_header

\begin_body

\begin_layout Title
An Invariant Shape representation using anisotropic wave equation
\end_layout

\begin_layout Author
Anand Joshi
\end_layout

\begin_layout Abstract
This paper presents a new shape representations for curves using anisotropic
 wave equation.
 We also show that this representation has useful and interesting properties
 such as stability, uniqueness and invariance to scaling and isometric transform
ation.
 The shape is represented in terms of eigensystem of wave equation.
 This shape presentation is then applied to 
\end_layout

\begin_layout Section*
Introduction
\end_layout

\begin_layout Standard
* Review existing shape representations
\end_layout

\begin_layout Standard
We present a novel shape 
\end_layout

\begin_layout Standard
We use anisotropic Laplace-Beltrami operator
\end_layout

\begin_layout Standard
\begin_inset Formula \[
L=\text{div}(\kappa\nabla)\]

\end_inset

 where 
\begin_inset Formula $\kappa$
\end_inset

 is curvature.
 In differential geometry, the fundamental theorem of curves states that
 any regular curve with non-zero curvature has its shape (and size) completely
 determined by its curvature and torsion.
\end_layout

\begin_layout Standard
For planar curves, torsion is identically zero.
 
\end_layout

\begin_layout Standard
* For 3D curves, encode diffusivity in 2 directions by torsion and curvature
 (?)
\end_layout

\begin_layout Section
Frenet-Serre formulas
\end_layout

\begin_layout Standard
http://en.wikipedia.org/wiki/Frenet_frame
\end_layout

\begin_layout Standard
http://en.wikipedia.org/wiki/Fundamental_theorem_of_curves
\end_layout

\begin_layout Standard
This shows that curvature represents the curve.
\end_layout

\begin_layout Section
GPS Curves
\end_layout

\begin_layout Standard
In this
\end_layout

\begin_layout Standard
* Application to finding Diffeomorphic point correspondance
\end_layout

\begin_layout Standard
* Sulcal Classification
\end_layout

\begin_layout Section
Discretization using finite element method
\end_layout

\begin_layout Standard
\begin_inset Formula \begin{eqnarray*}
Lf & = & \lambda f\\
\int(\nabla\kappa(s)\nabla f(s))\eta(s)ds & = & \lambda\int f(s)\eta(s)ds\\
\int\kappa(s)\nabla f(s)\nabla\eta(s)ds & = & \lambda\int f(s)\eta(s)ds\end{eqnarray*}

\end_inset


\end_layout

\begin_layout Standard
Using linear elements, 
\begin_inset Formula $f(s)=\sum_{i}f_{i}e_{i}(s)$
\end_inset

, 
\begin_inset Formula $\eta(s)=\sum_{i}\eta_{i}e_{i}(s)$
\end_inset

, we get,
\end_layout

\begin_layout Standard
\begin_inset Formula \begin{eqnarray*}
\kappa\sum_{i}\sum_{j}f_{i}\eta_{j}\int\nabla e_{i}(s)\nabla e_{j}(s)ds & = & \lambda\sum_{i}\sum_{j}f_{i}\eta_{j}\int e_{i}(s)e_{j}(s)ds\\
\kappa Sf & = & \lambda Mf\end{eqnarray*}

\end_inset


\end_layout

\begin_layout Standard
------------------
\end_layout

\end_body
\end_document
