% Copyright 2012 Anand A. Joshi, David W. Shattuck and Richard M. Leahy
% This file is part SVREG.
%

clc;close all;clear% all;
restoredefaultpath;
addpath(genpath('../src'));
addpath(genpath('../dev'));
ss=dir('/home/ajoshi/data/');kk1=0;
for jj=3:2:5
    kk1=kk1+1;
    subbasename1=sprintf('/home/ajoshi/data/%s/%s.bse',ss(jj).name,ss(jj).name);
    subbasename2=sprintf('/home/ajoshi/data/%s/%s.bse',ss(jj+1).name,ss(jj+1).name);
    c1all=readdfc_sipi([subbasename1,'.left.mapped.refined.dfc']);
    c2all=readdfc_sipi([subbasename2,'.left.mapped.refined.dfc']);
    for sulid=1:26
        
        c1=c1all{sulid};
        c2=c2all{sulid};
        %c2=curves
        figure;plot3(c1(:,1),c1(:,2),c1(:,3));
        hold on;plot3(c2(:,1),c2(:,2),c2(:,3));
        c1_param=param_curve(c1);c2_param=param_curve(c2);
        c1=interp1(c1_param,c1,linspace(0,1,100),'cubic');
        c2=interp1(c2_param,c2,linspace(0,1,100),'cubic');
        
        gps_dist=curves_distance(c1,c2);
        
        figure;plot3(c1(:,1),c1(:,2),c1(:,3));
        hold on;plot3(c2(:,1)+50,c2(:,2)+50,c2(:,3)+50);
        
        c1=gps_dist.c1.vertices;c2=gps_dist.c2.vertices;
        dst(sulid,kk1,:)=gps_dist.dist_ptwise;
        
        figure;plot3(c1(:,1),c1(:,2),c1(:,3));
        hold on;plot3(c2(:,1)+50,c2(:,2)+50,c2(:,3)+50);
        for kk=0:19
            line([c1(5*kk+1,1);c2(gps_dist.ind2to1(5*kk+1),1)+50],[c1(5*kk+1,2);c2(gps_dist.ind2to1(5*kk+1),2)+50],[c1(5*kk+1,3);c2(gps_dist.ind2to1(5*kk+1),3)+50]);
        end
        
    end
end

aa=mean(dst,2);



%
% X=[1:20];
% Y1=[0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0];
% Y2=[0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0];
% Y3=[0,0,0,0,0,0,0,.3,.6,1,.6,.3,0,0,0,0,0,0,0,0];
% c1=[X',Y1',0*X'];
% p=polyfit([1:length(c1)]',c1(:,2),7);
% c1(:,2)=polyval(p,linspace(1,length(c1),100));
% c1(:,1)=[1:length(c1)];
% c1(:,3)=c1(:,2)*0;


