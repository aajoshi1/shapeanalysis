% Copyright 2012 Anand A. Joshi, David W. Shattuck and Richard M. Leahy
% This file is part SVREG.
%

clc;close all;clear% all;
restoredefaultpath;
addpath(genpath('../src'));
addpath(genpath('../dev'));
ss=dir('/home/ajoshi/data/');kk=0;
X=[1:30];
Y1=[zeros(1,5),0,0,0,0,0,0,0,0,10,0,0,0,0,0,0,0,0,0,0,0,zeros(1,5)];
Y2=[zeros(1,5),0,0,0,0,0,0,0,0,0,0,10,0,0,0,0,0,0,0,0,0,zeros(1,5)];
%Y2=[zeros(1,5),0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,zeros(1,5)];

Y3=[0,0,0,0,0,0,0,.3,.6,1,.6,.3,0,0,0,0,0,0,0,0];

c2(:,2)=25*gausswin(100,10);
c1(:,2)=[c2(10:end,2)',c2(1:9,2)']';
c1(:,1)=1:length(c1);c1(:,3)=c1(:,1)*0;
c2(:,1)=1:length(c2);c2(:,3)=c2(:,1)*0;
c1=[[1:130]',[c1(:,2);zeros(30,1)],[c1(:,3);zeros(30,1)]];
c2=[[1:130]',[c2(:,2);zeros(30,1)],[c2(:,3);zeros(30,1)]];

figure;plot3(c1(:,1),c1(:,2),c1(:,3));
hold on;plot3(c2(:,1),c2(:,2),c2(:,3));
c1_param=param_curve(c1);c2_param=param_curve(c2);
c1=interp1(c1_param,c1,linspace(0,1,130),'cubic');
c2=interp1(c2_param,c2,linspace(0,1,130),'cubic');
   % c2(:,2)=c2(:,2)*.01;
gps_dist=curves_distance(c1,c2);
    
    
dst=gps_dist.dist;
dst=c1-c2(gps_dist.ind2to1,:);
dst=sqrt(sum(dst.^2,2) );
dst=gps_dist.dist_ptwise;
colr=jet(1000);
cind1=round(1+dst*999/max(dst));
color1=colr(cind1,:);gps_dist.c2.vertices=gps_dist.c2.vertices+10;
    
h=figure;plot_curve(gps_dist.c1,color1,h);%plot3(c1(:,1),c1(:,2),c1(:,3));
hold on;plot_curve(gps_dist.c2,color1,h);%plot3(c2(:,1)+1,c2(:,2)+1,c2(:,3)+1);
for kk=0:24
    plot3([gps_dist.c1.vertices(5*kk+1,1);gps_dist.c2.vertices(gps_dist.ind2to1(5*kk+1),1)],[gps_dist.c1.vertices(5*kk+1,2);gps_dist.c2.vertices(gps_dist.ind2to1(5*kk+1),2)],[gps_dist.c1.vertices(5*kk+1,3);gps_dist.c2.vertices(gps_dist.ind2to1(5*kk+1),3)],'k');
end


axis equal;



