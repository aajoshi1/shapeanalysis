% Copyright 2012 Anand A. Joshi, David W. Shattuck and Richard M. Leahy
% This file is part SVREG.
%

clc;close all;clear% all;
restoredefaultpath;
addpath(genpath('../src'));
addpath(genpath('../dev'));
ss=dir('/home/ajoshi/data/');kk=0;
X=[1:30];
Y1=[zeros(1,5),0,0,0,0,0,0,0,0,10,0,0,0,0,0,0,0,0,0,0,0,zeros(1,5)];
Y2=[zeros(1,5),0,0,0,0,0,0,0,0,0,0,10,0,0,0,0,0,0,0,0,0,zeros(1,5)];
Y3=[0,0,0,0,0,0,0,.3,.6,1,.6,.3,0,0,0,0,0,0,0,0];

c1=[X',Y1',0*X'];
p=polyfit([1:length(c1)]',c1(:,2),10);c1o=c1;clear c1;
c1(:,2)=polyval(p,linspace(1,length(c1o),100));
c1(:,1)=[1:length(c1)];
c1(:,3)=c1(:,2)*0;


c2=[X',Y2',0*X'];
p=polyfit([1:length(c2)]',c2(:,2),7);c2o=c2;clear c2;
c2(:,2)=polyval(p,linspace(1,length(c2o),100));
c2(:,1)=[1:length(c2)];
c2(:,3)=c2(:,2)*0;



    figure;plot3(c1(:,1),c1(:,2),c1(:,3));
    hold on;plot3(c2(:,1),c2(:,2),c2(:,3));
    c1_param=param_curve(c1);c2_param=param_curve(c2);
    c1=interp1(c1_param,c1,linspace(0,1,100),'cubic');
    c2=interp1(c2_param,c2,linspace(0,1,100),'cubic');
    
    gps_dist=curves_distance(c1,c2);
    
    
    dst=gps_dist.dist;

    figure;plot3(c1(:,1),c1(:,2),c1(:,3));
    hold on;plot3(c2(:,1)+1,c2(:,2)+1,c2(:,3)+1);
    for kk=0:19
        line([c1(5*kk+1,1);c2(gps_dist.ind2to1(5*kk+1),1)+1],[c1(5*kk+1,2);c2(gps_dist.ind2to1(5*kk+1),2)+1],[c1(5*kk+1,3);c2(gps_dist.ind2to1(5*kk+1),3)+1]);
    end






